Rocky Linux - Tested Hardware & Statistics
------------------------------------------

A project to collect tested hardware configurations for Rocky Linux.

Anyone can contribute to this report by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Please submit a probe of your configuration if it's not presented on the page or is rare.

This is a report for all computer types. See also reports for [desktops](/Dist/Rocky_Linux/Desktop/README.md) and [notebooks](/Dist/Rocky_Linux/Notebook/README.md).

Full-feature report is available here: https://linux-hardware.org/?view=trends

Contents
--------

* [ Test Cases ](#test-cases)

* [ System ](#system)
  - [ Kernel                   ](#kernel)
  - [ Kernel Family            ](#kernel-family)
  - [ Kernel Major Ver.        ](#kernel-major-ver)
  - [ Arch                     ](#arch)
  - [ DE                       ](#de)
  - [ Display Server           ](#display-server)
  - [ Display Manager          ](#display-manager)
  - [ OS Lang                  ](#os-lang)
  - [ Boot Mode                ](#boot-mode)
  - [ Filesystem               ](#filesystem)
  - [ Part. scheme             ](#part-scheme)
  - [ Dual Boot with Linux/BSD ](#dual-boot-with-linuxbsd)
  - [ Dual Boot (Win)          ](#dual-boot-win)

* [ Board ](#board)
  - [ Vendor                   ](#vendor)
  - [ Model                    ](#model)
  - [ Model Family             ](#model-family)
  - [ MFG Year                 ](#mfg-year)
  - [ Form Factor              ](#form-factor)
  - [ Secure Boot              ](#secure-boot)
  - [ Coreboot                 ](#coreboot)
  - [ RAM Size                 ](#ram-size)
  - [ RAM Used                 ](#ram-used)
  - [ Total Drives             ](#total-drives)
  - [ Has CD-ROM               ](#has-cd-rom)
  - [ Has Ethernet             ](#has-ethernet)
  - [ Has WiFi                 ](#has-wifi)
  - [ Has Bluetooth            ](#has-bluetooth)

* [ Location ](#location)
  - [ Country                  ](#country)
  - [ City                     ](#city)

* [ Drives ](#drives)
  - [ Drive Vendor             ](#drive-vendor)
  - [ Drive Model              ](#drive-model)
  - [ HDD Vendor               ](#hdd-vendor)
  - [ SSD Vendor               ](#ssd-vendor)
  - [ Drive Kind               ](#drive-kind)
  - [ Drive Connector          ](#drive-connector)
  - [ Drive Size               ](#drive-size)
  - [ Space Total              ](#space-total)
  - [ Space Used               ](#space-used)
  - [ Malfunc. Drives          ](#malfunc-drives)
  - [ Malfunc. Drive Vendor    ](#malfunc-drive-vendor)
  - [ Malfunc. HDD Vendor      ](#malfunc-hdd-vendor)
  - [ Malfunc. Drive Kind      ](#malfunc-drive-kind)
  - [ Failed Drives            ](#failed-drives)
  - [ Failed Drive Vendor      ](#failed-drive-vendor)
  - [ Drive Status             ](#drive-status)

* [ Storage controller ](#storage-controller)
  - [ Storage Vendor           ](#storage-vendor)
  - [ Storage Model            ](#storage-model)
  - [ Storage Kind             ](#storage-kind)

* [ Processor ](#processor)
  - [ CPU Vendor               ](#cpu-vendor)
  - [ CPU Model                ](#cpu-model)
  - [ CPU Model Family         ](#cpu-model-family)
  - [ CPU Cores                ](#cpu-cores)
  - [ CPU Sockets              ](#cpu-sockets)
  - [ CPU Threads              ](#cpu-threads)
  - [ CPU Op-Modes             ](#cpu-op-modes)
  - [ CPU Microcode            ](#cpu-microcode)
  - [ CPU Microarch            ](#cpu-microarch)

* [ Graphics ](#graphics)
  - [ GPU Vendor               ](#gpu-vendor)
  - [ GPU Model                ](#gpu-model)
  - [ GPU Combo                ](#gpu-combo)
  - [ GPU Driver               ](#gpu-driver)
  - [ GPU Memory               ](#gpu-memory)

* [ Monitor ](#monitor)
  - [ Monitor Vendor           ](#monitor-vendor)
  - [ Monitor Model            ](#monitor-model)
  - [ Monitor Resolution       ](#monitor-resolution)
  - [ Monitor Diagonal         ](#monitor-diagonal)
  - [ Monitor Width            ](#monitor-width)
  - [ Aspect Ratio             ](#aspect-ratio)
  - [ Monitor Area             ](#monitor-area)
  - [ Pixel Density            ](#pixel-density)
  - [ Multiple Monitors        ](#multiple-monitors)

* [ Network ](#network)
  - [ Net Controller Vendor    ](#net-controller-vendor)
  - [ Net Controller Model     ](#net-controller-model)
  - [ Wireless Vendor          ](#wireless-vendor)
  - [ Wireless Model           ](#wireless-model)
  - [ Ethernet Vendor          ](#ethernet-vendor)
  - [ Ethernet Model           ](#ethernet-model)
  - [ Net Controller Kind      ](#net-controller-kind)
  - [ Used Controller          ](#used-controller)
  - [ NICs                     ](#nics)
  - [ IPv6                     ](#ipv6)

* [ Bluetooth ](#bluetooth)
  - [ Bluetooth Vendor         ](#bluetooth-vendor)
  - [ Bluetooth Model          ](#bluetooth-model)

* [ Sound ](#sound)
  - [ Sound Vendor             ](#sound-vendor)
  - [ Sound Model              ](#sound-model)

* [ Memory ](#memory)
  - [ Memory Vendor            ](#memory-vendor)
  - [ Memory Model             ](#memory-model)
  - [ Memory Kind              ](#memory-kind)
  - [ Memory Form Factor       ](#memory-form-factor)
  - [ Memory Size              ](#memory-size)
  - [ Memory Speed             ](#memory-speed)

* [ Printers & scanners ](#printers--scanners)
  - [ Printer Vendor           ](#printer-vendor)
  - [ Printer Model            ](#printer-model)
  - [ Scanner Vendor           ](#scanner-vendor)
  - [ Scanner Model            ](#scanner-model)

* [ Camera ](#camera)
  - [ Camera Vendor            ](#camera-vendor)
  - [ Camera Model             ](#camera-model)

* [ Security ](#security)
  - [ Fingerprint Vendor       ](#fingerprint-vendor)
  - [ Fingerprint Model        ](#fingerprint-model)
  - [ Chipcard Vendor          ](#chipcard-vendor)
  - [ Chipcard Model           ](#chipcard-model)

* [ Unsupported ](#unsupported)
  - [ Unsupported Devices      ](#unsupported-devices)
  - [ Unsupported Device Types ](#unsupported-device-types)


Test Cases
----------

| Vendor        | Model                       | Form-Factor | Probe                                                      | Date         |
|---------------|-----------------------------|-------------|------------------------------------------------------------|--------------|
| Dell          | 0WN7Y6 A01                  | Desktop     | [ef36ccb6ab](https://linux-hardware.org/?probe=ef36ccb6ab) | Feb 22, 2022 |
| Dell          | 0PC5F7 A02                  | Desktop     | [7c6c7dcd5e](https://linux-hardware.org/?probe=7c6c7dcd5e) | Feb 18, 2022 |
| ASUSTek       | PRIME B450-PLUS             | Desktop     | [1d3c449e8a](https://linux-hardware.org/?probe=1d3c449e8a) | Feb 18, 2022 |
| Supermicro    | X11SPW-TF                   | Server      | [a76bb2e30d](https://linux-hardware.org/?probe=a76bb2e30d) | Feb 07, 2022 |
| Dell          | 0XDN97 A02                  | Server      | [02e5c56a80](https://linux-hardware.org/?probe=02e5c56a80) | Feb 03, 2022 |
| Dell          | 0XDN97 A02                  | Server      | [4aa06b4edd](https://linux-hardware.org/?probe=4aa06b4edd) | Feb 03, 2022 |
| Lenovo        | Legion Y7000 2020H 81Y7     | Notebook    | [2ab4cacc1e](https://linux-hardware.org/?probe=2ab4cacc1e) | Jan 26, 2022 |
| Lenovo        | Legion Y7000 2020H 81Y7     | Notebook    | [787aec5f1c](https://linux-hardware.org/?probe=787aec5f1c) | Jan 26, 2022 |
| ASRock        | B450M Pro4                  | Desktop     | [1ab47f8ff0](https://linux-hardware.org/?probe=1ab47f8ff0) | Jan 20, 2022 |
| MSI           | Z97A GAMING 6               | Desktop     | [4b935d705c](https://linux-hardware.org/?probe=4b935d705c) | Jan 20, 2022 |
| Dell          | 0X3D66 A07                  | Server      | [d5c4ef93c4](https://linux-hardware.org/?probe=d5c4ef93c4) | Jan 18, 2022 |
| Lenovo        | IdeaPad Y700-15ISK 80NV     | Notebook    | [7225108b91](https://linux-hardware.org/?probe=7225108b91) | Jan 10, 2022 |
| HP            | ZBook 15 G3                 | Notebook    | [89809f906e](https://linux-hardware.org/?probe=89809f906e) | Jan 10, 2022 |
| Lenovo        | Legion 5 15ARH05H 82B1      | Notebook    | [90821cb3a5](https://linux-hardware.org/?probe=90821cb3a5) | Jan 03, 2022 |
| Lenovo        | IdeaPadFlex 5 14ALC05 82... | Convertible | [c7f9478d55](https://linux-hardware.org/?probe=c7f9478d55) | Jan 03, 2022 |
| AZW           | Gemini M                    | Desktop     | [25e63b737c](https://linux-hardware.org/?probe=25e63b737c) | Dec 31, 2021 |
| AZW           | Gemini M                    | Desktop     | [05ef59842c](https://linux-hardware.org/?probe=05ef59842c) | Dec 31, 2021 |
| Google        | Panther                     | Desktop     | [92e2626936](https://linux-hardware.org/?probe=92e2626936) | Nov 30, 2021 |
| Lenovo        | IdeaPad 500S-14ISK 80Q3     | Notebook    | [6ea0cdba08](https://linux-hardware.org/?probe=6ea0cdba08) | Nov 27, 2021 |
| Lenovo        | ThinkPad W540 20BGCTO1WW    | Notebook    | [25055cdc26](https://linux-hardware.org/?probe=25055cdc26) | Nov 23, 2021 |
| Gigabyte      | X570 AORUS ULTRA            | Desktop     | [840d920fb2](https://linux-hardware.org/?probe=840d920fb2) | Nov 22, 2021 |
| Gigabyte      | H87-D3H-CF                  | Desktop     | [72fdde33b3](https://linux-hardware.org/?probe=72fdde33b3) | Nov 19, 2021 |
| HP            | Laptop 17-ca1xxx            | Notebook    | [61fe4e654d](https://linux-hardware.org/?probe=61fe4e654d) | Nov 09, 2021 |
| Raspberry ... | Raspberry Pi                | Soc         | [9d7947a5a8](https://linux-hardware.org/?probe=9d7947a5a8) | Nov 06, 2021 |
| Toshiba       | TECRA W50-A                 | Notebook    | [abee9f36ad](https://linux-hardware.org/?probe=abee9f36ad) | Nov 05, 2021 |
| Dell          | 0N4YC8 A00                  | Desktop     | [1a94195ddb](https://linux-hardware.org/?probe=1a94195ddb) | Oct 15, 2021 |
| Intel         | S2600WFT H48104-850         | Server      | [36c4acac2d](https://linux-hardware.org/?probe=36c4acac2d) | Sep 14, 2021 |
| ASUSTek       | PRIME B450M-A II            | Desktop     | [cb9f02b3de](https://linux-hardware.org/?probe=cb9f02b3de) | Sep 07, 2021 |
| ASUSTek       | PRIME B450M-A II            | Desktop     | [f80365b98a](https://linux-hardware.org/?probe=f80365b98a) | Sep 07, 2021 |
| ASUSTek       | P5Q DELUXE                  | Desktop     | [243dba3b27](https://linux-hardware.org/?probe=243dba3b27) | Sep 02, 2021 |
| Lenovo        | ThinkPad T420 42365H1       | Notebook    | [3430adab89](https://linux-hardware.org/?probe=3430adab89) | Aug 25, 2021 |
| Lenovo        | NOK                         | Desktop     | [274005087d](https://linux-hardware.org/?probe=274005087d) | Aug 23, 2021 |
| Lenovo        | ThinkPad T420 42365H1       | Notebook    | [6a306e2253](https://linux-hardware.org/?probe=6a306e2253) | Aug 16, 2021 |
| Dell          | 0M5DCD A00                  | Desktop     | [91acc7eb93](https://linux-hardware.org/?probe=91acc7eb93) | Aug 15, 2021 |
| Lenovo        | ThinkPad W500 406132G       | Notebook    | [e79080e90d](https://linux-hardware.org/?probe=e79080e90d) | Aug 08, 2021 |
| Lenovo        | IdeaPad Slim 1-14AST-05 ... | Notebook    | [860ec3c89d](https://linux-hardware.org/?probe=860ec3c89d) | Aug 08, 2021 |
| Lenovo        | IdeaPad Y410P 20216         | Notebook    | [b2df1c0e6d](https://linux-hardware.org/?probe=b2df1c0e6d) | Aug 08, 2021 |
| Lenovo        | IdeaPad Y410P 20216         | Notebook    | [3fc207c5b9](https://linux-hardware.org/?probe=3fc207c5b9) | Aug 07, 2021 |
| Dell          | 0M5DCD A00                  | Desktop     | [d40f4d3bee](https://linux-hardware.org/?probe=d40f4d3bee) | Aug 06, 2021 |
| ASUSTek       | PRIME TRX40-PRO S           | Desktop     | [59f7d599dd](https://linux-hardware.org/?probe=59f7d599dd) | Aug 04, 2021 |
| Dell          | 0M5DCD A00                  | Desktop     | [5ee09ac705](https://linux-hardware.org/?probe=5ee09ac705) | Aug 04, 2021 |
| Dell          | 0M5DCD A00                  | Desktop     | [77c3d7076e](https://linux-hardware.org/?probe=77c3d7076e) | Aug 04, 2021 |
| Lenovo        | IdeaPad Slim 1-14AST-05 ... | Notebook    | [2a4bd5cb12](https://linux-hardware.org/?probe=2a4bd5cb12) | Jul 11, 2021 |
| Lenovo        | IdeaPad Slim 1-14AST-05 ... | Notebook    | [09738de946](https://linux-hardware.org/?probe=09738de946) | Jul 04, 2021 |
| Lenovo        | IdeaPad Slim 1-14AST-05 ... | Notebook    | [741cab87e1](https://linux-hardware.org/?probe=741cab87e1) | Jun 29, 2021 |
| ASUSTek       | ASUS TUF Gaming A15 FA50... | Notebook    | [60fe7f2653](https://linux-hardware.org/?probe=60fe7f2653) | Jun 13, 2021 |
| Toshiba       | Satellite E45-B             | Notebook    | [84683df1f0](https://linux-hardware.org/?probe=84683df1f0) | Jun 12, 2021 |
| HP            | 0B54h D                     | Desktop     | [ee9a2da17c](https://linux-hardware.org/?probe=ee9a2da17c) | May 19, 2021 |
| Acer          | Aspire VN7-591G             | Notebook    | [bc9e6c4910](https://linux-hardware.org/?probe=bc9e6c4910) | May 10, 2021 |

System
------

Kernel
------

Version of the Linux kernel

![Kernel](./All/images/pie_chart/os_kernel.svg)


| Version                      | Computers | Percent |
|------------------------------|-----------|---------|
| 4.18.0-348.7.1.el8_5.x86_64  | 8         | 21.05%  |
| 4.18.0-348.12.2.el8_5.x86_64 | 6         | 15.79%  |
| 4.18.0-305.10.2.el8_4.x86_64 | 5         | 13.16%  |
| 4.18.0-305.25.1.el8_4.x86_64 | 3         | 7.89%   |
| 4.18.0-348.2.1.el8_5.x86_64  | 2         | 5.26%   |
| 4.18.0-305.el8.x86_64        | 2         | 5.26%   |
| 4.18.0-305.3.1.el8_4.x86_64  | 2         | 5.26%   |
| 4.18.0-305.19.1.el8_4.x86_64 | 2         | 5.26%   |
| 4.18.0-305.12.1.el8_4.x86_64 | 2         | 5.26%   |
| 4.18.0-240.22.1.el8.x86_64   | 2         | 5.26%   |
| 5.4.157-1.el8.elrepo.x86_64  | 1         | 2.63%   |
| 5.14.1-1.el8.elrepo.x86_64   | 1         | 2.63%   |
| 5.10.52-v8.1.el8             | 1         | 2.63%   |
| 4.18.0-348.el8.0.2.x86_64    | 1         | 2.63%   |

Kernel Family
-------------

Linux kernel without a distro release

![Kernel Family](./All/images/pie_chart/os_kernel_family.svg)


| Version | Computers | Percent |
|---------|-----------|---------|
| 4.18.0  | 35        | 92.11%  |
| 5.4.157 | 1         | 2.63%   |
| 5.14.1  | 1         | 2.63%   |
| 5.10.52 | 1         | 2.63%   |

Kernel Major Ver.
-----------------

Linux kernel major version

![Kernel Major Ver.](./All/images/pie_chart/os_kernel_major.svg)


| Version | Computers | Percent |
|---------|-----------|---------|
| 4.18    | 35        | 92.11%  |
| 5.4     | 1         | 2.63%   |
| 5.14    | 1         | 2.63%   |
| 5.10    | 1         | 2.63%   |

Arch
----

OS architecture (x86_64, i586, etc.)

![Arch](./All/images/pie_chart/os_arch.svg)


| Name    | Computers | Percent |
|---------|-----------|---------|
| x86_64  | 37        | 97.37%  |
| aarch64 | 1         | 2.63%   |

DE
--

Desktop Environment

![DE](./All/images/pie_chart/os_de.svg)


| Name          | Computers | Percent |
|---------------|-----------|---------|
| GNOME         | 22        | 57.89%  |
| Unknown       | 7         | 18.42%  |
| KDE5          | 4         | 10.53%  |
| MATE          | 2         | 5.26%   |
| XFCE          | 1         | 2.63%   |
| X-Cinnamon    | 1         | 2.63%   |
| GNOME Classic | 1         | 2.63%   |

Display Server
--------------

X11 or Wayland

![Display Server](./All/images/pie_chart/os_display_server.svg)


| Name    | Computers | Percent |
|---------|-----------|---------|
| Wayland | 17        | 44.74%  |
| X11     | 16        | 42.11%  |
| Unknown | 5         | 13.16%  |

Display Manager
---------------

SDDM, LightDM, etc.

![Display Manager](./All/images/pie_chart/os_display_manager.svg)


| Name    | Computers | Percent |
|---------|-----------|---------|
| Unknown | 21        | 55.26%  |
| GDM     | 12        | 31.58%  |
| SDDM    | 3         | 7.89%   |
| LightDM | 2         | 5.26%   |

OS Lang
-------

Language

![OS Lang](./All/images/pie_chart/os_lang.svg)


| Lang    | Computers | Percent |
|---------|-----------|---------|
| en_US   | 23        | 60.53%  |
| en_IL   | 3         | 7.89%   |
| en_AU   | 2         | 5.26%   |
| de_DE   | 2         | 5.26%   |
| ru_RU   | 1         | 2.63%   |
| ja_JP   | 1         | 2.63%   |
| it_IT   | 1         | 2.63%   |
| es_CO   | 1         | 2.63%   |
| en_ZA   | 1         | 2.63%   |
| en_SG   | 1         | 2.63%   |
| en_CA   | 1         | 2.63%   |
| Unknown | 1         | 2.63%   |

Boot Mode
---------

EFI or BIOS

![Boot Mode](./All/images/pie_chart/os_boot_mode.svg)


| Mode | Computers | Percent |
|------|-----------|---------|
| EFI  | 20        | 52.63%  |
| BIOS | 18        | 47.37%  |

Filesystem
----------

Type of filesystem

![Filesystem](./All/images/pie_chart/os_filesystem.svg)


| Type | Computers | Percent |
|------|-----------|---------|
| Xfs  | 29        | 76.32%  |
| Ext4 | 9         | 23.68%  |

Part. scheme
------------

Scheme of partitioning

![Part. scheme](./All/images/pie_chart/os_part_scheme.svg)


| Type    | Computers | Percent |
|---------|-----------|---------|
| Unknown | 17        | 44.74%  |
| GPT     | 15        | 39.47%  |
| MBR     | 6         | 15.79%  |

Dual Boot with Linux/BSD
------------------------

Hosting more than one Linux/BSD

![Dual Boot with Linux/BSD](./All/images/pie_chart/os_dual_boot.svg)


| Dual boot | Computers | Percent |
|-----------|-----------|---------|
| No        | 35        | 92.11%  |
| Yes       | 3         | 7.89%   |

Dual Boot (Win)
---------------

Hosting Linux and Windows

![Dual Boot (Win)](./All/images/pie_chart/os_dual_boot_win.svg)


| Dual boot | Computers | Percent |
|-----------|-----------|---------|
| No        | 35        | 92.11%  |
| Yes       | 3         | 7.89%   |

Board
-----

Vendor
------

Motherboard manufacturer

![Vendor](./All/images/pie_chart/node_vendor.svg)


| Name                    | Computers | Percent |
|-------------------------|-----------|---------|
| Lenovo                  | 11        | 28.95%  |
| Dell                    | 7         | 18.42%  |
| ASUSTek Computer        | 5         | 13.16%  |
| Hewlett-Packard         | 3         | 7.89%   |
| Toshiba                 | 2         | 5.26%   |
| Gigabyte Technology     | 2         | 5.26%   |
| Supermicro              | 1         | 2.63%   |
| Raspberry Pi Foundation | 1         | 2.63%   |
| MSI                     | 1         | 2.63%   |
| Intel                   | 1         | 2.63%   |
| Google                  | 1         | 2.63%   |
| AZW                     | 1         | 2.63%   |
| ASRock                  | 1         | 2.63%   |
| Acer                    | 1         | 2.63%   |

Model
-----

Motherboard model

![Model](./All/images/pie_chart/node_model.svg)


| Name                                     | Computers | Percent |
|------------------------------------------|-----------|---------|
| Dell PowerEdge R610                      | 2         | 5.26%   |
| Dell OptiPlex 9020                       | 2         | 5.26%   |
| Toshiba TECRA W50-A                      | 1         | 2.63%   |
| Toshiba Satellite E45-B                  | 1         | 2.63%   |
| Supermicro SYS-5029P-WTR                 | 1         | 2.63%   |
| RPi Raspberry Pi                         | 1         | 2.63%   |
| MSI MS-7917                              | 1         | 2.63%   |
| Lenovo ThinkPad W540 20BGCTO1WW          | 1         | 2.63%   |
| Lenovo ThinkPad W500 406132G             | 1         | 2.63%   |
| Lenovo ThinkPad T420 42365H1             | 1         | 2.63%   |
| Lenovo ThinkCentre M72e 36601Y8          | 1         | 2.63%   |
| Lenovo Legion Y7000 2020H 81Y7           | 1         | 2.63%   |
| Lenovo Legion 5 15ARH05H 82B1            | 1         | 2.63%   |
| Lenovo IdeaPadFlex 5 14ALC05 82HU        | 1         | 2.63%   |
| Lenovo IdeaPad Y700-15ISK 80NV           | 1         | 2.63%   |
| Lenovo IdeaPad Y410P 20216               | 1         | 2.63%   |
| Lenovo IdeaPad Slim 1-14AST-05 81VS      | 1         | 2.63%   |
| Lenovo IdeaPad 500S-14ISK 80Q3           | 1         | 2.63%   |
| Intel S2600WFT                           | 1         | 2.63%   |
| HP ZBook 15 G3                           | 1         | 2.63%   |
| HP Z600 Workstation                      | 1         | 2.63%   |
| HP Laptop 17-ca1xxx                      | 1         | 2.63%   |
| Google Panther                           | 1         | 2.63%   |
| Gigabyte X570 AORUS ULTRA                | 1         | 2.63%   |
| Gigabyte H87-D3H                         | 1         | 2.63%   |
| Dell Precision T5610                     | 1         | 2.63%   |
| Dell PowerEdge R720xd                    | 1         | 2.63%   |
| Dell OptiPlex 390                        | 1         | 2.63%   |
| AZW Gemini M                             | 1         | 2.63%   |
| ASUS PRIME TRX40-PRO S                   | 1         | 2.63%   |
| ASUS PRIME B450M-A II                    | 1         | 2.63%   |
| ASUS PRIME B450-PLUS                     | 1         | 2.63%   |
| ASUS P5Q DELUXE                          | 1         | 2.63%   |
| ASUS ASUS TUF Gaming A15 FA506II_FA506II | 1         | 2.63%   |
| ASRock B450M Pro4                        | 1         | 2.63%   |
| Acer Aspire VN7-591G                     | 1         | 2.63%   |

Model Family
------------

Motherboard model prefix

![Model Family](./All/images/pie_chart/node_model_family.svg)


| Name                     | Computers | Percent |
|--------------------------|-----------|---------|
| Lenovo IdeaPad           | 4         | 10.53%  |
| Lenovo ThinkPad          | 3         | 7.89%   |
| Dell PowerEdge           | 3         | 7.89%   |
| Dell OptiPlex            | 3         | 7.89%   |
| ASUS PRIME               | 3         | 7.89%   |
| Lenovo Legion            | 2         | 5.26%   |
| Toshiba TECRA            | 1         | 2.63%   |
| Toshiba Satellite        | 1         | 2.63%   |
| Supermicro SYS-5029P-WTR | 1         | 2.63%   |
| RPi Raspberry            | 1         | 2.63%   |
| MSI MS-7917              | 1         | 2.63%   |
| Lenovo ThinkCentre       | 1         | 2.63%   |
| Lenovo IdeaPadFlex       | 1         | 2.63%   |
| Intel S2600WFT           | 1         | 2.63%   |
| HP ZBook                 | 1         | 2.63%   |
| HP Z600                  | 1         | 2.63%   |
| HP Laptop                | 1         | 2.63%   |
| Google Panther           | 1         | 2.63%   |
| Gigabyte X570            | 1         | 2.63%   |
| Gigabyte H87-D3H         | 1         | 2.63%   |
| Dell Precision           | 1         | 2.63%   |
| AZW Gemini               | 1         | 2.63%   |
| ASUS P5Q                 | 1         | 2.63%   |
| ASUS ASUS                | 1         | 2.63%   |
| ASRock B450M             | 1         | 2.63%   |
| Acer Aspire              | 1         | 2.63%   |

MFG Year
--------

Motherboard manufacture year

![MFG Year](./All/images/pie_chart/node_year.svg)


| Year    | Computers | Percent |
|---------|-----------|---------|
| 2020    | 6         | 15.79%  |
| 2014    | 6         | 15.79%  |
| 2018    | 5         | 13.16%  |
| 2019    | 4         | 10.53%  |
| 2021    | 3         | 7.89%   |
| 2015    | 3         | 7.89%   |
| 2013    | 3         | 7.89%   |
| 2016    | 2         | 5.26%   |
| 2011    | 2         | 5.26%   |
| 2010    | 1         | 2.63%   |
| 2009    | 1         | 2.63%   |
| 2008    | 1         | 2.63%   |
| Unknown | 1         | 2.63%   |

Form Factor
-----------

Physical design of the computer

![Form Factor](./All/images/pie_chart/node_formfactor.svg)


| Name           | Computers | Percent |
|----------------|-----------|---------|
| Desktop        | 16        | 42.11%  |
| Notebook       | 15        | 39.47%  |
| Server         | 5         | 13.16%  |
| System on chip | 1         | 2.63%   |
| Convertible    | 1         | 2.63%   |

Secure Boot
-----------

Enabled or disabled

![Secure Boot](./All/images/pie_chart/node_secureboot.svg)


| State    | Computers | Percent |
|----------|-----------|---------|
| Disabled | 38        | 100%    |

Coreboot
--------

Have coreboot on board

![Coreboot](./All/images/pie_chart/node_coreboot.svg)


| Used | Computers | Percent |
|------|-----------|---------|
| No   | 37        | 97.37%  |
| Yes  | 1         | 2.63%   |

RAM Size
--------

Total RAM memory

![RAM Size](./All/images/pie_chart/node_ram_total.svg)


| Size in GB      | Computers | Percent |
|-----------------|-----------|---------|
| 4.01-8.0        | 9         | 23.68%  |
| 32.01-64.0      | 7         | 18.42%  |
| 16.01-24.0      | 7         | 18.42%  |
| 8.01-16.0       | 6         | 15.79%  |
| 3.01-4.0        | 4         | 10.53%  |
| More than 256.0 | 2         | 5.26%   |
| 64.01-256.0     | 2         | 5.26%   |
| 1.01-2.0        | 1         | 2.63%   |

RAM Used
--------

Used RAM memory

![RAM Used](./All/images/pie_chart/node_ram_used.svg)


| Used GB   | Computers | Percent |
|-----------|-----------|---------|
| 2.01-3.0  | 12        | 31.58%  |
| 3.01-4.0  | 9         | 23.68%  |
| 1.01-2.0  | 7         | 18.42%  |
| 4.01-8.0  | 6         | 15.79%  |
| 8.01-16.0 | 2         | 5.26%   |
| 0.51-1.0  | 1         | 2.63%   |
| 0.01-0.5  | 1         | 2.63%   |

Total Drives
------------

Number of drives on board

![Total Drives](./All/images/pie_chart/node_total_drives.svg)


| Drives | Computers | Percent |
|--------|-----------|---------|
| 1      | 19        | 50%     |
| 2      | 8         | 21.05%  |
| 3      | 5         | 13.16%  |
| 4      | 4         | 10.53%  |
| 14     | 1         | 2.63%   |
| 8      | 1         | 2.63%   |

Has CD-ROM
----------

Has CD-ROM on board

![Has CD-ROM](./All/images/pie_chart/node_has_cdrom.svg)


| Presented | Computers | Percent |
|-----------|-----------|---------|
| No        | 24        | 63.16%  |
| Yes       | 14        | 36.84%  |

Has Ethernet
------------

Has Ethernet on board

![Has Ethernet](./All/images/pie_chart/node_has_ethernet.svg)


| Presented | Computers | Percent |
|-----------|-----------|---------|
| Yes       | 35        | 92.11%  |
| No        | 3         | 7.89%   |

Has WiFi
--------

Has WiFi module

![Has WiFi](./All/images/pie_chart/node_has_wifi.svg)


| Presented | Computers | Percent |
|-----------|-----------|---------|
| Yes       | 23        | 60.53%  |
| No        | 15        | 39.47%  |

Has Bluetooth
-------------

Has Bluetooth module

![Has Bluetooth](./All/images/pie_chart/node_has_bluetooth.svg)


| Presented | Computers | Percent |
|-----------|-----------|---------|
| Yes       | 20        | 52.63%  |
| No        | 18        | 47.37%  |

Location
--------

Country
-------

Geographic location (country)

![Country](./All/images/pie_chart/node_location.svg)


| Country      | Computers | Percent |
|--------------|-----------|---------|
| USA          | 13        | 34.21%  |
| Israel       | 3         | 7.89%   |
| Czechia      | 3         | 7.89%   |
| Germany      | 2         | 5.26%   |
| Australia    | 2         | 5.26%   |
| UK           | 1         | 2.63%   |
| South Africa | 1         | 2.63%   |
| Singapore    | 1         | 2.63%   |
| Russia       | 1         | 2.63%   |
| Portugal     | 1         | 2.63%   |
| Poland       | 1         | 2.63%   |
| Malaysia     | 1         | 2.63%   |
| Kazakhstan   | 1         | 2.63%   |
| Japan        | 1         | 2.63%   |
| Italy        | 1         | 2.63%   |
| France       | 1         | 2.63%   |
| Colombia     | 1         | 2.63%   |
| China        | 1         | 2.63%   |
| Canada       | 1         | 2.63%   |
| Belgium      | 1         | 2.63%   |

City
----

Geographic location (city)

![City](./All/images/pie_chart/node_city.svg)


| City             | Computers | Percent |
|------------------|-----------|---------|
| Prague           | 2         | 5.26%   |
| Melbourne        | 2         | 5.26%   |
| Haifa            | 2         | 5.26%   |
| Xi'an            | 1         | 2.63%   |
| Weinheim         | 1         | 2.63%   |
| St Petersburg    | 1         | 2.63%   |
| Singapore        | 1         | 2.63%   |
| Scarborough      | 1         | 2.63%   |
| San Ramon        | 1         | 2.63%   |
| Saint Albans     | 1         | 2.63%   |
| Rehovot          | 1         | 2.63%   |
| Pretoria         | 1         | 2.63%   |
| Paris            | 1         | 2.63%   |
| Nur-Sultan       | 1         | 2.63%   |
| Nowy Wisnicz     | 1         | 2.63%   |
| New York         | 1         | 2.63%   |
| Mequon           | 1         | 2.63%   |
| Manassas         | 1         | 2.63%   |
| Lisbon           | 1         | 2.63%   |
| Lebanon          | 1         | 2.63%   |
| Kuala Terengganu | 1         | 2.63%   |
| Jette            | 1         | 2.63%   |
| Houston          | 1         | 2.63%   |
| Glasgow          | 1         | 2.63%   |
| Fredericton      | 1         | 2.63%   |
| Fredericksburg   | 1         | 2.63%   |
| Forest           | 1         | 2.63%   |
| Corvallis        | 1         | 2.63%   |
| Contrada Tenna   | 1         | 2.63%   |
| Burlington       | 1         | 2.63%   |
| Bucaramanga      | 1         | 2.63%   |
| Boskovice        | 1         | 2.63%   |
| Berlin           | 1         | 2.63%   |
| ÅŒtsu          | 1         | 2.63%   |
| Ashburn          | 1         | 2.63%   |

Drives
------

Drive Vendor
------------

Hard drive vendors

![Drive Vendor](./All/images/pie_chart/drive_vendor.svg)


| Vendor                  | Computers | Drives | Percent |
|-------------------------|-----------|--------|---------|
| Seagate                 | 9         | 31     | 15.25%  |
| Samsung Electronics     | 9         | 11     | 15.25%  |
| WDC                     | 8         | 13     | 13.56%  |
| Toshiba                 | 5         | 5      | 8.47%   |
| Unknown                 | 3         | 3      | 5.08%   |
| Intel                   | 3         | 3      | 5.08%   |
| Kingston                | 2         | 2      | 3.39%   |
| Hitachi                 | 2         | 3      | 3.39%   |
| Crucial                 | 2         | 2      | 3.39%   |
| Union Memory (Shenzhen) | 1         | 1      | 1.69%   |
| UMIS                    | 1         | 2      | 1.69%   |
| StoreJet                | 1         | 1      | 1.69%   |
| SK Hynix                | 1         | 1      | 1.69%   |
| SanDisk                 | 1         | 1      | 1.69%   |
| PNY                     | 1         | 1      | 1.69%   |
| Phison                  | 1         | 1      | 1.69%   |
| LITEONIT                | 1         | 1      | 1.69%   |
| LITEON                  | 1         | 1      | 1.69%   |
| HGST                    | 1         | 1      | 1.69%   |
| Fujitsu                 | 1         | 1      | 1.69%   |
| Dogfish                 | 1         | 1      | 1.69%   |
| Corsair                 | 1         | 1      | 1.69%   |
| China                   | 1         | 1      | 1.69%   |
| AGI                     | 1         | 1      | 1.69%   |
| A-DATA Technology       | 1         | 1      | 1.69%   |

Drive Model
-----------

Hard drive models

![Drive Model](./All/images/pie_chart/drive_model.svg)


| Model                                        | Computers | Percent |
|----------------------------------------------|-----------|---------|
| Unknown MMC Card  64GB                       | 2         | 3.13%   |
| Seagate ST300MP0005 304GB                    | 2         | 3.13%   |
| WDC WDS240G2G0B-00EPW0 240GB SSD             | 1         | 1.56%   |
| WDC WDS240G2G0A-00JH30 240GB SSD             | 1         | 1.56%   |
| WDC WD5000LPCX-24VHAT0 500GB                 | 1         | 1.56%   |
| WDC WD5000AAKX-75U6AA0 500GB                 | 1         | 1.56%   |
| WDC WD5000AAKX-001CA0 500GB                  | 1         | 1.56%   |
| WDC WD20EZRX-00DC0B0 2TB                     | 1         | 1.56%   |
| WDC WD10SPCX-24HWST1 1TB                     | 1         | 1.56%   |
| WDC WD10JPVX-22JC3T0 1TB                     | 1         | 1.56%   |
| WDC WD1001FALS-00J7B0 1TB                    | 1         | 1.56%   |
| WDC PC SN530 SDBPMPZ-512G-1101 512GB         | 1         | 1.56%   |
| Unknown SD/MMC/MS PRO 64GB                   | 1         | 1.56%   |
| Union Memory (Shenzhen) NVMe SSD Drive 128GB | 1         | 1.56%   |
| UMIS RPFTJ128PDD2EWX 128GB                   | 1         | 1.56%   |
| Toshiba THNSNJ512GACU 512GB SSD              | 1         | 1.56%   |
| Toshiba THNSNJ128G8NU 128GB SSD              | 1         | 1.56%   |
| Toshiba MG04ACA400E 4TB                      | 1         | 1.56%   |
| Toshiba MG03ACA400 4TB                       | 1         | 1.56%   |
| Toshiba DT01ACA050 500GB                     | 1         | 1.56%   |
| StoreJet Disk 4TB                            | 1         | 1.56%   |
| SK Hynix SH920 2.5 7MM 256GB SSD             | 1         | 1.56%   |
| Seagate ST91000640SS 1TB                     | 1         | 1.56%   |
| Seagate ST500DM002-1BD142 500GB              | 1         | 1.56%   |
| Seagate ST4000NM0033-9ZM170 4TB              | 1         | 1.56%   |
| Seagate ST4000DM004-2CV104 4TB               | 1         | 1.56%   |
| Seagate ST3160318AS 160GB                    | 1         | 1.56%   |
| Seagate ST2000DM008-2FR102 2TB               | 1         | 1.56%   |
| Seagate ST1000VX005-2EZ102 1TB               | 1         | 1.56%   |
| Seagate ST1000LM024 HN-M101MBB 1TB           | 1         | 1.56%   |
| SanDisk SSD U110 16GB                        | 1         | 1.56%   |
| Samsung SSD 980 PRO 1TB                      | 1         | 1.56%   |
| Samsung SSD 970 EVO 500GB                    | 1         | 1.56%   |
| Samsung SSD 870 QVO 2TB                      | 1         | 1.56%   |
| Samsung SSD 870 EVO 1TB                      | 1         | 1.56%   |
| Samsung SSD 860 QVO 4TB                      | 1         | 1.56%   |
| Samsung SSD 860 EVO 500GB                    | 1         | 1.56%   |
| Samsung SSD 860 EVO 1TB                      | 1         | 1.56%   |
| Samsung SM963 2.5" NVMe PCIe SSD 256GB       | 1         | 1.56%   |
| Samsung MZVLB512HBJQ-000L2 512GB             | 1         | 1.56%   |
| Samsung MZNLN512HAJQ-000H1 512GB SSD         | 1         | 1.56%   |
| PNY CS900 120GB SSD                          | 1         | 1.56%   |
| Phison Sabrent 1TB                           | 1         | 1.56%   |
| LITEONIT LSS-24L6G 24GB SSD                  | 1         | 1.56%   |
| LITEON CV1-DB256 256GB SSD                   | 1         | 1.56%   |
| Kingston SA400S37120G 120GB SSD              | 1         | 1.56%   |
| Kingston NVMe SSD Drive 512GB                | 1         | 1.56%   |
| Intel SSDPEDMW012T4 1TB                      | 1         | 1.56%   |
| Intel SSDPE7KX020T7 2TB                      | 1         | 1.56%   |
| Intel NVMe SSD Drive 512GB                   | 1         | 1.56%   |
| Hitachi HTS727575A9E364 752GB                | 1         | 1.56%   |
| Hitachi HTS543232A7A384 320GB                | 1         | 1.56%   |
| Hitachi HDP725050GLA360 500GB                | 1         | 1.56%   |
| HGST HTS721010A9E630 1TB                     | 1         | 1.56%   |
| Fujitsu MHV2080BH 80GB                       | 1         | 1.56%   |
| Dogfish SSD 2TB                              | 1         | 1.56%   |
| Crucial CT240BX500SSD1 240GB                 | 1         | 1.56%   |
| Crucial CT1000P2SSD8 1TB                     | 1         | 1.56%   |
| Corsair Neutron SSD 64GB                     | 1         | 1.56%   |
| China M.2 SSD 256GB                          | 1         | 1.56%   |

HDD Vendor
----------

Hard disk drive vendors

![HDD Vendor](./All/images/pie_chart/drive_hdd_vendor.svg)


| Vendor   | Computers | Drives | Percent |
|----------|-----------|--------|---------|
| Seagate  | 9         | 31     | 37.5%   |
| WDC      | 6         | 10     | 25%     |
| Toshiba  | 3         | 3      | 12.5%   |
| Hitachi  | 2         | 3      | 8.33%   |
| Unknown  | 1         | 1      | 4.17%   |
| StoreJet | 1         | 1      | 4.17%   |
| HGST     | 1         | 1      | 4.17%   |
| Fujitsu  | 1         | 1      | 4.17%   |

SSD Vendor
----------

Solid state drive vendors

![SSD Vendor](./All/images/pie_chart/drive_ssd_vendor.svg)


| Vendor              | Computers | Drives | Percent |
|---------------------|-----------|--------|---------|
| Samsung Electronics | 6         | 7      | 30%     |
| WDC                 | 2         | 2      | 10%     |
| Toshiba             | 2         | 2      | 10%     |
| SK Hynix            | 1         | 1      | 5%      |
| SanDisk             | 1         | 1      | 5%      |
| PNY                 | 1         | 1      | 5%      |
| LITEONIT            | 1         | 1      | 5%      |
| LITEON              | 1         | 1      | 5%      |
| Kingston            | 1         | 1      | 5%      |
| Dogfish             | 1         | 1      | 5%      |
| Crucial             | 1         | 1      | 5%      |
| Corsair             | 1         | 1      | 5%      |
| China               | 1         | 1      | 5%      |

Drive Kind
----------

HDD or SSD

![Drive Kind](./All/images/pie_chart/drive_kind.svg)


| Kind    | Computers | Drives | Percent |
|---------|-----------|--------|---------|
| HDD     | 19        | 51     | 37.25%  |
| SSD     | 17        | 21     | 33.33%  |
| NVMe    | 12        | 15     | 23.53%  |
| MMC     | 2         | 2      | 3.92%   |
| Unknown | 1         | 1      | 1.96%   |

Drive Connector
---------------

SATA, SAS, NVMe, etc.

![Drive Connector](./All/images/pie_chart/drive_bus.svg)


| Type | Computers | Drives | Percent |
|------|-----------|--------|---------|
| SATA | 29        | 70     | 63.04%  |
| NVMe | 12        | 15     | 26.09%  |
| SAS  | 3         | 3      | 6.52%   |
| MMC  | 2         | 2      | 4.35%   |

Drive Size
----------

Size of hard drive

![Drive Size](./All/images/pie_chart/drive_size.svg)


| Size in TB | Computers | Drives | Percent |
|------------|-----------|--------|---------|
| 0.01-0.5   | 18        | 30     | 47.37%  |
| 0.51-1.0   | 11        | 16     | 28.95%  |
| 3.01-4.0   | 5         | 21     | 13.16%  |
| 1.01-2.0   | 4         | 5      | 10.53%  |

Space Total
-----------

Amount of disk space available on the file system

![Space Total](./All/images/pie_chart/drive_space_total.svg)


| Size in GB     | Computers | Percent |
|----------------|-----------|---------|
| 101-250        | 11        | 28.95%  |
| 501-1000       | 7         | 18.42%  |
| 251-500        | 6         | 15.79%  |
| More than 3000 | 4         | 10.53%  |
| 1001-2000      | 4         | 10.53%  |
| 2001-3000      | 2         | 5.26%   |
| 51-100         | 2         | 5.26%   |
| 1-20           | 1         | 2.63%   |
| Unknown        | 1         | 2.63%   |

Space Used
----------

Amount of used disk space

![Space Used](./All/images/pie_chart/drive_space_used.svg)


| Used GB        | Computers | Percent |
|----------------|-----------|---------|
| 1-20           | 13        | 34.21%  |
| 21-50          | 10        | 26.32%  |
| 51-100         | 4         | 10.53%  |
| 101-250        | 3         | 7.89%   |
| 251-500        | 2         | 5.26%   |
| 1001-2000      | 2         | 5.26%   |
| More than 3000 | 1         | 2.63%   |
| 2001-3000      | 1         | 2.63%   |
| 501-1000       | 1         | 2.63%   |
| Unknown        | 1         | 2.63%   |

Malfunc. Drives
---------------

Drive models with a malfunction

![Malfunc. Drives](./All/images/pie_chart/drive_malfunc.svg)


| Model                           | Computers | Drives | Percent |
|---------------------------------|-----------|--------|---------|
| WDC WD1001FALS-00J7B0 1TB       | 1         | 4      | 20%     |
| Seagate ST4000NM0033-9ZM170 4TB | 1         | 10     | 20%     |
| Seagate ST2000DM008-2FR102 2TB  | 1         | 2      | 20%     |
| Hitachi HTS727575A9E364 752GB   | 1         | 1      | 20%     |
| Corsair Neutron SSD 64GB        | 1         | 1      | 20%     |

Malfunc. Drive Vendor
---------------------

Vendors of faulty drives

![Malfunc. Drive Vendor](./All/images/pie_chart/drive_malfunc_vendor.svg)


| Vendor  | Computers | Drives | Percent |
|---------|-----------|--------|---------|
| Seagate | 2         | 12     | 40%     |
| WDC     | 1         | 4      | 20%     |
| Hitachi | 1         | 1      | 20%     |
| Corsair | 1         | 1      | 20%     |

Malfunc. HDD Vendor
-------------------

Vendors of faulty HDD drives

![Malfunc. HDD Vendor](./All/images/pie_chart/drive_malfunc_hdd_vendor.svg)


| Vendor  | Computers | Drives | Percent |
|---------|-----------|--------|---------|
| Seagate | 2         | 12     | 50%     |
| WDC     | 1         | 4      | 25%     |
| Hitachi | 1         | 1      | 25%     |

Malfunc. Drive Kind
-------------------

Kinds of faulty drives

![Malfunc. Drive Kind](./All/images/pie_chart/drive_malfunc_kind.svg)


| Kind | Computers | Drives | Percent |
|------|-----------|--------|---------|
| HDD  | 4         | 17     | 80%     |
| SSD  | 1         | 1      | 20%     |

Failed Drives
-------------

Failed drive models

Zero info for selected period =(

Failed Drive Vendor
-------------------

Failed drive vendors

Zero info for selected period =(

Drive Status
------------

Number of failed and malfunc. drives

![Drive Status](./All/images/pie_chart/drive_status.svg)


| Status   | Computers | Drives | Percent |
|----------|-----------|--------|---------|
| Works    | 19        | 41     | 45.24%  |
| Detected | 18        | 31     | 42.86%  |
| Malfunc  | 5         | 18     | 11.9%   |

Storage controller
------------------

Storage Vendor
--------------

Storage controller vendors

![Storage Vendor](./All/images/pie_chart/storage_vendor.svg)


| Vendor                      | Computers | Percent |
|-----------------------------|-----------|---------|
| Intel                       | 27        | 54%     |
| AMD                         | 8         | 16%     |
| Samsung Electronics         | 3         | 6%      |
| LSI Logic / Symbios Logic   | 3         | 6%      |
| Union Memory (Shenzhen)     | 1         | 2%      |
| Sandisk                     | 1         | 2%      |
| Realtek Semiconductor       | 1         | 2%      |
| Phison Electronics          | 1         | 2%      |
| Micron/Crucial Technology   | 1         | 2%      |
| Marvell Technology Group    | 1         | 2%      |
| Kingston Technology Company | 1         | 2%      |
| ASMedia Technology          | 1         | 2%      |
| Adaptec                     | 1         | 2%      |

Storage Model
-------------

Storage controller models

![Storage Model](./All/images/pie_chart/storage_model.svg)


| Model                                                                                   | Computers | Percent |
|-----------------------------------------------------------------------------------------|-----------|---------|
| AMD FCH SATA Controller [AHCI mode]                                                     | 7         | 11.48%  |
| Intel 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode]          | 4         | 6.56%   |
| Intel SATA Controller [RAID mode]                                                       | 3         | 4.92%   |
| AMD 400 Series Chipset SATA Controller                                                  | 3         | 4.92%   |
| LSI Logic / Symbios Logic MegaRAID SAS 1078                                             | 2         | 3.28%   |
| Intel C620 Series Chipset Family SSATA Controller [AHCI mode]                           | 2         | 3.28%   |
| Intel C620 Series Chipset Family SATA Controller [AHCI mode]                            | 2         | 3.28%   |
| Intel 82801IB (ICH9) 2 port SATA Controller [IDE mode]                                  | 2         | 3.28%   |
| Intel 8 Series SATA Controller 1 [AHCI mode]                                            | 2         | 3.28%   |
| Union Memory (Shenzhen) Non-Volatile memory controller                                  | 1         | 1.64%   |
| Sandisk Non-Volatile memory controller                                                  | 1         | 1.64%   |
| Samsung NVMe SSD Controller SM981/PM981/PM983                                           | 1         | 1.64%   |
| Samsung NVMe SSD Controller SM951/PM951                                                 | 1         | 1.64%   |
| Samsung NVMe SSD Controller PM9A1/PM9A3/980PRO                                          | 1         | 1.64%   |
| Realtek Realtek Non-Volatile memory controller                                          | 1         | 1.64%   |
| Phison E12 NVMe Controller                                                              | 1         | 1.64%   |
| Micron/Crucial P2 NVMe PCIe SSD                                                         | 1         | 1.64%   |
| Marvell Group 88SE6111/6121 SATA II / PATA Controller                                   | 1         | 1.64%   |
| LSI Logic / Symbios Logic MegaRAID SAS 2208 [Thunderbolt]                               | 1         | 1.64%   |
| Kingston Company Company Non-Volatile memory controller                                 | 1         | 1.64%   |
| Intel Sunrise Point-LP SATA Controller [AHCI mode]                                      | 1         | 1.64%   |
| Intel SSD 660P Series                                                                   | 1         | 1.64%   |
| Intel Q170/Q150/B150/H170/H110/Z170/CM236 Chipset SATA Controller [AHCI Mode]           | 1         | 1.64%   |
| Intel PCIe Data Center SSD                                                              | 1         | 1.64%   |
| Intel NVMe Datacenter SSD [3DNAND, Beta Rock Controller]                                | 1         | 1.64%   |
| Intel Mobile 4 Series Chipset PT IDER Controller                                        | 1         | 1.64%   |
| Intel HM170/QM170 Chipset SATA Controller [AHCI Mode]                                   | 1         | 1.64%   |
| Intel Celeron/Pentium Silver Processor SATA Controller                                  | 1         | 1.64%   |
| Intel C600/X79 series chipset SATA RAID Controller                                      | 1         | 1.64%   |
| Intel 9 Series Chipset Family SATA Controller [AHCI Mode]                               | 1         | 1.64%   |
| Intel 82801JI (ICH10 Family) 4 port SATA IDE Controller #1                              | 1         | 1.64%   |
| Intel 82801JI (ICH10 Family) 2 port SATA IDE Controller #2                              | 1         | 1.64%   |
| Intel 82801IBM/IEM (ICH9M/ICH9M-E) 4 port SATA Controller [AHCI mode]                   | 1         | 1.64%   |
| Intel 8 Series/C220 Series Chipset Family 2-port SATA Controller 2 [IDE mode]           | 1         | 1.64%   |
| Intel 8 Series Chipset Family 4-port SATA Controller 1 [IDE mode] - Mobile              | 1         | 1.64%   |
| Intel 6 Series/C200 Series Chipset Family Mobile SATA Controller (IDE mode, ports 4-5)  | 1         | 1.64%   |
| Intel 6 Series/C200 Series Chipset Family Mobile SATA Controller (IDE mode, ports 0-3)  | 1         | 1.64%   |
| Intel 6 Series/C200 Series Chipset Family Desktop SATA Controller (IDE mode, ports 4-5) | 1         | 1.64%   |
| Intel 6 Series/C200 Series Chipset Family Desktop SATA Controller (IDE mode, ports 0-3) | 1         | 1.64%   |
| Intel 6 Series/C200 Series Chipset Family 6 port Desktop SATA AHCI Controller           | 1         | 1.64%   |
| Intel 400 Series Chipset Family SATA AHCI Controller                                    | 1         | 1.64%   |
| ASMedia ASM1062 Serial ATA Controller                                                   | 1         | 1.64%   |
| Adaptec ASC-39320A U320                                                                 | 1         | 1.64%   |

Storage Kind
------------

Kind of storage controller (IDE, SATA, NVMe, SAS, ...)

![Storage Kind](./All/images/pie_chart/storage_kind.svg)


| Kind | Computers | Percent |
|------|-----------|---------|
| SATA | 24        | 47.06%  |
| NVMe | 12        | 23.53%  |
| RAID | 7         | 13.73%  |
| IDE  | 7         | 13.73%  |
| SCSI | 1         | 1.96%   |

Processor
---------

CPU Vendor
----------

Processor vendors

![CPU Vendor](./All/images/pie_chart/cpu_vendor.svg)


| Vendor | Computers | Percent |
|--------|-----------|---------|
| Intel  | 27        | 71.05%  |
| AMD    | 10        | 26.32%  |
| ARM    | 1         | 2.63%   |

CPU Model
---------

Processor models

![CPU Model](./All/images/pie_chart/cpu_model.svg)


| Model                                          | Computers | Percent |
|------------------------------------------------|-----------|---------|
| Intel Xeon CPU L5530 @ 2.40GHz                 | 2         | 5.26%   |
| Intel Core i7-6700HQ CPU @ 2.60GHz             | 2         | 5.26%   |
| Intel Core i7-4770 CPU @ 3.40GHz               | 2         | 5.26%   |
| Intel Xeon Silver 4216 CPU @ 2.10GHz           | 1         | 2.63%   |
| Intel Xeon Gold 6130 CPU @ 2.10GHz             | 1         | 2.63%   |
| Intel Xeon CPU E5620 @ 2.40GHz                 | 1         | 2.63%   |
| Intel Xeon CPU E5-2665 0 @ 2.40GHz             | 1         | 2.63%   |
| Intel Xeon CPU E5-2620 v2 @ 2.10GHz            | 1         | 2.63%   |
| Intel Core i7-6500U CPU @ 2.50GHz              | 1         | 2.63%   |
| Intel Core i7-4900MQ CPU @ 2.80GHz             | 1         | 2.63%   |
| Intel Core i7-4810MQ CPU @ 2.80GHz             | 1         | 2.63%   |
| Intel Core i7-4790K CPU @ 4.00GHz              | 1         | 2.63%   |
| Intel Core i7-4790 CPU @ 3.60GHz               | 1         | 2.63%   |
| Intel Core i7-4720HQ CPU @ 2.60GHz             | 1         | 2.63%   |
| Intel Core i7-4700MQ CPU @ 2.40GHz             | 1         | 2.63%   |
| Intel Core i7-10750H CPU @ 2.60GHz             | 1         | 2.63%   |
| Intel Core i5-4210U CPU @ 1.70GHz              | 1         | 2.63%   |
| Intel Core i5-3470 CPU @ 3.20GHz               | 1         | 2.63%   |
| Intel Core i5-2520M CPU @ 2.50GHz              | 1         | 2.63%   |
| Intel Core i3-2120 CPU @ 3.30GHz               | 1         | 2.63%   |
| Intel Core 2 Quad CPU Q8200 @ 2.33GHz          | 1         | 2.63%   |
| Intel Core 2 Duo CPU T9600 @ 2.80GHz           | 1         | 2.63%   |
| Intel Celeron J4125 CPU @ 2.00GHz              | 1         | 2.63%   |
| Intel Celeron 2955U @ 1.40GHz                  | 1         | 2.63%   |
| ARM Processor                                  | 1         | 2.63%   |
| AMD Ryzen Threadripper 3960X 24-Core Processor | 1         | 2.63%   |
| AMD Ryzen 9 5900X 12-Core Processor            | 1         | 2.63%   |
| AMD Ryzen 7 5700U with Radeon Graphics         | 1         | 2.63%   |
| AMD Ryzen 7 4800H with Radeon Graphics         | 1         | 2.63%   |
| AMD Ryzen 7 2700 Eight-Core Processor          | 1         | 2.63%   |
| AMD Ryzen 5 4600H with Radeon Graphics         | 1         | 2.63%   |
| AMD Ryzen 5 3600 6-Core Processor              | 1         | 2.63%   |
| AMD Ryzen 5 3500U with Radeon Vega Mobile Gfx  | 1         | 2.63%   |
| AMD Ryzen 3 3200G with Radeon Vega Graphics    | 1         | 2.63%   |
| AMD A9-9420e RADEON R5, 5 COMPUTE CORES 2C+3G  | 1         | 2.63%   |

CPU Model Family
----------------

Processor model prefix

![CPU Model Family](./All/images/pie_chart/cpu_family.svg)


| Model                  | Computers | Percent |
|------------------------|-----------|---------|
| Intel Core i7          | 12        | 31.58%  |
| Intel Xeon             | 5         | 13.16%  |
| Intel Core i5          | 3         | 7.89%   |
| AMD Ryzen 7            | 3         | 7.89%   |
| AMD Ryzen 5            | 3         | 7.89%   |
| Other                  | 2         | 5.26%   |
| Intel Celeron          | 2         | 5.26%   |
| Intel Xeon Silver      | 1         | 2.63%   |
| Intel Xeon Gold        | 1         | 2.63%   |
| Intel Core i3          | 1         | 2.63%   |
| Intel Core 2 Quad      | 1         | 2.63%   |
| Intel Core 2 Duo       | 1         | 2.63%   |
| AMD Ryzen Threadripper | 1         | 2.63%   |
| AMD Ryzen 9            | 1         | 2.63%   |
| AMD Ryzen 3            | 1         | 2.63%   |

CPU Cores
---------

Number of processor cores

![CPU Cores](./All/images/pie_chart/cpu_cores.svg)


| Number | Computers | Percent |
|--------|-----------|---------|
| 4      | 16        | 42.11%  |
| 2      | 7         | 18.42%  |
| 8      | 6         | 15.79%  |
| 6      | 3         | 7.89%   |
| 16     | 2         | 5.26%   |
| 12     | 2         | 5.26%   |
| 32     | 1         | 2.63%   |
| 24     | 1         | 2.63%   |

CPU Sockets
-----------

Number of sockets

![CPU Sockets](./All/images/pie_chart/cpu_sockets.svg)


| Number | Computers | Percent |
|--------|-----------|---------|
| 1      | 32        | 84.21%  |
| 2      | 6         | 15.79%  |

CPU Threads
-----------

Threads per core (Hyper-Threading)

![CPU Threads](./All/images/pie_chart/cpu_threads.svg)


| Number | Computers | Percent |
|--------|-----------|---------|
| 2      | 26        | 68.42%  |
| 1      | 12        | 31.58%  |

CPU Op-Modes
------------

CPU Operation Modes (32-bit, 64-bit)

![CPU Op-Modes](./All/images/pie_chart/cpu_op_modes.svg)


| Op mode        | Computers | Percent |
|----------------|-----------|---------|
| 32-bit, 64-bit | 38        | 100%    |

CPU Microcode
-------------

Microcode number

![CPU Microcode](./All/images/pie_chart/cpu_microcode.svg)


| Number     | Computers | Percent |
|------------|-----------|---------|
| 0x306c3    | 7         | 18.42%  |
| 0x506e3    | 2         | 5.26%   |
| 0x40651    | 2         | 5.26%   |
| 0x206a7    | 2         | 5.26%   |
| 0x106a5    | 2         | 5.26%   |
| 0x08600104 | 2         | 5.26%   |
| Unknown    | 2         | 5.26%   |
| 0xa0652    | 1         | 2.63%   |
| 0x706a8    | 1         | 2.63%   |
| 0x50657    | 1         | 2.63%   |
| 0x50654    | 1         | 2.63%   |
| 0x406e3    | 1         | 2.63%   |
| 0x306e4    | 1         | 2.63%   |
| 0x306a9    | 1         | 2.63%   |
| 0x206d7    | 1         | 2.63%   |
| 0x206c2    | 1         | 2.63%   |
| 0x10677    | 1         | 2.63%   |
| 0x10676    | 1         | 2.63%   |
| 0x0a201009 | 1         | 2.63%   |
| 0x0870100a | 1         | 2.63%   |
| 0x08608103 | 1         | 2.63%   |
| 0x08301039 | 1         | 2.63%   |
| 0x08108109 | 1         | 2.63%   |
| 0x08108102 | 1         | 2.63%   |
| 0x0800820d | 1         | 2.63%   |
| 0x06006705 | 1         | 2.63%   |

CPU Microarch
-------------

Microarchitecture

![CPU Microarch](./All/images/pie_chart/cpu_microarch.svg)


| Name          | Computers | Percent |
|---------------|-----------|---------|
| Haswell       | 10        | 26.32%  |
| Skylake       | 5         | 13.16%  |
| Zen 2         | 4         | 10.53%  |
| Zen+          | 3         | 7.89%   |
| SandyBridge   | 3         | 7.89%   |
| Penryn        | 2         | 5.26%   |
| Nehalem       | 2         | 5.26%   |
| IvyBridge     | 2         | 5.26%   |
| Unknown       | 2         | 5.26%   |
| Zen 3         | 1         | 2.63%   |
| Westmere      | 1         | 2.63%   |
| Goldmont plus | 1         | 2.63%   |
| Excavator     | 1         | 2.63%   |
| CometLake     | 1         | 2.63%   |

Graphics
--------

GPU Vendor
----------

Vendors of graphics cards

![GPU Vendor](./All/images/pie_chart/gpu_vendor.svg)


| Vendor                     | Computers | Percent |
|----------------------------|-----------|---------|
| Nvidia                     | 20        | 41.67%  |
| Intel                      | 15        | 31.25%  |
| AMD                        | 8         | 16.67%  |
| Matrox Electronics Systems | 3         | 6.25%   |
| ASPEED Technology          | 2         | 4.17%   |

GPU Model
---------

Graphics card models

![GPU Model](./All/images/pie_chart/gpu_model.svg)


| Model                                                                       | Computers | Percent |
|-----------------------------------------------------------------------------|-----------|---------|
| Intel 4th Gen Core Processor Integrated Graphics Controller                 | 3         | 6.25%   |
| Nvidia TU106M [GeForce RTX 2060 Mobile]                                     | 2         | 4.17%   |
| Nvidia GK106GLM [Quadro K2100M]                                             | 2         | 4.17%   |
| Matrox Electronics Systems MGA G200eW WPCM450                               | 2         | 4.17%   |
| Intel Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller | 2         | 4.17%   |
| Intel Haswell-ULT Integrated Graphics Controller                            | 2         | 4.17%   |
| Intel 2nd Generation Core Processor Family Integrated Graphics Controller   | 2         | 4.17%   |
| ASPEED Technology ASPEED Graphics Family                                    | 2         | 4.17%   |
| AMD RV620 LE [Radeon HD 3450]                                               | 2         | 4.17%   |
| AMD Picasso/Raven 2 [Radeon Vega Series / Radeon Vega Mobile Series]        | 2         | 4.17%   |
| Nvidia TU117M [GeForce GTX 1650 Ti Mobile]                                  | 1         | 2.08%   |
| Nvidia TU117GL [T600]                                                       | 1         | 2.08%   |
| Nvidia GT218 [GeForce 210]                                                  | 1         | 2.08%   |
| Nvidia GP107GL [Quadro P400]                                                | 1         | 2.08%   |
| Nvidia GP102 [GeForce GTX 1080 Ti]                                          | 1         | 2.08%   |
| Nvidia GM200 [GeForce GTX 980 Ti]                                           | 1         | 2.08%   |
| Nvidia GM108M [GeForce 940M]                                                | 1         | 2.08%   |
| Nvidia GM107M [GeForce GTX 960M]                                            | 1         | 2.08%   |
| Nvidia GM107M [GeForce GTX 860M]                                            | 1         | 2.08%   |
| Nvidia GM107GLM [Quadro M1000M]                                             | 1         | 2.08%   |
| Nvidia GK208B [GeForce GT 730]                                              | 1         | 2.08%   |
| Nvidia GK107M [GeForce GT 755M]                                             | 1         | 2.08%   |
| Nvidia GK107GL [Quadro K2000]                                               | 1         | 2.08%   |
| Nvidia GK107 [GeForce GTX 650]                                              | 1         | 2.08%   |
| Nvidia GK104GL [GRID K2]                                                    | 1         | 2.08%   |
| Nvidia GF119M [Quadro NVS 4200M]                                            | 1         | 2.08%   |
| Matrox Electronics Systems G200eR2                                          | 1         | 2.08%   |
| Intel Xeon E3-1200 v2/3rd Gen Core processor Graphics Controller            | 1         | 2.08%   |
| Intel Skylake GT2 [HD Graphics 520]                                         | 1         | 2.08%   |
| Intel Mobile 4 Series Chipset Integrated Graphics Controller                | 1         | 2.08%   |
| Intel HD Graphics 530                                                       | 1         | 2.08%   |
| Intel GeminiLake [UHD Graphics 600]                                         | 1         | 2.08%   |
| Intel CometLake-H GT2 [UHD Graphics]                                        | 1         | 2.08%   |
| AMD Stoney [Radeon R2/R3/R4/R5 Graphics]                                    | 1         | 2.08%   |
| AMD RV635/M86 [Mobility Radeon HD 3650]                                     | 1         | 2.08%   |
| AMD Renoir                                                                  | 1         | 2.08%   |
| AMD Lucienne                                                                | 1         | 2.08%   |

GPU Combo
---------

Combinations of graphics cards

![GPU Combo](./All/images/pie_chart/gpu_combo.svg)


| Name            | Computers | Percent |
|-----------------|-----------|---------|
| 1 x Nvidia      | 10        | 26.32%  |
| Intel + Nvidia  | 8         | 21.05%  |
| 1 x Intel       | 6         | 15.79%  |
| 1 x AMD         | 6         | 15.79%  |
| 1 x Matrox      | 2         | 5.26%   |
| 1 x ASPEED      | 2         | 5.26%   |
| Other           | 1         | 2.63%   |
| Nvidia + Matrox | 1         | 2.63%   |
| Intel + AMD     | 1         | 2.63%   |
| AMD + Nvidia    | 1         | 2.63%   |

GPU Driver
----------

Free vs proprietary

![GPU Driver](./All/images/pie_chart/gpu_driver.svg)


| Driver      | Computers | Percent |
|-------------|-----------|---------|
| Free        | 26        | 68.42%  |
| Proprietary | 9         | 23.68%  |
| Unknown     | 3         | 7.89%   |

GPU Memory
----------

Total video memory

![GPU Memory](./All/images/pie_chart/gpu_memory.svg)


| Size in GB | Computers | Percent |
|------------|-----------|---------|
| Unknown    | 16        | 42.11%  |
| 1.01-2.0   | 7         | 18.42%  |
| 0.01-0.5   | 7         | 18.42%  |
| 3.01-4.0   | 4         | 10.53%  |
| 5.01-6.0   | 3         | 7.89%   |
| 8.01-16.0  | 1         | 2.63%   |

Monitor
-------

Monitor Vendor
--------------

Monitor vendors

![Monitor Vendor](./All/images/pie_chart/mon_vendor.svg)


| Vendor              | Computers | Percent |
|---------------------|-----------|---------|
| Dell                | 7         | 18.92%  |
| Samsung Electronics | 4         | 10.81%  |
| LG Display          | 4         | 10.81%  |
| AU Optronics        | 4         | 10.81%  |
| Goldstar            | 3         | 8.11%   |
| Chimei Innolux      | 3         | 8.11%   |
| Philips             | 2         | 5.41%   |
| Iiyama              | 2         | 5.41%   |
| Sony                | 1         | 2.7%    |
| PANDA               | 1         | 2.7%    |
| Panasonic           | 1         | 2.7%    |
| Lenovo              | 1         | 2.7%    |
| BOE                 | 1         | 2.7%    |
| ASUSTek Computer    | 1         | 2.7%    |
| AOC                 | 1         | 2.7%    |
| ADR                 | 1         | 2.7%    |

Monitor Model
-------------

Monitor models

![Monitor Model](./All/images/pie_chart/mon_model.svg)


| Model                                                                 | Computers | Percent |
|-----------------------------------------------------------------------|-----------|---------|
| Sony LG TV SNY045B 1920x540                                           | 1         | 2.56%   |
| Samsung Electronics U32R59x SAM0F96 3840x2160 700x390mm 31.5-inch     | 1         | 2.56%   |
| Samsung Electronics LF27T450F SAM7099 1920x1080 597x336mm 27.0-inch   | 1         | 2.56%   |
| Samsung Electronics LCD Monitor SDC3752 1920x1080 344x194mm 15.5-inch | 1         | 2.56%   |
| Samsung Electronics C49RG9x SAM0F9C 3840x1080 1193x336mm 48.8-inch    | 1         | 2.56%   |
| Philips PHL 272S4L PHL08E4 1920x1080 600x340mm 27.2-inch              | 1         | 2.56%   |
| Philips PHL 271S7Q PHL090A 1920x1080 600x340mm 27.2-inch              | 1         | 2.56%   |
| Philips 226V4 PHLC0B1 1920x1080 477x268mm 21.5-inch                   | 1         | 2.56%   |
| PANDA LCD Monitor NCP004D 1920x1080 344x194mm 15.5-inch               | 1         | 2.56%   |
| Panasonic VVX10T025J00 MEI96A2 2560x1600 223x125mm 10.1-inch          | 1         | 2.56%   |
| LG Display LCD Monitor LGD04D5 1920x1080 344x194mm 15.5-inch          | 1         | 2.56%   |
| LG Display LCD Monitor LGD046F 1920x1080 344x194mm 15.5-inch          | 1         | 2.56%   |
| LG Display LCD Monitor LGD0406 1920x1080 309x175mm 14.0-inch          | 1         | 2.56%   |
| LG Display LCD Monitor LGD02E2 1600x900 310x174mm 14.0-inch           | 1         | 2.56%   |
| Lenovo LCD Monitor LEN4055 1920x1200 331x207mm 15.4-inch              | 1         | 2.56%   |
| Iiyama PL2483H IVM6138 1920x1080 531x299mm 24.0-inch                  | 1         | 2.56%   |
| Iiyama PL2377 IVM561D 1920x1080 510x287mm 23.0-inch                   | 1         | 2.56%   |
| Goldstar LG UltraFine GSM5B11                                         | 1         | 2.56%   |
| Goldstar HDR WFHD GSM7714 2560x1080 798x334mm 34.1-inch               | 1         | 2.56%   |
| Goldstar 32ML600 GSM772D 1920x1080 480x270mm 21.7-inch                | 1         | 2.56%   |
| Dell S2740L DELA08E 1920x1080 598x336mm 27.0-inch                     | 1         | 2.56%   |
| Dell P2715Q DEL40BD 3840x2160 600x340mm 27.2-inch                     | 1         | 2.56%   |
| Dell P2014H DEL4096 1600x900 434x236mm 19.4-inch                      | 1         | 2.56%   |
| Dell LCD Monitor U2414H 3840x1080                                     | 1         | 2.56%   |
| Dell LCD Monitor U2414H                                               | 1         | 2.56%   |
| Dell IN2030M DELF03C 1600x900 443x249mm 20.0-inch                     | 1         | 2.56%   |
| Dell E177FP DELA023 1280x1024 338x270mm 17.0-inch                     | 1         | 2.56%   |
| Dell 1703FP DEL3011 1280x1024 338x270mm 17.0-inch                     | 1         | 2.56%   |
| Chimei Innolux LCD Monitor CMN15C3 1920x1080 344x193mm 15.5-inch      | 1         | 2.56%   |
| Chimei Innolux LCD Monitor CMN14A7 1920x1080 308x173mm 13.9-inch      | 1         | 2.56%   |
| Chimei Innolux LCD Monitor CMN1406 1920x1080 309x173mm 13.9-inch      | 1         | 2.56%   |
| BOE LCD Monitor BOE0900 1920x1080 344x194mm 15.5-inch                 | 1         | 2.56%   |
| AU Optronics LCD Monitor AUOB78D 1920x1080 344x193mm 15.5-inch        | 1         | 2.56%   |
| AU Optronics LCD Monitor AUO203E 1600x900 309x174mm 14.0-inch         | 1         | 2.56%   |
| AU Optronics LCD Monitor AUO203D 1920x1080 309x174mm 14.0-inch        | 1         | 2.56%   |
| AU Optronics LCD Monitor AUO109D 1920x1080 381x214mm 17.2-inch        | 1         | 2.56%   |
| ASUSTek Computer VP247 AUS24DA 1920x1080 521x293mm 23.5-inch          | 1         | 2.56%   |
| AOC 2279WH AOC2279 1920x1080 477x268mm 21.5-inch                      | 1         | 2.56%   |
| ADR KVM-via-IP ADR0219 1280x1024                                      | 1         | 2.56%   |

Monitor Resolution
------------------

Monitor screen resolution

![Monitor Resolution](./All/images/pie_chart/mon_resolution.svg)


| Resolution        | Computers | Percent |
|-------------------|-----------|---------|
| 1920x1080 (FHD)   | 16        | 48.48%  |
| 1600x900 (HD+)    | 4         | 12.12%  |
| 1280x1024 (SXGA)  | 3         | 9.09%   |
| 3840x2160 (4K)    | 2         | 6.06%   |
| 3840x1080         | 2         | 6.06%   |
| Unknown           | 2         | 6.06%   |
| 2560x1440 (QHD)   | 1         | 3.03%   |
| 2560x1080         | 1         | 3.03%   |
| 1920x540          | 1         | 3.03%   |
| 1920x1200 (WUXGA) | 1         | 3.03%   |

Monitor Diagonal
----------------

Diagonal size in inches

![Monitor Diagonal](./All/images/pie_chart/mon_diagonal.svg)


| Inches  | Computers | Percent |
|---------|-----------|---------|
| 15      | 8         | 21.62%  |
| 27      | 5         | 13.51%  |
| 17      | 4         | 10.81%  |
| 14      | 4         | 10.81%  |
| 21      | 3         | 8.11%   |
| 23      | 2         | 5.41%   |
| 13      | 2         | 5.41%   |
| Unknown | 2         | 5.41%   |
| 65      | 1         | 2.7%    |
| 48      | 1         | 2.7%    |
| 34      | 1         | 2.7%    |
| 31      | 1         | 2.7%    |
| 24      | 1         | 2.7%    |
| 20      | 1         | 2.7%    |
| 19      | 1         | 2.7%    |

Monitor Width
-------------

Physical width

![Monitor Width](./All/images/pie_chart/mon_width.svg)


| Width in mm | Computers | Percent |
|-------------|-----------|---------|
| 301-350     | 16        | 43.24%  |
| 501-600     | 8         | 21.62%  |
| 401-500     | 5         | 13.51%  |
| 351-400     | 2         | 5.41%   |
| 1001-1500   | 2         | 5.41%   |
| Unknown     | 2         | 5.41%   |
| 701-800     | 1         | 2.7%    |
| 601-700     | 1         | 2.7%    |

Aspect Ratio
------------

Proportional relationship between the width and the height

![Aspect Ratio](./All/images/pie_chart/mon_ratio.svg)


| Ratio   | Computers | Percent |
|---------|-----------|---------|
| 16/9    | 23        | 76.67%  |
| 5/4     | 3         | 10%     |
| 32/9    | 1         | 3.33%   |
| 21/9    | 1         | 3.33%   |
| 16/10   | 1         | 3.33%   |
| Unknown | 1         | 3.33%   |

Monitor Area
------------

Area in inch²

![Monitor Area](./All/images/pie_chart/mon_area.svg)


| Area in inch² | Computers | Percent |
|----------------|-----------|---------|
| 101-110        | 8         | 21.62%  |
| 81-90          | 6         | 16.22%  |
| 301-350        | 5         | 13.51%  |
| 201-250        | 5         | 13.51%  |
| 151-200        | 3         | 8.11%   |
| 351-500        | 2         | 5.41%   |
| 141-150        | 2         | 5.41%   |
| 121-130        | 2         | 5.41%   |
| Unknown        | 2         | 5.41%   |
| More than 1000 | 1         | 2.7%    |
| 501-1000       | 1         | 2.7%    |

Pixel Density
-------------

Pixels per inch

![Pixel Density](./All/images/pie_chart/mon_density.svg)


| Density       | Computers | Percent |
|---------------|-----------|---------|
| 121-160       | 16        | 43.24%  |
| 51-100        | 12        | 32.43%  |
| 101-120       | 4         | 10.81%  |
| Unknown       | 2         | 5.41%   |
| More than 240 | 1         | 2.7%    |
| 1-50          | 1         | 2.7%    |
| 161-240       | 1         | 2.7%    |

Multiple Monitors
-----------------

Total monitors connected

![Multiple Monitors](./All/images/pie_chart/mon_total.svg)


| Total | Computers | Percent |
|-------|-----------|---------|
| 1     | 21        | 55.26%  |
| 0     | 9         | 23.68%  |
| 2     | 6         | 15.79%  |
| 3     | 2         | 5.26%   |

Network
-------

Net Controller Vendor
---------------------

Controller vendors

![Net Controller Vendor](./All/images/pie_chart/net_vendor.svg)


| Vendor                    | Computers | Percent |
|---------------------------|-----------|---------|
| Intel                     | 23        | 42.59%  |
| Realtek Semiconductor     | 15        | 27.78%  |
| Qualcomm Atheros          | 4         | 7.41%   |
| Broadcom                  | 4         | 7.41%   |
| Mellanox Technologies     | 3         | 5.56%   |
| Solarflare Communications | 1         | 1.85%   |
| Microsoft                 | 1         | 1.85%   |
| Marvell Technology Group  | 1         | 1.85%   |
| BUFFALO                   | 1         | 1.85%   |
| Broadcom Limited          | 1         | 1.85%   |

Net Controller Model
--------------------

Controller models

![Net Controller Model](./All/images/pie_chart/net_model.svg)


| Model                                                                                                                  | Computers | Percent |
|------------------------------------------------------------------------------------------------------------------------|-----------|---------|
| Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller                                                      | 15        | 23.08%  |
| Intel Wireless 7260                                                                                                    | 4         | 6.15%   |
| Intel Ethernet Connection I217-LM                                                                                      | 4         | 6.15%   |
| Intel Wi-Fi 6 AX200                                                                                                    | 3         | 4.62%   |
| Mellanox MT25408A0-FCC-QI ConnectX, Dual Port 40Gb/s InfiniBand / 10GigE Adapter IC with PCIe 2.0 x8 5.0GT/s Interface | 2         | 3.08%   |
| Intel Wireless 8260                                                                                                    | 2         | 3.08%   |
| Intel I211 Gigabit Network Connection                                                                                  | 2         | 3.08%   |
| Intel Ethernet Connection X722 for 10GBASE-T                                                                           | 2         | 3.08%   |
| Intel 82579LM Gigabit Network Connection (Lewisville)                                                                  | 2         | 3.08%   |
| Broadcom NetXtreme II BCM5709 Gigabit Ethernet                                                                         | 2         | 3.08%   |
| Solarflare SFC9020 10G Ethernet Controller                                                                             | 1         | 1.54%   |
| Realtek RTL88x2bu [AC1200 Techkey]                                                                                     | 1         | 1.54%   |
| Realtek RTL8723DE Wireless Network Adapter                                                                             | 1         | 1.54%   |
| Realtek 802.11ac NIC                                                                                                   | 1         | 1.54%   |
| Qualcomm Atheros QCA9377 802.11ac Wireless Network Adapter                                                             | 1         | 1.54%   |
| Qualcomm Atheros QCA8171 Gigabit Ethernet                                                                              | 1         | 1.54%   |
| Qualcomm Atheros AR9462 Wireless Network Adapter                                                                       | 1         | 1.54%   |
| Qualcomm Atheros AR93xx Wireless Network Adapter                                                                       | 1         | 1.54%   |
| Microsoft Xbox 360 Wireless Adapter                                                                                    | 1         | 1.54%   |
| Mellanox MT27700 Family [ConnectX-4]                                                                                   | 1         | 1.54%   |
| Marvell Group 88E8056 PCI-E Gigabit Ethernet Controller                                                                | 1         | 1.54%   |
| Marvell Group 88E8001 Gigabit Ethernet Controller                                                                      | 1         | 1.54%   |
| Intel Wireless 7265                                                                                                    | 1         | 1.54%   |
| Intel Wireless 3165                                                                                                    | 1         | 1.54%   |
| Intel Wireless 3160                                                                                                    | 1         | 1.54%   |
| Intel Ultimate N WiFi Link 5300                                                                                        | 1         | 1.54%   |
| Intel Ethernet Virtual Function 700 Series                                                                             | 1         | 1.54%   |
| Intel Ethernet Connection I217-V                                                                                       | 1         | 1.54%   |
| Intel Ethernet Connection (2) I219-LM                                                                                  | 1         | 1.54%   |
| Intel Dual Band Wireless-AC 3165 Plus Bluetooth                                                                        | 1         | 1.54%   |
| Intel Comet Lake PCH CNVi WiFi                                                                                         | 1         | 1.54%   |
| Intel Centrino Advanced-N 6205 [Taylor Peak]                                                                           | 1         | 1.54%   |
| Intel 82567LM Gigabit Network Connection                                                                               | 1         | 1.54%   |
| BUFFALO WLI-UC-GNM2 Wireless LAN Adapter [Ralink RT3070]                                                               | 1         | 1.54%   |
| Broadcom NetXtreme BCM57762 Gigabit Ethernet PCIe                                                                      | 1         | 1.54%   |
| Broadcom NetXtreme BCM5764M Gigabit Ethernet PCIe                                                                      | 1         | 1.54%   |
| Broadcom Limited NetXtreme II BCM57800 1/10 Gigabit Ethernet                                                           | 1         | 1.54%   |

Wireless Vendor
---------------

Wireless vendors

![Wireless Vendor](./All/images/pie_chart/net_wireless_vendor.svg)


| Vendor                | Computers | Percent |
|-----------------------|-----------|---------|
| Intel                 | 16        | 69.57%  |
| Qualcomm Atheros      | 3         | 13.04%  |
| Realtek Semiconductor | 2         | 8.7%    |
| Microsoft             | 1         | 4.35%   |
| BUFFALO               | 1         | 4.35%   |

Wireless Model
--------------

Wireless models

![Wireless Model](./All/images/pie_chart/net_wireless_model.svg)


| Model                                                      | Computers | Percent |
|------------------------------------------------------------|-----------|---------|
| Intel Wireless 7260                                        | 4         | 16.67%  |
| Intel Wi-Fi 6 AX200                                        | 3         | 12.5%   |
| Intel Wireless 8260                                        | 2         | 8.33%   |
| Realtek RTL88x2bu [AC1200 Techkey]                         | 1         | 4.17%   |
| Realtek RTL8723DE Wireless Network Adapter                 | 1         | 4.17%   |
| Realtek 802.11ac NIC                                       | 1         | 4.17%   |
| Qualcomm Atheros QCA9377 802.11ac Wireless Network Adapter | 1         | 4.17%   |
| Qualcomm Atheros AR9462 Wireless Network Adapter           | 1         | 4.17%   |
| Qualcomm Atheros AR93xx Wireless Network Adapter           | 1         | 4.17%   |
| Microsoft Xbox 360 Wireless Adapter                        | 1         | 4.17%   |
| Intel Wireless 7265                                        | 1         | 4.17%   |
| Intel Wireless 3165                                        | 1         | 4.17%   |
| Intel Wireless 3160                                        | 1         | 4.17%   |
| Intel Ultimate N WiFi Link 5300                            | 1         | 4.17%   |
| Intel Dual Band Wireless-AC 3165 Plus Bluetooth            | 1         | 4.17%   |
| Intel Comet Lake PCH CNVi WiFi                             | 1         | 4.17%   |
| Intel Centrino Advanced-N 6205 [Taylor Peak]               | 1         | 4.17%   |
| BUFFALO WLI-UC-GNM2 Wireless LAN Adapter [Ralink RT3070]   | 1         | 4.17%   |

Ethernet Vendor
---------------

Ethernet vendors

![Ethernet Vendor](./All/images/pie_chart/net_ethernet_vendor.svg)


| Vendor                    | Computers | Percent |
|---------------------------|-----------|---------|
| Realtek Semiconductor     | 15        | 41.67%  |
| Intel                     | 13        | 36.11%  |
| Broadcom                  | 4         | 11.11%  |
| Solarflare Communications | 1         | 2.78%   |
| Qualcomm Atheros          | 1         | 2.78%   |
| Marvell Technology Group  | 1         | 2.78%   |
| Broadcom Limited          | 1         | 2.78%   |

Ethernet Model
--------------

Ethernet models

![Ethernet Model](./All/images/pie_chart/net_ethernet_model.svg)


| Model                                                             | Computers | Percent |
|-------------------------------------------------------------------|-----------|---------|
| Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller | 15        | 39.47%  |
| Intel Ethernet Connection I217-LM                                 | 4         | 10.53%  |
| Intel I211 Gigabit Network Connection                             | 2         | 5.26%   |
| Intel Ethernet Connection X722 for 10GBASE-T                      | 2         | 5.26%   |
| Intel 82579LM Gigabit Network Connection (Lewisville)             | 2         | 5.26%   |
| Broadcom NetXtreme II BCM5709 Gigabit Ethernet                    | 2         | 5.26%   |
| Solarflare SFC9020 10G Ethernet Controller                        | 1         | 2.63%   |
| Qualcomm Atheros QCA8171 Gigabit Ethernet                         | 1         | 2.63%   |
| Marvell Group 88E8056 PCI-E Gigabit Ethernet Controller           | 1         | 2.63%   |
| Marvell Group 88E8001 Gigabit Ethernet Controller                 | 1         | 2.63%   |
| Intel Ethernet Virtual Function 700 Series                        | 1         | 2.63%   |
| Intel Ethernet Connection I217-V                                  | 1         | 2.63%   |
| Intel Ethernet Connection (2) I219-LM                             | 1         | 2.63%   |
| Intel 82567LM Gigabit Network Connection                          | 1         | 2.63%   |
| Broadcom NetXtreme BCM57762 Gigabit Ethernet PCIe                 | 1         | 2.63%   |
| Broadcom NetXtreme BCM5764M Gigabit Ethernet PCIe                 | 1         | 2.63%   |
| Broadcom Limited NetXtreme II BCM57800 1/10 Gigabit Ethernet      | 1         | 2.63%   |

Net Controller Kind
-------------------

Ethernet, WiFi or modem

![Net Controller Kind](./All/images/pie_chart/net_kind.svg)


| Kind     | Computers | Percent |
|----------|-----------|---------|
| Ethernet | 35        | 57.38%  |
| WiFi     | 23        | 37.7%   |
| Unknown  | 3         | 4.92%   |

Used Controller
---------------

Currently used network controller

![Used Controller](./All/images/pie_chart/net_used.svg)


| Kind     | Computers | Percent |
|----------|-----------|---------|
| Ethernet | 30        | 63.83%  |
| WiFi     | 16        | 34.04%  |
| Unknown  | 1         | 2.13%   |

NICs
----

Total network controllers on board

![NICs](./All/images/pie_chart/net_nics.svg)


| Total | Computers | Percent |
|-------|-----------|---------|
| 2     | 19        | 50%     |
| 1     | 12        | 31.58%  |
| 5     | 2         | 5.26%   |
| 3     | 2         | 5.26%   |
| 66    | 1         | 2.63%   |
| 4     | 1         | 2.63%   |
| 0     | 1         | 2.63%   |

IPv6
----

IPv6 vs IPv4

![IPv6](./All/images/pie_chart/node_ipv6.svg)


| Used | Computers | Percent |
|------|-----------|---------|
| No   | 29        | 76.32%  |
| Yes  | 9         | 23.68%  |

Bluetooth
---------

Bluetooth Vendor
----------------

Controller vendors

![Bluetooth Vendor](./All/images/pie_chart/bt_vendor.svg)


| Vendor                          | Computers | Percent |
|---------------------------------|-----------|---------|
| Intel                           | 14        | 70%     |
| Broadcom                        | 2         | 10%     |
| Realtek Semiconductor           | 1         | 5%      |
| Qualcomm Atheros Communications | 1         | 5%      |
| IMC Networks                    | 1         | 5%      |
| Cambridge Silicon Radio         | 1         | 5%      |

Bluetooth Model
---------------

Controller models

![Bluetooth Model](./All/images/pie_chart/bt_model.svg)


| Model                                               | Computers | Percent |
|-----------------------------------------------------|-----------|---------|
| Intel Bluetooth wireless interface                  | 8         | 40%     |
| Intel AX200 Bluetooth                               | 3         | 15%     |
| Intel Bluetooth Device                              | 2         | 10%     |
| Realtek  Bluetooth 4.2 Adapter                      | 1         | 5%      |
| Qualcomm Atheros  Bluetooth Device                  | 1         | 5%      |
| Intel AX201 Bluetooth                               | 1         | 5%      |
| IMC Networks Bluetooth Device                       | 1         | 5%      |
| Cambridge Silicon Radio Bluetooth Dongle (HCI mode) | 1         | 5%      |
| Broadcom BCM20702A0 Bluetooth 4.0                   | 1         | 5%      |
| Broadcom BCM2045B (BDC-2.1) [Bluetooth Controller]  | 1         | 5%      |

Sound
-----

Sound Vendor
------------

Sound card vendors

![Sound Vendor](./All/images/pie_chart/snd_vendor.svg)


| Vendor              | Computers | Percent |
|---------------------|-----------|---------|
| Intel               | 22        | 40%     |
| Nvidia              | 16        | 29.09%  |
| AMD                 | 11        | 20%     |
| Logitech            | 2         | 3.64%   |
| C-Media Electronics | 2         | 3.64%   |
| Conexant Systems    | 1         | 1.82%   |
| ASUSTek Computer    | 1         | 1.82%   |

Sound Model
-----------

Sound card models

![Sound Model](./All/images/pie_chart/snd_model.svg)


| Model                                                                      | Computers | Percent |
|----------------------------------------------------------------------------|-----------|---------|
| Intel 8 Series/C220 Series Chipset High Definition Audio Controller        | 7         | 10.29%  |
| Intel Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller           | 5         | 7.35%   |
| AMD Family 17h/19h HD Audio Controller                                     | 5         | 7.35%   |
| Nvidia GK107 HDMI Audio Controller                                         | 3         | 4.41%   |
| Intel 6 Series/C200 Series Chipset Family High Definition Audio Controller | 3         | 4.41%   |
| AMD Starship/Matisse HD Audio Controller                                   | 3         | 4.41%   |
| Nvidia TU107 GeForce GTX 1650 High Definition Audio Controller             | 2         | 2.94%   |
| Nvidia TU106 High Definition Audio Controller                              | 2         | 2.94%   |
| Nvidia GK106 HDMI Audio Controller                                         | 2         | 2.94%   |
| Intel Haswell-ULT HD Audio Controller                                      | 2         | 2.94%   |
| Intel 82801JI (ICH10 Family) HD Audio Controller                           | 2         | 2.94%   |
| Intel 8 Series HD Audio Controller                                         | 2         | 2.94%   |
| Intel 100 Series/C230 Series Chipset Family HD Audio Controller            | 2         | 2.94%   |
| AMD RV620 HDMI Audio [Radeon HD 3450/3470/3550/3570]                       | 2         | 2.94%   |
| AMD Renoir Radeon High Definition Audio Controller                         | 2         | 2.94%   |
| AMD Raven/Raven2/Fenghuang HDMI/DP Audio Controller                        | 2         | 2.94%   |
| Nvidia High Definition Audio Controller                                    | 1         | 1.47%   |
| Nvidia GP107GL High Definition Audio Controller                            | 1         | 1.47%   |
| Nvidia GP102 HDMI Audio Controller                                         | 1         | 1.47%   |
| Nvidia GM200 High Definition Audio                                         | 1         | 1.47%   |
| Nvidia GM107 High Definition Audio Controller [GeForce 940MX]              | 1         | 1.47%   |
| Nvidia GK208 HDMI/DP Audio Controller                                      | 1         | 1.47%   |
| Nvidia GF119 HDMI Audio Controller                                         | 1         | 1.47%   |
| Logitech [G533 Wireless Headset Dongle]                                    | 1         | 1.47%   |
| Logitech Stereo H650e                                                      | 1         | 1.47%   |
| Intel Sunrise Point-LP HD Audio                                            | 1         | 1.47%   |
| Intel Comet Lake PCH cAVS                                                  | 1         | 1.47%   |
| Intel Celeron/Pentium Silver Processor High Definition Audio               | 1         | 1.47%   |
| Intel C600/X79 series chipset High Definition Audio Controller             | 1         | 1.47%   |
| Intel 9 Series Chipset Family HD Audio Controller                          | 1         | 1.47%   |
| Intel 82801I (ICH9 Family) HD Audio Controller                             | 1         | 1.47%   |
| Conexant Systems HP Dock Audio                                             | 1         | 1.47%   |
| C-Media Electronics USB Advanced Audio Device                              | 1         | 1.47%   |
| C-Media Electronics TONOR TC30 Audio Device                                | 1         | 1.47%   |
| ASUSTek Computer USB Audio                                                 | 1         | 1.47%   |
| AMD High Definition Audio Controller                                       | 1         | 1.47%   |
| AMD Family 17h (Models 00h-0fh) HD Audio Controller                        | 1         | 1.47%   |
| AMD Family 15h (Models 60h-6fh) Audio Controller                           | 1         | 1.47%   |

Memory
------

Memory Vendor
-------------

Memory module vendors

![Memory Vendor](./All/images/pie_chart/memory_vendor.svg)


| Vendor              | Computers | Percent |
|---------------------|-----------|---------|
| Samsung Electronics | 7         | 33.33%  |
| Micron Technology   | 4         | 19.05%  |
| G.Skill             | 3         | 14.29%  |
| Unknown             | 2         | 9.52%   |
| SK Hynix            | 2         | 9.52%   |
| Kingston            | 1         | 4.76%   |
| Crucial             | 1         | 4.76%   |
| Corsair             | 1         | 4.76%   |

Memory Model
------------

Memory module models

![Memory Model](./All/images/pie_chart/memory_model.svg)


| Model                                                         | Computers | Percent |
|---------------------------------------------------------------|-----------|---------|
| Unknown RAM Module 2048MB SODIMM DDR3 1600MT/s                | 1         | 4.76%   |
| Unknown RAM Module 1GB DIMM DDR2 667MT/s                      | 1         | 4.76%   |
| SK Hynix RAM HMT351U6EFR8C-PB 4GB DIMM DDR3 1800MT/s          | 1         | 4.76%   |
| SK Hynix RAM HMA81GS6DJR8N-XN 8192MB SODIMM DDR4 3200MT/s     | 1         | 4.76%   |
| Samsung RAM M471B1G73QH0-YK0 8GB SODIMM DDR3 2667MT/s         | 1         | 4.76%   |
| Samsung RAM M471B1G73DB0-YK0 8GB SODIMM DDR3 1600MT/s         | 1         | 4.76%   |
| Samsung RAM M393B2G70BH0-YK0 16GB DIMM DDR3 1600MT/s          | 1         | 4.76%   |
| Samsung RAM M393B2873EH1-CF8 1GB DIMM DDR3 1066MT/s           | 1         | 4.76%   |
| Samsung RAM M393B2873DZ1-CF8 1GB DIMM DDR3 1066MT/s           | 1         | 4.76%   |
| Samsung RAM M378B5273DH0-CK0 4096MB DIMM DDR3 2200MT/s        | 1         | 4.76%   |
| Samsung RAM M378A4G43AB2-CWE 32GB DIMM DDR4 3200MT/s          | 1         | 4.76%   |
| Micron RAM Module 8GB DIMM DDR4 3200MT/s                      | 1         | 4.76%   |
| Micron RAM 4ATF1G64HZ-3G2E1 8192MB Row Of Chips DDR4 3200MT/s | 1         | 4.76%   |
| Micron RAM 36ASF4G72PZ-2G6D1 32GB DIMM DDR4 2667MT/s          | 1         | 4.76%   |
| Micron RAM 18ASF1G72PDZ-2G6F1 8GB DIMM DDR4 2666MT/s          | 1         | 4.76%   |
| Kingston RAM KHX1600C9D3/8GX 8GB DIMM DDR3 1600MT/s           | 1         | 4.76%   |
| G.Skill RAM F4-3600C19-8GVRB 8GB DIMM DDR4 2933MT/s           | 1         | 4.76%   |
| G.Skill RAM F4-2666C18-32GVK 32GB DIMM DDR4 2666MT/s          | 1         | 4.76%   |
| G.Skill RAM F3-2400C10-8GTX 8GB DIMM DDR3 2400MT/s            | 1         | 4.76%   |
| Crucial RAM CT16G4SFRA266.M16FRS 16GB SODIMM DDR4 2667MT/s    | 1         | 4.76%   |
| Corsair RAM CMK8GX4M1Z3200C16 8GB DIMM DDR4 3200MT/s          | 1         | 4.76%   |

Memory Kind
-----------

Memory module kinds

![Memory Kind](./All/images/pie_chart/memory_kind.svg)


| Kind | Computers | Percent |
|------|-----------|---------|
| DDR4 | 10        | 50%     |
| DDR3 | 9         | 45%     |
| DDR2 | 1         | 5%      |

Memory Form Factor
------------------

Physical design of the memory module

![Memory Form Factor](./All/images/pie_chart/memory_formfactor.svg)


| Name         | Computers | Percent |
|--------------|-----------|---------|
| DIMM         | 14        | 70%     |
| SODIMM       | 5         | 25%     |
| Row Of Chips | 1         | 5%      |

Memory Size
-----------

Memory module size

![Memory Size](./All/images/pie_chart/memory_size.svg)


| Size  | Computers | Percent |
|-------|-----------|---------|
| 8192  | 10        | 50%     |
| 32768 | 3         | 15%     |
| 1024  | 3         | 15%     |
| 16384 | 2         | 10%     |
| 4096  | 1         | 5%      |
| 2048  | 1         | 5%      |

Memory Speed
------------

Memory module speed

![Memory Speed](./All/images/pie_chart/memory_speed.svg)


| Speed | Computers | Percent |
|-------|-----------|---------|
| 3200  | 5         | 23.81%  |
| 1600  | 4         | 19.05%  |
| 2667  | 3         | 14.29%  |
| 2666  | 2         | 9.52%   |
| 1066  | 2         | 9.52%   |
| 2933  | 1         | 4.76%   |
| 2400  | 1         | 4.76%   |
| 2200  | 1         | 4.76%   |
| 1800  | 1         | 4.76%   |
| 667   | 1         | 4.76%   |

Printers & scanners
-------------------

Printer Vendor
--------------

Printer device vendors

![Printer Vendor](./All/images/pie_chart/printer_vendor.svg)


| Vendor             | Computers | Percent |
|--------------------|-----------|---------|
| Brother Industries | 1         | 100%    |

Printer Model
-------------

Printer device models

![Printer Model](./All/images/pie_chart/printer_model.svg)


| Model                         | Computers | Percent |
|-------------------------------|-----------|---------|
| Brother HL-2030 Laser Printer | 1         | 100%    |

Scanner Vendor
--------------

Scanner device vendors

Zero info for selected period =(

Scanner Model
-------------

Scanner device models

Zero info for selected period =(

Camera
------

Camera Vendor
-------------

Camera device vendors

![Camera Vendor](./All/images/pie_chart/camera_vendor.svg)


| Vendor                | Computers | Percent |
|-----------------------|-----------|---------|
| Chicony Electronics   | 5         | 26.32%  |
| Syntek                | 4         | 21.05%  |
| Logitech              | 3         | 15.79%  |
| IMC Networks          | 2         | 10.53%  |
| Realtek Semiconductor | 1         | 5.26%   |
| Quanta                | 1         | 5.26%   |
| Lenovo                | 1         | 5.26%   |
| Intel                 | 1         | 5.26%   |
| Huawei Technologies   | 1         | 5.26%   |

Camera Model
------------

Camera device models

![Camera Model](./All/images/pie_chart/camera_model.svg)


| Model                                  | Computers | Percent |
|----------------------------------------|-----------|---------|
| Chicony Integrated Camera              | 3         | 15.79%  |
| Syntek Lenovo EasyCamera               | 2         | 10.53%  |
| Syntek Integrated Camera               | 2         | 10.53%  |
| Realtek EasyCamera                     | 1         | 5.26%   |
| Quanta HP Webcam                       | 1         | 5.26%   |
| Logitech Webcam C270                   | 1         | 5.26%   |
| Logitech HD Pro Webcam C920            | 1         | 5.26%   |
| Logitech BRIO Ultra HD Webcam          | 1         | 5.26%   |
| Lenovo UVC Camera                      | 1         | 5.26%   |
| Intel RealSense 3D Camera (Front F200) | 1         | 5.26%   |
| IMC Networks USB2.0 HD UVC WebCam      | 1         | 5.26%   |
| IMC Networks TOSHIBA Web Camera - HD   | 1         | 5.26%   |
| Huawei HiCamera                        | 1         | 5.26%   |
| Chicony HP HD Camera                   | 1         | 5.26%   |
| Chicony HD WebCam                      | 1         | 5.26%   |

Security
--------

Fingerprint Vendor
------------------

Fingerprint sensor vendors

![Fingerprint Vendor](./All/images/pie_chart/fingerprint_vendor.svg)


| Vendor           | Computers | Percent |
|------------------|-----------|---------|
| Validity Sensors | 3         | 60%     |
| Synaptics        | 1         | 20%     |
| AuthenTec        | 1         | 20%     |

Fingerprint Model
-----------------

Fingerprint sensor models

![Fingerprint Model](./All/images/pie_chart/fingerprint_model.svg)


| Model                                        | Computers | Percent |
|----------------------------------------------|-----------|---------|
| Validity Sensors VFS495 Fingerprint Reader   | 1         | 20%     |
| Validity Sensors VFS Fingerprint sensor      | 1         | 20%     |
| Validity Sensors VFS 5011 fingerprint sensor | 1         | 20%     |
| AuthenTec AES2810                            | 1         | 20%     |
| Unknown                                      | 1         | 20%     |

Chipcard Vendor
---------------

Chipcard module vendors

![Chipcard Vendor](./All/images/pie_chart/chipcard_vendor.svg)


| Vendor   | Computers | Percent |
|----------|-----------|---------|
| O2 Micro | 1         | 100%    |

Chipcard Model
--------------

Chipcard module models

![Chipcard Model](./All/images/pie_chart/chipcard_model.svg)


| Model                                | Computers | Percent |
|--------------------------------------|-----------|---------|
| O2 Micro OZ776 CCID Smartcard Reader | 1         | 100%    |

Unsupported
-----------

Unsupported Devices
-------------------

Total unsupported devices on board

![Unsupported Devices](./All/images/pie_chart/device_unsupported.svg)


| Total | Computers | Percent |
|-------|-----------|---------|
| 0     | 22        | 57.89%  |
| 1     | 12        | 31.58%  |
| 2     | 2         | 5.26%   |
| 5     | 1         | 2.63%   |
| 4     | 1         | 2.63%   |

Unsupported Device Types
------------------------

Types of unsupported devices

![Unsupported Device Types](./All/images/pie_chart/device_unsupported_type.svg)


| Type                     | Computers | Percent |
|--------------------------|-----------|---------|
| Fingerprint reader       | 5         | 25%     |
| Graphics card            | 3         | 15%     |
| Communication controller | 3         | 15%     |
| Unassigned class         | 2         | 10%     |
| Network                  | 2         | 10%     |
| Net/wireless             | 2         | 10%     |
| Multimedia controller    | 1         | 5%      |
| Chipcard                 | 1         | 5%      |
| Card reader              | 1         | 5%      |

