Ubuntu 21.10 - Tested Hardware & Statistics (Desktops)
------------------------------------------------------

A project to collect tested hardware configurations for Ubuntu 21.10.

Anyone can contribute to this report by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Please submit a probe of your configuration if it's not presented on the page or is rare.

Full-feature report is available here: https://linux-hardware.org/?view=trends

Contents
--------

* [ Test Cases ](#test-cases)

* [ System ](#system)
  - [ Kernel                   ](#kernel)
  - [ Kernel Family            ](#kernel-family)
  - [ Kernel Major Ver.        ](#kernel-major-ver)
  - [ Arch                     ](#arch)
  - [ DE                       ](#de)
  - [ Display Server           ](#display-server)
  - [ Display Manager          ](#display-manager)
  - [ OS Lang                  ](#os-lang)
  - [ Boot Mode                ](#boot-mode)
  - [ Filesystem               ](#filesystem)
  - [ Part. scheme             ](#part-scheme)
  - [ Dual Boot with Linux/BSD ](#dual-boot-with-linuxbsd)
  - [ Dual Boot (Win)          ](#dual-boot-win)

* [ Board ](#board)
  - [ Vendor                   ](#vendor)
  - [ Model                    ](#model)
  - [ Model Family             ](#model-family)
  - [ MFG Year                 ](#mfg-year)
  - [ Form Factor              ](#form-factor)
  - [ Secure Boot              ](#secure-boot)
  - [ Coreboot                 ](#coreboot)
  - [ RAM Size                 ](#ram-size)
  - [ RAM Used                 ](#ram-used)
  - [ Total Drives             ](#total-drives)
  - [ Has CD-ROM               ](#has-cd-rom)
  - [ Has Ethernet             ](#has-ethernet)
  - [ Has WiFi                 ](#has-wifi)
  - [ Has Bluetooth            ](#has-bluetooth)

* [ Location ](#location)
  - [ Country                  ](#country)
  - [ City                     ](#city)

* [ Drives ](#drives)
  - [ Drive Vendor             ](#drive-vendor)
  - [ Drive Model              ](#drive-model)
  - [ HDD Vendor               ](#hdd-vendor)
  - [ SSD Vendor               ](#ssd-vendor)
  - [ Drive Kind               ](#drive-kind)
  - [ Drive Connector          ](#drive-connector)
  - [ Drive Size               ](#drive-size)
  - [ Space Total              ](#space-total)
  - [ Space Used               ](#space-used)
  - [ Malfunc. Drives          ](#malfunc-drives)
  - [ Malfunc. Drive Vendor    ](#malfunc-drive-vendor)
  - [ Malfunc. HDD Vendor      ](#malfunc-hdd-vendor)
  - [ Malfunc. Drive Kind      ](#malfunc-drive-kind)
  - [ Failed Drives            ](#failed-drives)
  - [ Failed Drive Vendor      ](#failed-drive-vendor)
  - [ Drive Status             ](#drive-status)

* [ Storage controller ](#storage-controller)
  - [ Storage Vendor           ](#storage-vendor)
  - [ Storage Model            ](#storage-model)
  - [ Storage Kind             ](#storage-kind)

* [ Processor ](#processor)
  - [ CPU Vendor               ](#cpu-vendor)
  - [ CPU Model                ](#cpu-model)
  - [ CPU Model Family         ](#cpu-model-family)
  - [ CPU Cores                ](#cpu-cores)
  - [ CPU Sockets              ](#cpu-sockets)
  - [ CPU Threads              ](#cpu-threads)
  - [ CPU Op-Modes             ](#cpu-op-modes)
  - [ CPU Microcode            ](#cpu-microcode)
  - [ CPU Microarch            ](#cpu-microarch)

* [ Graphics ](#graphics)
  - [ GPU Vendor               ](#gpu-vendor)
  - [ GPU Model                ](#gpu-model)
  - [ GPU Combo                ](#gpu-combo)
  - [ GPU Driver               ](#gpu-driver)
  - [ GPU Memory               ](#gpu-memory)

* [ Monitor ](#monitor)
  - [ Monitor Vendor           ](#monitor-vendor)
  - [ Monitor Model            ](#monitor-model)
  - [ Monitor Resolution       ](#monitor-resolution)
  - [ Monitor Diagonal         ](#monitor-diagonal)
  - [ Monitor Width            ](#monitor-width)
  - [ Aspect Ratio             ](#aspect-ratio)
  - [ Monitor Area             ](#monitor-area)
  - [ Pixel Density            ](#pixel-density)
  - [ Multiple Monitors        ](#multiple-monitors)

* [ Network ](#network)
  - [ Net Controller Vendor    ](#net-controller-vendor)
  - [ Net Controller Model     ](#net-controller-model)
  - [ Wireless Vendor          ](#wireless-vendor)
  - [ Wireless Model           ](#wireless-model)
  - [ Ethernet Vendor          ](#ethernet-vendor)
  - [ Ethernet Model           ](#ethernet-model)
  - [ Net Controller Kind      ](#net-controller-kind)
  - [ Used Controller          ](#used-controller)
  - [ NICs                     ](#nics)
  - [ IPv6                     ](#ipv6)

* [ Bluetooth ](#bluetooth)
  - [ Bluetooth Vendor         ](#bluetooth-vendor)
  - [ Bluetooth Model          ](#bluetooth-model)

* [ Sound ](#sound)
  - [ Sound Vendor             ](#sound-vendor)
  - [ Sound Model              ](#sound-model)

* [ Memory ](#memory)
  - [ Memory Vendor            ](#memory-vendor)
  - [ Memory Model             ](#memory-model)
  - [ Memory Kind              ](#memory-kind)
  - [ Memory Form Factor       ](#memory-form-factor)
  - [ Memory Size              ](#memory-size)
  - [ Memory Speed             ](#memory-speed)

* [ Printers & scanners ](#printers--scanners)
  - [ Printer Vendor           ](#printer-vendor)
  - [ Printer Model            ](#printer-model)
  - [ Scanner Vendor           ](#scanner-vendor)
  - [ Scanner Model            ](#scanner-model)

* [ Camera ](#camera)
  - [ Camera Vendor            ](#camera-vendor)
  - [ Camera Model             ](#camera-model)

* [ Security ](#security)
  - [ Fingerprint Vendor       ](#fingerprint-vendor)
  - [ Fingerprint Model        ](#fingerprint-model)
  - [ Chipcard Vendor          ](#chipcard-vendor)
  - [ Chipcard Model           ](#chipcard-model)

* [ Unsupported ](#unsupported)
  - [ Unsupported Devices      ](#unsupported-devices)
  - [ Unsupported Device Types ](#unsupported-device-types)


Test Cases
----------

| Vendor        | Model                       | Probe                                                      | Date         |
|---------------|-----------------------------|------------------------------------------------------------|--------------|
| Gigabyte      | Z77X-D3H                    | [3ebf180dc4](https://linux-hardware.org/?probe=3ebf180dc4) | Jan 01, 2022 |
| MSI           | A320M-A PRO MAX             | [6601708090](https://linux-hardware.org/?probe=6601708090) | Dec 31, 2021 |
| MSI           | A320M PRO-M2 V2             | [4e84971678](https://linux-hardware.org/?probe=4e84971678) | Dec 30, 2021 |
| Gigabyte      | X570 GAMING X               | [50045a522a](https://linux-hardware.org/?probe=50045a522a) | Dec 30, 2021 |
| MSI           | Q45MDO                      | [e3ea0d80d3](https://linux-hardware.org/?probe=e3ea0d80d3) | Dec 30, 2021 |
| ASUSTek       | P8H61                       | [682efb70d7](https://linux-hardware.org/?probe=682efb70d7) | Dec 29, 2021 |
| Pegatron      | Narra6                      | [da3be9b31b](https://linux-hardware.org/?probe=da3be9b31b) | Dec 29, 2021 |
| ASRock        | B550M-ITX/ac                | [1e4bffb013](https://linux-hardware.org/?probe=1e4bffb013) | Dec 29, 2021 |
| ASRock        | Z170 Gaming K4              | [590ae02fdb](https://linux-hardware.org/?probe=590ae02fdb) | Dec 29, 2021 |
| Gigabyte      | 965P-DS3                    | [467762be06](https://linux-hardware.org/?probe=467762be06) | Dec 29, 2021 |
| EVGA          | 134-KS-E377                 | [503b4d620c](https://linux-hardware.org/?probe=503b4d620c) | Dec 29, 2021 |
| Gigabyte      | Z690 AORUS ELITE DDR4       | [8b006f4339](https://linux-hardware.org/?probe=8b006f4339) | Dec 28, 2021 |
| ASRock        | 4Core1600P35-WiFi+          | [a3af4e5057](https://linux-hardware.org/?probe=a3af4e5057) | Dec 28, 2021 |
| ASRock        | 4Core1600P35-WiFi+          | [bae2ef5b28](https://linux-hardware.org/?probe=bae2ef5b28) | Dec 28, 2021 |
| ASUSTek       | Maximus VIII EXTREME        | [ccc49903fd](https://linux-hardware.org/?probe=ccc49903fd) | Dec 28, 2021 |
| Medion        | H81H3-EM2 H81EM2W08.308     | [cb6cfc3c5e](https://linux-hardware.org/?probe=cb6cfc3c5e) | Dec 27, 2021 |
| Gigabyte      | Z690 AORUS ELITE DDR4       | [5ea8f7d7a8](https://linux-hardware.org/?probe=5ea8f7d7a8) | Dec 27, 2021 |
| ASUSTek       | CM6870                      | [0a24371b49](https://linux-hardware.org/?probe=0a24371b49) | Dec 27, 2021 |
| Acer          | Predator PO3-620            | [d33f608e2e](https://linux-hardware.org/?probe=d33f608e2e) | Dec 27, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [9089dc4f34](https://linux-hardware.org/?probe=9089dc4f34) | Dec 27, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [a5e698c32b](https://linux-hardware.org/?probe=a5e698c32b) | Dec 27, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [757d6a5d20](https://linux-hardware.org/?probe=757d6a5d20) | Dec 27, 2021 |
| Dell          | 0HD5W2 A01                  | [143f0ef296](https://linux-hardware.org/?probe=143f0ef296) | Dec 27, 2021 |
| Positivo      | POS-MIH61CF                 | [a8fd13db4c](https://linux-hardware.org/?probe=a8fd13db4c) | Dec 27, 2021 |
| ASUSTek       | P5G41T-M LX3                | [05b30c96fd](https://linux-hardware.org/?probe=05b30c96fd) | Dec 26, 2021 |
| ASRock        | Z690M-ITX/ax                | [0a35834719](https://linux-hardware.org/?probe=0a35834719) | Dec 26, 2021 |
| Gigabyte      | X570 GAMING X               | [4ca65158f1](https://linux-hardware.org/?probe=4ca65158f1) | Dec 26, 2021 |
| Gigabyte      | X570 GAMING X               | [19959c7845](https://linux-hardware.org/?probe=19959c7845) | Dec 26, 2021 |
| Dell          | 0WN7Y6 A01                  | [45220bb46c](https://linux-hardware.org/?probe=45220bb46c) | Dec 26, 2021 |
| ASRock        | Z690M-ITX/ax                | [fcc19136e0](https://linux-hardware.org/?probe=fcc19136e0) | Dec 26, 2021 |
| MSI           | MPG X570 GAMING EDGE WIF... | [8881d4e351](https://linux-hardware.org/?probe=8881d4e351) | Dec 25, 2021 |
| ASUSTek       | M4A89GTD-PRO/USB3           | [0ed55de90b](https://linux-hardware.org/?probe=0ed55de90b) | Dec 25, 2021 |
| ASUSTek       | M4A89GTD-PRO/USB3           | [e93a789ba7](https://linux-hardware.org/?probe=e93a789ba7) | Dec 25, 2021 |
| Foxconn       | 2ABF                        | [fdc6aac853](https://linux-hardware.org/?probe=fdc6aac853) | Dec 25, 2021 |
| Medion        | MS-7707                     | [2590f9fac3](https://linux-hardware.org/?probe=2590f9fac3) | Dec 25, 2021 |
| Medion        | MS-7707                     | [9a64e7919f](https://linux-hardware.org/?probe=9a64e7919f) | Dec 25, 2021 |
| HP            | 8643 SMVB                   | [18fdb46bb5](https://linux-hardware.org/?probe=18fdb46bb5) | Dec 24, 2021 |
| Positivo      | POS-MIH61CF                 | [f10e90bd72](https://linux-hardware.org/?probe=f10e90bd72) | Dec 24, 2021 |
| Dell          | 0F3KHR A00                  | [3efe58bf92](https://linux-hardware.org/?probe=3efe58bf92) | Dec 24, 2021 |
| HP            | 158A                        | [dc1212a83a](https://linux-hardware.org/?probe=dc1212a83a) | Dec 24, 2021 |
| Gigabyte      | B150M-HD3-CF                | [8b9eeb5990](https://linux-hardware.org/?probe=8b9eeb5990) | Dec 24, 2021 |
| ASUSTek       | TUF GAMING Z690-PLUS WIF... | [26b64d67a9](https://linux-hardware.org/?probe=26b64d67a9) | Dec 24, 2021 |
| HP            | 82B4                        | [02f9952fa5](https://linux-hardware.org/?probe=02f9952fa5) | Dec 24, 2021 |
| HP            | 83EE                        | [225f3c4b8d](https://linux-hardware.org/?probe=225f3c4b8d) | Dec 24, 2021 |
| Lenovo        | NOK                         | [5671e681fe](https://linux-hardware.org/?probe=5671e681fe) | Dec 24, 2021 |
| ASUSTek       | TUF B450-PLUS GAMING        | [04f1714e45](https://linux-hardware.org/?probe=04f1714e45) | Dec 23, 2021 |
| Dell          | 0WN7Y6 A01                  | [80a3b049dd](https://linux-hardware.org/?probe=80a3b049dd) | Dec 23, 2021 |
| HP            | 3048h                       | [2e47687170](https://linux-hardware.org/?probe=2e47687170) | Dec 23, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [35e30d5285](https://linux-hardware.org/?probe=35e30d5285) | Dec 23, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [040ae5d30d](https://linux-hardware.org/?probe=040ae5d30d) | Dec 23, 2021 |
| MSI           | Z97 GAMING 5                | [4f71fa3090](https://linux-hardware.org/?probe=4f71fa3090) | Dec 23, 2021 |
| ASRock        | Z370 Pro4                   | [482842307e](https://linux-hardware.org/?probe=482842307e) | Dec 23, 2021 |
| Dell          | 0VHWTR A02                  | [3be006d99a](https://linux-hardware.org/?probe=3be006d99a) | Dec 23, 2021 |
| ASUSTek       | P5P43TD PRO                 | [6019461793](https://linux-hardware.org/?probe=6019461793) | Dec 22, 2021 |
| ASUSTek       | TUF GAMING X570-PRO         | [c66f391530](https://linux-hardware.org/?probe=c66f391530) | Dec 22, 2021 |
| MSI           | H81M PRO-VD                 | [b0c70d78fd](https://linux-hardware.org/?probe=b0c70d78fd) | Dec 22, 2021 |
| Dell          | 0MM599                      | [91a32378db](https://linux-hardware.org/?probe=91a32378db) | Dec 22, 2021 |
| ASUSTek       | ProArt X570-CREATOR WIFI    | [427ddfb2d9](https://linux-hardware.org/?probe=427ddfb2d9) | Dec 21, 2021 |
| MSI           | B85M ECO                    | [034b632cee](https://linux-hardware.org/?probe=034b632cee) | Dec 21, 2021 |
| MSI           | B85M ECO                    | [9c63b4bd85](https://linux-hardware.org/?probe=9c63b4bd85) | Dec 21, 2021 |
| MSI           | 790XT-G45                   | [8f8e171a52](https://linux-hardware.org/?probe=8f8e171a52) | Dec 20, 2021 |
| Gigabyte      | Z170X-Gaming 7              | [bcd3f5edf4](https://linux-hardware.org/?probe=bcd3f5edf4) | Dec 20, 2021 |
| ASRock        | X58 Extreme3                | [0e188e9dd0](https://linux-hardware.org/?probe=0e188e9dd0) | Dec 19, 2021 |
| ASUSTek       | M5A99FX PRO R2.0            | [16017b88bc](https://linux-hardware.org/?probe=16017b88bc) | Dec 19, 2021 |
| MSI           | B450 TOMAHAWK               | [125b9e5965](https://linux-hardware.org/?probe=125b9e5965) | Dec 19, 2021 |
| ASUSTek       | M4A77TD PRO                 | [4e65d26e64](https://linux-hardware.org/?probe=4e65d26e64) | Dec 18, 2021 |
| Dell          | 042P49 A02                  | [56afcbdd15](https://linux-hardware.org/?probe=56afcbdd15) | Dec 18, 2021 |
| ASUSTek       | M5A97 EVO R2.0              | [1564f2f5ea](https://linux-hardware.org/?probe=1564f2f5ea) | Dec 18, 2021 |
| MSI           | X399 SLI PLUS               | [08c23ed037](https://linux-hardware.org/?probe=08c23ed037) | Dec 17, 2021 |
| ASUSTek       | PRIME B450M-A               | [b639c498bb](https://linux-hardware.org/?probe=b639c498bb) | Dec 17, 2021 |
| MSI           | MAG B460 TORPEDO            | [a26f1ed9a0](https://linux-hardware.org/?probe=a26f1ed9a0) | Dec 17, 2021 |
| Gigabyte      | EP35-DS4                    | [46e2648ab6](https://linux-hardware.org/?probe=46e2648ab6) | Dec 16, 2021 |
| Dell          | 0WR7PY A02                  | [459b162eab](https://linux-hardware.org/?probe=459b162eab) | Dec 16, 2021 |
| Biostar       | H61MLC                      | [ff1c843adf](https://linux-hardware.org/?probe=ff1c843adf) | Dec 15, 2021 |
| ASUSTek       | ROG STRIX Z370-F GAMING     | [6aee0ff442](https://linux-hardware.org/?probe=6aee0ff442) | Dec 15, 2021 |
| ASUSTek       | P6T DELUXE V2               | [275bcfce49](https://linux-hardware.org/?probe=275bcfce49) | Dec 15, 2021 |
| Dell          | 0WN7Y6 A01                  | [48e34ad548](https://linux-hardware.org/?probe=48e34ad548) | Dec 15, 2021 |
| Dell          | 0TP412                      | [8c26fae09d](https://linux-hardware.org/?probe=8c26fae09d) | Dec 15, 2021 |
| Positivo      | POS-MIH61CF                 | [20ecdbd153](https://linux-hardware.org/?probe=20ecdbd153) | Dec 14, 2021 |
| ASUSTek       | PRIME B450M-A               | [906909677d](https://linux-hardware.org/?probe=906909677d) | Dec 14, 2021 |
| MSI           | MAG Z490 TOMAHAWK           | [38d013c7db](https://linux-hardware.org/?probe=38d013c7db) | Dec 14, 2021 |
| HP            | 8054                        | [454fd4c8c7](https://linux-hardware.org/?probe=454fd4c8c7) | Dec 13, 2021 |
| ASRock        | Z170 Gaming K4              | [4f8e294d95](https://linux-hardware.org/?probe=4f8e294d95) | Dec 13, 2021 |
| Dell          | 0HN7XN A01                  | [648ce917b9](https://linux-hardware.org/?probe=648ce917b9) | Dec 13, 2021 |
| ASUSTek       | M5A78L-M/USB3               | [a85c1b3793](https://linux-hardware.org/?probe=a85c1b3793) | Dec 13, 2021 |
| ASUSTek       | TUF GAMING B550M-PLUS       | [1ee5ab4347](https://linux-hardware.org/?probe=1ee5ab4347) | Dec 12, 2021 |
| Gigabyte      | GA-78LMT-USB3               | [6e9b3e47ac](https://linux-hardware.org/?probe=6e9b3e47ac) | Dec 12, 2021 |
| Dell          | 0YXT71 A01                  | [fbe4f7fdb9](https://linux-hardware.org/?probe=fbe4f7fdb9) | Dec 12, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [36f09857c2](https://linux-hardware.org/?probe=36f09857c2) | Dec 12, 2021 |
| ASRock        | N68-GS4 FX                  | [17296e4753](https://linux-hardware.org/?probe=17296e4753) | Dec 11, 2021 |
| ASRock        | N68-GS4 FX                  | [883dca4bce](https://linux-hardware.org/?probe=883dca4bce) | Dec 11, 2021 |
| EVGA          | 134-KS-E377                 | [537cce12a4](https://linux-hardware.org/?probe=537cce12a4) | Dec 11, 2021 |
| Gigabyte      | Z390 AORUS MASTER-CF        | [19a0f28f2f](https://linux-hardware.org/?probe=19a0f28f2f) | Dec 11, 2021 |
| EVGA          | 134-KS-E377                 | [1ad3ddd70b](https://linux-hardware.org/?probe=1ad3ddd70b) | Dec 11, 2021 |
| ASUSTek       | CROSSHAIR VI HERO           | [15df6092b7](https://linux-hardware.org/?probe=15df6092b7) | Dec 11, 2021 |
| ASUSTek       | P5N-D                       | [772d984bb8](https://linux-hardware.org/?probe=772d984bb8) | Dec 10, 2021 |
| Dell          | 03KWTV A02                  | [446130659d](https://linux-hardware.org/?probe=446130659d) | Dec 10, 2021 |
| MSI           | H510I PRO WIFI              | [86ba639fd8](https://linux-hardware.org/?probe=86ba639fd8) | Dec 10, 2021 |
| Gigabyte      | X570 AORUS MASTER           | [2ad79030d4](https://linux-hardware.org/?probe=2ad79030d4) | Dec 10, 2021 |
| ASUSTek       | ROG CROSSHAIR VIII HERO     | [8d9bc5957a](https://linux-hardware.org/?probe=8d9bc5957a) | Dec 10, 2021 |
| Dell          | 03KWTV A02                  | [643a045c8b](https://linux-hardware.org/?probe=643a045c8b) | Dec 09, 2021 |
| MSI           | Z87-G45 GAMING              | [1a012f2f1f](https://linux-hardware.org/?probe=1a012f2f1f) | Dec 09, 2021 |
| ASRock        | X58 Extreme3                | [5c8c08277f](https://linux-hardware.org/?probe=5c8c08277f) | Dec 09, 2021 |
| ASRock        | X58 Extreme3                | [73fa8b0ca4](https://linux-hardware.org/?probe=73fa8b0ca4) | Dec 09, 2021 |
| ASUSTek       | PRIME B450M-A               | [b6e2e39051](https://linux-hardware.org/?probe=b6e2e39051) | Dec 09, 2021 |
| Lenovo        | MAHOBAY Win8 Pro DPK TPG    | [83df1d7ac7](https://linux-hardware.org/?probe=83df1d7ac7) | Dec 09, 2021 |
| ASUSTek       | H97I-PLUS                   | [47f631ff16](https://linux-hardware.org/?probe=47f631ff16) | Dec 09, 2021 |
| Gigabyte      | 945GCM-S2L                  | [a253c74be5](https://linux-hardware.org/?probe=a253c74be5) | Dec 08, 2021 |
| Dell          | 088DT1 A00                  | [1825e90eed](https://linux-hardware.org/?probe=1825e90eed) | Dec 08, 2021 |
| ASUSTek       | M4A89GTD-PRO/USB3           | [160b6097cd](https://linux-hardware.org/?probe=160b6097cd) | Dec 08, 2021 |
| ASUSTek       | STRIX Z270E GAMING          | [caa363c86c](https://linux-hardware.org/?probe=caa363c86c) | Dec 08, 2021 |
| Gigabyte      | GA-990FX-GAMING             | [38c5e135f8](https://linux-hardware.org/?probe=38c5e135f8) | Dec 08, 2021 |
| MSI           | Z270 GAMING PLUS            | [dc62f56792](https://linux-hardware.org/?probe=dc62f56792) | Dec 08, 2021 |
| OEM           | TOP77D Ver1.0               | [5747ccfcd4](https://linux-hardware.org/?probe=5747ccfcd4) | Dec 07, 2021 |
| Gigabyte      | EX58-UD5                    | [29f0eb6b67](https://linux-hardware.org/?probe=29f0eb6b67) | Dec 07, 2021 |
| ASUSTek       | PRIME B350-PLUS             | [70d51853a0](https://linux-hardware.org/?probe=70d51853a0) | Dec 07, 2021 |
| ASUSTek       | PRIME B350-PLUS             | [c0928aad35](https://linux-hardware.org/?probe=c0928aad35) | Dec 07, 2021 |
| ASRock        | B360M-ITX/ac                | [2490a94114](https://linux-hardware.org/?probe=2490a94114) | Dec 07, 2021 |
| MSI           | MEG X570 ACE                | [ce4bd7acd9](https://linux-hardware.org/?probe=ce4bd7acd9) | Dec 07, 2021 |
| Gigabyte      | B450 AORUS M                | [1493fa959b](https://linux-hardware.org/?probe=1493fa959b) | Dec 06, 2021 |
| EVGA          | 111-CS-E371                 | [e6af9b4e1e](https://linux-hardware.org/?probe=e6af9b4e1e) | Dec 06, 2021 |
| Gigabyte      | B365M DS3H                  | [8365e0bff7](https://linux-hardware.org/?probe=8365e0bff7) | Dec 05, 2021 |
| ASUSTek       | TUF GAMING Z690-PLUS WIF... | [0a69990530](https://linux-hardware.org/?probe=0a69990530) | Dec 05, 2021 |
| Dell          | 0KRC95 A00                  | [d16dfb27de](https://linux-hardware.org/?probe=d16dfb27de) | Dec 05, 2021 |
| ASUSTek       | M4A87TD/USB3                | [6550b760b5](https://linux-hardware.org/?probe=6550b760b5) | Dec 05, 2021 |
| Lenovo        | SHARKBAY SDK0E50510 WIN     | [7a511dda3d](https://linux-hardware.org/?probe=7a511dda3d) | Dec 04, 2021 |
| Lenovo        | SHARKBAY SDK0E50510 WIN     | [60e7267ddb](https://linux-hardware.org/?probe=60e7267ddb) | Dec 04, 2021 |
| ASUSTek       | PRIME A320M-K               | [1f37d4567d](https://linux-hardware.org/?probe=1f37d4567d) | Dec 04, 2021 |
| ASUSTek       | ROG STRIX X570-F GAMING     | [142f88e753](https://linux-hardware.org/?probe=142f88e753) | Dec 04, 2021 |
| HP            | 1587h                       | [aaa0c74349](https://linux-hardware.org/?probe=aaa0c74349) | Dec 04, 2021 |
| Gigabyte      | X79-UP4                     | [c98cb8462e](https://linux-hardware.org/?probe=c98cb8462e) | Dec 04, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [a83c80b9bd](https://linux-hardware.org/?probe=a83c80b9bd) | Dec 04, 2021 |
| ASUSTek       | A88XM-PLUS                  | [16fa85e296](https://linux-hardware.org/?probe=16fa85e296) | Dec 03, 2021 |
| ASRock        | ConRoe1333-D667             | [dce4f3f103](https://linux-hardware.org/?probe=dce4f3f103) | Dec 03, 2021 |
| HP            | 0AECh D                     | [b9550ddc31](https://linux-hardware.org/?probe=b9550ddc31) | Dec 03, 2021 |
| HP            | 0AECh D                     | [61d0324f27](https://linux-hardware.org/?probe=61d0324f27) | Dec 03, 2021 |
| MSI           | B450M-A PRO MAX             | [17a6983b50](https://linux-hardware.org/?probe=17a6983b50) | Dec 03, 2021 |
| Gigabyte      | 970A-DS3P                   | [054aa7b858](https://linux-hardware.org/?probe=054aa7b858) | Dec 02, 2021 |
| OEM           | TOP77D Ver1.0               | [8348cb42fc](https://linux-hardware.org/?probe=8348cb42fc) | Dec 02, 2021 |
| Lenovo        | NO DPK                      | [0d7784e414](https://linux-hardware.org/?probe=0d7784e414) | Dec 02, 2021 |
| Gigabyte      | X570 UD                     | [79c117738b](https://linux-hardware.org/?probe=79c117738b) | Dec 01, 2021 |
| ASUSTek       | CROSSHAIR VI HERO           | [876f4598f0](https://linux-hardware.org/?probe=876f4598f0) | Dec 01, 2021 |
| HP            | 212B                        | [b8688e6712](https://linux-hardware.org/?probe=b8688e6712) | Dec 01, 2021 |
| Lenovo        | Annapurna CRB 0B98401 PR... | [0e0cd0b225](https://linux-hardware.org/?probe=0e0cd0b225) | Dec 01, 2021 |
| OEM           | TOP77D Ver1.0               | [6b91b58b81](https://linux-hardware.org/?probe=6b91b58b81) | Nov 30, 2021 |
| MSI           | B460M-A PRO                 | [f972dc5e2a](https://linux-hardware.org/?probe=f972dc5e2a) | Nov 30, 2021 |
| MSI           | MAG Z490 TOMAHAWK           | [b3f648de8e](https://linux-hardware.org/?probe=b3f648de8e) | Nov 29, 2021 |
| ECS           | G31T-M7                     | [749da166c4](https://linux-hardware.org/?probe=749da166c4) | Nov 29, 2021 |
| ASRock        | G31M-VS                     | [b042297ea5](https://linux-hardware.org/?probe=b042297ea5) | Nov 29, 2021 |
| Gigabyte      | X570 AORUS ELITE            | [8c2024b822](https://linux-hardware.org/?probe=8c2024b822) | Nov 28, 2021 |
| Dell          | 0WR7PY A01                  | [f886714ec5](https://linux-hardware.org/?probe=f886714ec5) | Nov 28, 2021 |
| ASUSTek       | P8Z77-V LX                  | [17bd2e3558](https://linux-hardware.org/?probe=17bd2e3558) | Nov 28, 2021 |
| ASUSTek       | PRIME B450M-A               | [629e6cac75](https://linux-hardware.org/?probe=629e6cac75) | Nov 28, 2021 |
| ASUSTek       | ROG STRIX X570-E GAMING     | [88cf97c553](https://linux-hardware.org/?probe=88cf97c553) | Nov 27, 2021 |
| Gigabyte      | P35-S3G                     | [f8b94398df](https://linux-hardware.org/?probe=f8b94398df) | Nov 27, 2021 |
| ASRock        | A320M-HDV R4.0              | [59f124bbad](https://linux-hardware.org/?probe=59f124bbad) | Nov 27, 2021 |
| Pegatron      | 2A86E01                     | [17a1186394](https://linux-hardware.org/?probe=17a1186394) | Nov 26, 2021 |
| Gigabyte      | GA-MA770-UD3                | [551c45ed6b](https://linux-hardware.org/?probe=551c45ed6b) | Nov 26, 2021 |
| HP            | 2129                        | [8379f5fd56](https://linux-hardware.org/?probe=8379f5fd56) | Nov 26, 2021 |
| Gigabyte      | GA-MA770-UD3                | [58da56c671](https://linux-hardware.org/?probe=58da56c671) | Nov 26, 2021 |
| Gigabyte      | H77M-D3H                    | [a2606aebd8](https://linux-hardware.org/?probe=a2606aebd8) | Nov 26, 2021 |
| Gigabyte      | H110M-D2P-WG-CF             | [52d32ab3be](https://linux-hardware.org/?probe=52d32ab3be) | Nov 25, 2021 |
| Gigabyte      | B450M DS3H-CF               | [1c3cabc42a](https://linux-hardware.org/?probe=1c3cabc42a) | Nov 25, 2021 |
| MSI           | MPG Z590 GAMING CARBON W... | [651bc7560d](https://linux-hardware.org/?probe=651bc7560d) | Nov 25, 2021 |
| Acer          | G31T-M5                     | [a561aa834a](https://linux-hardware.org/?probe=a561aa834a) | Nov 25, 2021 |
| Gigabyte      | 970A-D3P                    | [76e0745afc](https://linux-hardware.org/?probe=76e0745afc) | Nov 24, 2021 |
| ASRock        | A320M-HDV R4.0              | [e7ab7b2011](https://linux-hardware.org/?probe=e7ab7b2011) | Nov 24, 2021 |
| MSI           | H510I PRO WIFI              | [b9b9b8c6ee](https://linux-hardware.org/?probe=b9b9b8c6ee) | Nov 24, 2021 |
| MSI           | H510I PRO WIFI              | [1abf510439](https://linux-hardware.org/?probe=1abf510439) | Nov 24, 2021 |
| ASUSTek       | STRIX Z270F GAMING          | [148f8b6de1](https://linux-hardware.org/?probe=148f8b6de1) | Nov 24, 2021 |
| ASUSTek       | PRIME X370-PRO              | [011f4ef64a](https://linux-hardware.org/?probe=011f4ef64a) | Nov 24, 2021 |
| ASUSTek       | STRIX Z270F GAMING          | [021d70dbf2](https://linux-hardware.org/?probe=021d70dbf2) | Nov 24, 2021 |
| Gigabyte      | GA-78LMT-USB3               | [bcf9812525](https://linux-hardware.org/?probe=bcf9812525) | Nov 24, 2021 |
| Gigabyte      | GA-78LMT-USB3 SEx           | [dab9856bd1](https://linux-hardware.org/?probe=dab9856bd1) | Nov 24, 2021 |
| ASUSTek       | TUF GAMING X570-PLUS        | [9bd8ca78c3](https://linux-hardware.org/?probe=9bd8ca78c3) | Nov 24, 2021 |
| HP            | 3397                        | [5412dd8b52](https://linux-hardware.org/?probe=5412dd8b52) | Nov 23, 2021 |
| MSI           | MPG Z390 GAMING PLUS        | [2d5ec95a93](https://linux-hardware.org/?probe=2d5ec95a93) | Nov 23, 2021 |
| MSI           | MPG Z390 GAMING PLUS        | [3fcd092ee3](https://linux-hardware.org/?probe=3fcd092ee3) | Nov 23, 2021 |
| Gigabyte      | GB-BRR7H-4800               | [8e73316539](https://linux-hardware.org/?probe=8e73316539) | Nov 23, 2021 |
| ASUSTek       | PRIME Z590-P                | [69fe9cde99](https://linux-hardware.org/?probe=69fe9cde99) | Nov 23, 2021 |
| Unknown       | Unknown                     | [57364e93b2](https://linux-hardware.org/?probe=57364e93b2) | Nov 23, 2021 |
| MSI           | PRO Z690-A DDR4             | [1022ba26e4](https://linux-hardware.org/?probe=1022ba26e4) | Nov 22, 2021 |
| JGINYUE       | H97I PLUS V2.0              | [fcefb22fe5](https://linux-hardware.org/?probe=fcefb22fe5) | Nov 22, 2021 |
| Gigabyte      | A320M-DS2-CF                | [02020c5820](https://linux-hardware.org/?probe=02020c5820) | Nov 22, 2021 |
| HP            | 0AECh D                     | [c2d85e5d81](https://linux-hardware.org/?probe=c2d85e5d81) | Nov 22, 2021 |
| Gigabyte      | B75M-D3H                    | [939cd87ee7](https://linux-hardware.org/?probe=939cd87ee7) | Nov 22, 2021 |
| Gigabyte      | X570 AORUS ELITE            | [4c603f8b64](https://linux-hardware.org/?probe=4c603f8b64) | Nov 22, 2021 |
| Dell          | 0C27VV A00                  | [d3a7b5b39b](https://linux-hardware.org/?probe=d3a7b5b39b) | Nov 22, 2021 |
| Unknown       | Unknown                     | [ebc991da95](https://linux-hardware.org/?probe=ebc991da95) | Nov 22, 2021 |
| ASRock        | X99M Extreme4               | [3f738eedfc](https://linux-hardware.org/?probe=3f738eedfc) | Nov 22, 2021 |
| Lenovo        | 1036 SDK0T76457 WIN 3915... | [f894442edc](https://linux-hardware.org/?probe=f894442edc) | Nov 22, 2021 |
| ASRock        | Z390 Extreme4               | [12f1aecfc3](https://linux-hardware.org/?probe=12f1aecfc3) | Nov 21, 2021 |
| ASUSTek       | P5L-VM 1394                 | [a43426b94f](https://linux-hardware.org/?probe=a43426b94f) | Nov 21, 2021 |
| ASUSTek       | M2N                         | [06bba5770c](https://linux-hardware.org/?probe=06bba5770c) | Nov 21, 2021 |
| HP            | 0AECh D                     | [4edef515e0](https://linux-hardware.org/?probe=4edef515e0) | Nov 21, 2021 |
| ASUSTek       | P8H61/USB3 R2.0             | [64591821a6](https://linux-hardware.org/?probe=64591821a6) | Nov 20, 2021 |
| Lenovo        | ThinkServer TS140           | [da5af2478e](https://linux-hardware.org/?probe=da5af2478e) | Nov 20, 2021 |
| MSI           | PRO Z690-A DDR4             | [64e545e8b1](https://linux-hardware.org/?probe=64e545e8b1) | Nov 20, 2021 |
| Dell          | 0Y5DDC A00                  | [9f04ac4715](https://linux-hardware.org/?probe=9f04ac4715) | Nov 20, 2021 |
| Fujitsu       | FujitsuTP7000 -1            | [a509f56734](https://linux-hardware.org/?probe=a509f56734) | Nov 20, 2021 |
| ASUSTek       | M2N                         | [dfc5087439](https://linux-hardware.org/?probe=dfc5087439) | Nov 20, 2021 |
| ASUSTek       | PRIME H510M-K               | [dc3ffc2288](https://linux-hardware.org/?probe=dc3ffc2288) | Nov 20, 2021 |
| ASRock        | Z690 Steel Legend           | [73afa3e4f2](https://linux-hardware.org/?probe=73afa3e4f2) | Nov 19, 2021 |
| ASUSTek       | Rampage IV EXTREME          | [50dbf1ce3d](https://linux-hardware.org/?probe=50dbf1ce3d) | Nov 19, 2021 |
| MSI           | Z170A SLI                   | [2705718ac8](https://linux-hardware.org/?probe=2705718ac8) | Nov 19, 2021 |
| Gigabyte      | A320M-H-CF                  | [8eeef70a27](https://linux-hardware.org/?probe=8eeef70a27) | Nov 19, 2021 |
| MSI           | Z270 GAMING PLUS            | [28b50f68d2](https://linux-hardware.org/?probe=28b50f68d2) | Nov 19, 2021 |
| ASUSTek       | P8H61/USB3 R2.0             | [a1261a6eca](https://linux-hardware.org/?probe=a1261a6eca) | Nov 19, 2021 |
| HP            | ProLiant ML350 Gen9         | [9a5ec34f4b](https://linux-hardware.org/?probe=9a5ec34f4b) | Nov 18, 2021 |
| HP            | 0AECh D                     | [e52cea7894](https://linux-hardware.org/?probe=e52cea7894) | Nov 18, 2021 |
| HP            | 304Ah                       | [988e1e374a](https://linux-hardware.org/?probe=988e1e374a) | Nov 18, 2021 |
| ASUSTek       | PRIME Z690-P WIFI D4        | [61ba55d34f](https://linux-hardware.org/?probe=61ba55d34f) | Nov 18, 2021 |
| Gigabyte      | X570 AORUS MASTER           | [c754a471ba](https://linux-hardware.org/?probe=c754a471ba) | Nov 17, 2021 |
| MSI           | Z97-G45 GAMING              | [fc7a1caa36](https://linux-hardware.org/?probe=fc7a1caa36) | Nov 17, 2021 |
| ASUSTek       | P8H61-M LX R2.0             | [8377977aee](https://linux-hardware.org/?probe=8377977aee) | Nov 17, 2021 |
| Gigabyte      | Z77X-UD5H                   | [f1cd45b49a](https://linux-hardware.org/?probe=f1cd45b49a) | Nov 17, 2021 |
| HP            | 1494                        | [a1e8628159](https://linux-hardware.org/?probe=a1e8628159) | Nov 17, 2021 |
| Gigabyte      | Z690 UD DDR4                | [8435741705](https://linux-hardware.org/?probe=8435741705) | Nov 17, 2021 |
| Gigabyte      | X470 AORUS GAMING 7 WIFI... | [e460a37b0f](https://linux-hardware.org/?probe=e460a37b0f) | Nov 17, 2021 |
| ASUSTek       | PRIME Z690-P WIFI D4        | [2c5829a148](https://linux-hardware.org/?probe=2c5829a148) | Nov 17, 2021 |
| Gigabyte      | X570 AORUS MASTER           | [3cc581951b](https://linux-hardware.org/?probe=3cc581951b) | Nov 17, 2021 |
| MSI           | H270 GAMING M3              | [d863ad50dd](https://linux-hardware.org/?probe=d863ad50dd) | Nov 16, 2021 |
| Dell          | 0FM586                      | [79a63f7f55](https://linux-hardware.org/?probe=79a63f7f55) | Nov 16, 2021 |
| MSI           | PRO Z690-A DDR4             | [63c901cb53](https://linux-hardware.org/?probe=63c901cb53) | Nov 16, 2021 |
| MSI           | PRO Z690-A WIFI DDR4        | [22cf0835fb](https://linux-hardware.org/?probe=22cf0835fb) | Nov 16, 2021 |
| Dell          | 0Y5DDC A00                  | [f90eb0a986](https://linux-hardware.org/?probe=f90eb0a986) | Nov 16, 2021 |
| Dell          | 088DT1 A00                  | [0a20b8ab82](https://linux-hardware.org/?probe=0a20b8ab82) | Nov 16, 2021 |
| MSI           | MEG B550 UNIFY-X            | [c50d253bab](https://linux-hardware.org/?probe=c50d253bab) | Nov 16, 2021 |
| MSI           | MEG B550 UNIFY-X            | [d9142d6737](https://linux-hardware.org/?probe=d9142d6737) | Nov 16, 2021 |
| HP            | 8056                        | [f62a924908](https://linux-hardware.org/?probe=f62a924908) | Nov 16, 2021 |
| ASRock        | Z590M-ITX/ax                | [aa81c59d8a](https://linux-hardware.org/?probe=aa81c59d8a) | Nov 16, 2021 |
| Gigabyte      | H110M-D2P-WG-CF             | [1eac89d527](https://linux-hardware.org/?probe=1eac89d527) | Nov 15, 2021 |
| Gigabyte      | A320M-H-CF                  | [a8e3d44c9e](https://linux-hardware.org/?probe=a8e3d44c9e) | Nov 15, 2021 |
| ASUSTek       | P8H61-M LX R2.0             | [1755230380](https://linux-hardware.org/?probe=1755230380) | Nov 15, 2021 |
| MSI           | MS-7250                     | [84c50a42f3](https://linux-hardware.org/?probe=84c50a42f3) | Nov 15, 2021 |
| Dell          | 0HJ054                      | [f7dee29940](https://linux-hardware.org/?probe=f7dee29940) | Nov 14, 2021 |
| Dell          | 0VRWRC A01                  | [e202cef308](https://linux-hardware.org/?probe=e202cef308) | Nov 14, 2021 |
| Pegatron      | 2AB6                        | [3f03379235](https://linux-hardware.org/?probe=3f03379235) | Nov 14, 2021 |
| HP            | 1998                        | [2e830badd5](https://linux-hardware.org/?probe=2e830badd5) | Nov 14, 2021 |
| ASUSTek       | PRIME Z370-A                | [01b7731b34](https://linux-hardware.org/?probe=01b7731b34) | Nov 14, 2021 |
| ASUSTek       | Z87-EXPERT                  | [445090e2b7](https://linux-hardware.org/?probe=445090e2b7) | Nov 14, 2021 |
| Dell          | 0HJ054                      | [298206106c](https://linux-hardware.org/?probe=298206106c) | Nov 14, 2021 |
| ASUSTek       | ROG STRIX Z590-E GAMING ... | [b1edada81b](https://linux-hardware.org/?probe=b1edada81b) | Nov 13, 2021 |
| ASUSTek       | PRIME A320M-K               | [7ae75212ce](https://linux-hardware.org/?probe=7ae75212ce) | Nov 13, 2021 |
| ASUSTek       | H110M-C                     | [594c61d37a](https://linux-hardware.org/?probe=594c61d37a) | Nov 13, 2021 |
| MSI           | MS-7346                     | [66b30282c8](https://linux-hardware.org/?probe=66b30282c8) | Nov 13, 2021 |
| Acer          | RS780DV                     | [415847f244](https://linux-hardware.org/?probe=415847f244) | Nov 13, 2021 |
| Dell          | 0Y5DDC A00                  | [66188284bd](https://linux-hardware.org/?probe=66188284bd) | Nov 13, 2021 |
| Dell          | 0WMJ54 A01                  | [12892e0de3](https://linux-hardware.org/?probe=12892e0de3) | Nov 13, 2021 |
| Acer          | RS780DV                     | [610927f2e1](https://linux-hardware.org/?probe=610927f2e1) | Nov 12, 2021 |
| Gigabyte      | Z690 UD DDR4                | [8ca8eff19e](https://linux-hardware.org/?probe=8ca8eff19e) | Nov 12, 2021 |
| Gigabyte      | Z690 UD DDR4                | [7f1c2cfc0b](https://linux-hardware.org/?probe=7f1c2cfc0b) | Nov 12, 2021 |
| Dell          | 0WMJ54 A01                  | [eccf63021f](https://linux-hardware.org/?probe=eccf63021f) | Nov 12, 2021 |
| MSI           | B75MA-P45                   | [8196870f95](https://linux-hardware.org/?probe=8196870f95) | Nov 11, 2021 |
| ASUSTek       | PRIME B550M-K               | [e995b26637](https://linux-hardware.org/?probe=e995b26637) | Nov 11, 2021 |
| Gigabyte      | Z370 HD3-CF                 | [867958b2cc](https://linux-hardware.org/?probe=867958b2cc) | Nov 11, 2021 |
| Gigabyte      | X470 AORUS GAMING 5 WIFI... | [27171367d8](https://linux-hardware.org/?probe=27171367d8) | Nov 11, 2021 |
| Gigabyte      | X470 AORUS GAMING 5 WIFI... | [15600f6f21](https://linux-hardware.org/?probe=15600f6f21) | Nov 11, 2021 |
| Dell          | 08HPGT A02                  | [9bf3a3f311](https://linux-hardware.org/?probe=9bf3a3f311) | Nov 10, 2021 |
| ASRock        | Z370 Pro4                   | [e0856ca7fa](https://linux-hardware.org/?probe=e0856ca7fa) | Nov 10, 2021 |
| HP            | 1494                        | [b748a69ed1](https://linux-hardware.org/?probe=b748a69ed1) | Nov 10, 2021 |
| ASRock        | A300M-STX                   | [712e203294](https://linux-hardware.org/?probe=712e203294) | Nov 10, 2021 |
| Gigabyte      | GA-790XTA-UD4               | [6eb5a4107e](https://linux-hardware.org/?probe=6eb5a4107e) | Nov 10, 2021 |
| ASUSTek       | P5QP18L/T5-P5G41E           | [c64c41d162](https://linux-hardware.org/?probe=c64c41d162) | Nov 10, 2021 |
| Lenovo        | MAHOBAY Win8 Pro DPK TPG    | [6f7902132b](https://linux-hardware.org/?probe=6f7902132b) | Nov 10, 2021 |
| Lenovo        | MAHOBAY Win8 Pro DPK TPG    | [da9d9b9d1e](https://linux-hardware.org/?probe=da9d9b9d1e) | Nov 10, 2021 |
| EVGA          | 111-CS-E371                 | [fd202c951f](https://linux-hardware.org/?probe=fd202c951f) | Nov 09, 2021 |
| Gigabyte      | B85M-D2V                    | [6fbd588373](https://linux-hardware.org/?probe=6fbd588373) | Nov 09, 2021 |
| ASUSTek       | M5A78L-M LX/BR              | [2458e67cf5](https://linux-hardware.org/?probe=2458e67cf5) | Nov 09, 2021 |
| MSI           | B350M PRO-VDH               | [7e77378fb3](https://linux-hardware.org/?probe=7e77378fb3) | Nov 07, 2021 |
| HP            | 1998                        | [c2ab98c42f](https://linux-hardware.org/?probe=c2ab98c42f) | Nov 07, 2021 |
| ASUSTek       | H87-PLUS                    | [a699d4683c](https://linux-hardware.org/?probe=a699d4683c) | Nov 07, 2021 |
| MSI           | MAG B550M MORTAR            | [35b24a070f](https://linux-hardware.org/?probe=35b24a070f) | Nov 07, 2021 |
| ASUSTek       | TUF GAMING X570-PLUS        | [3cf5d9865c](https://linux-hardware.org/?probe=3cf5d9865c) | Nov 07, 2021 |
| HP            | 0A98h                       | [faff7d4f1b](https://linux-hardware.org/?probe=faff7d4f1b) | Nov 07, 2021 |
| MSI           | MEG X570 UNIFY              | [651ac91f37](https://linux-hardware.org/?probe=651ac91f37) | Nov 06, 2021 |
| Lenovo        | ThinkCentre A62 9935B5U     | [85db2ee229](https://linux-hardware.org/?probe=85db2ee229) | Nov 06, 2021 |
| MSI           | X399 GAMING PRO CARBON A... | [9ceab9ce1b](https://linux-hardware.org/?probe=9ceab9ce1b) | Nov 06, 2021 |
| Lenovo        | SHARKBAY No DPK             | [92be8f6c8b](https://linux-hardware.org/?probe=92be8f6c8b) | Nov 06, 2021 |
| HP            | Compaq 8200 Elite SFF PC    | [8227932323](https://linux-hardware.org/?probe=8227932323) | Nov 06, 2021 |
| Dell          | 088DT1 A00                  | [1c0647daa9](https://linux-hardware.org/?probe=1c0647daa9) | Nov 05, 2021 |
| Gigabyte      | H510M S2H                   | [1a749d9336](https://linux-hardware.org/?probe=1a749d9336) | Nov 05, 2021 |
| Lenovo        | ThinkCentre M70e 0833A29    | [6d1875bdd9](https://linux-hardware.org/?probe=6d1875bdd9) | Nov 04, 2021 |
| MSI           | A75MA-G55                   | [ccdd789559](https://linux-hardware.org/?probe=ccdd789559) | Nov 04, 2021 |
| MSI           | B150 PC MATE                | [3fdd2f72ac](https://linux-hardware.org/?probe=3fdd2f72ac) | Nov 04, 2021 |
| Dell          | 0NW73C A00                  | [b02dd0e75e](https://linux-hardware.org/?probe=b02dd0e75e) | Nov 04, 2021 |
| MSI           | 3664h                       | [f2547bae94](https://linux-hardware.org/?probe=f2547bae94) | Nov 03, 2021 |
| Gigabyte      | GA-MA74GMT-S2               | [a94050d3b2](https://linux-hardware.org/?probe=a94050d3b2) | Nov 03, 2021 |
| ASUSTek       | ROG CROSSHAIR VIII DARK ... | [f8baadef0e](https://linux-hardware.org/?probe=f8baadef0e) | Nov 03, 2021 |
| Dell          | 0YXT71 A01                  | [6f599a0889](https://linux-hardware.org/?probe=6f599a0889) | Nov 03, 2021 |
| ASUSTek       | Z97-K                       | [1e136c311c](https://linux-hardware.org/?probe=1e136c311c) | Nov 03, 2021 |
| Dell          | 0N826N A03                  | [d7d0a0b507](https://linux-hardware.org/?probe=d7d0a0b507) | Nov 03, 2021 |
| Gigabyte      | EP45-UD3R                   | [30d809be04](https://linux-hardware.org/?probe=30d809be04) | Nov 03, 2021 |
| MSI           | Z370M MORTAR                | [b6d4cdb6b9](https://linux-hardware.org/?probe=b6d4cdb6b9) | Nov 03, 2021 |
| MSI           | Z370M MORTAR                | [af4e356569](https://linux-hardware.org/?probe=af4e356569) | Nov 03, 2021 |
| ASUSTek       | ROG Maximus XI HERO         | [2e8569e851](https://linux-hardware.org/?probe=2e8569e851) | Nov 02, 2021 |
| ASUSTek       | P8H61-M LX R2.0             | [290c114444](https://linux-hardware.org/?probe=290c114444) | Nov 02, 2021 |
| MSI           | 3664h                       | [61d2ff264b](https://linux-hardware.org/?probe=61d2ff264b) | Nov 02, 2021 |
| Lenovo        | ThinkCentre M70e 0833A29    | [ebaea435bf](https://linux-hardware.org/?probe=ebaea435bf) | Nov 02, 2021 |
| ASUSTek       | M3N78                       | [07562bbf08](https://linux-hardware.org/?probe=07562bbf08) | Nov 01, 2021 |
| MSI           | B550-A PRO                  | [ee7c2851a5](https://linux-hardware.org/?probe=ee7c2851a5) | Nov 01, 2021 |
| Acer          | Aspire TC-885 V:1.1         | [e4aeb69a18](https://linux-hardware.org/?probe=e4aeb69a18) | Nov 01, 2021 |
| EVGA          | Z390 FTW                    | [22e9e0f01b](https://linux-hardware.org/?probe=22e9e0f01b) | Nov 01, 2021 |
| Gigabyte      | Z390 AORUS MASTER-CF        | [2bee1e8a30](https://linux-hardware.org/?probe=2bee1e8a30) | Nov 01, 2021 |
| ASUSTek       | CROSSHAIR VI HERO           | [3676c4f8f0](https://linux-hardware.org/?probe=3676c4f8f0) | Nov 01, 2021 |
| EVGA          | 134-KS-E377                 | [27e29303bc](https://linux-hardware.org/?probe=27e29303bc) | Nov 01, 2021 |
| EVGA          | 111-CS-E371                 | [2fc377709d](https://linux-hardware.org/?probe=2fc377709d) | Nov 01, 2021 |
| Lenovo        | SHARKBAY SDK0E50510 WIN     | [068bee7912](https://linux-hardware.org/?probe=068bee7912) | Oct 31, 2021 |
| Gigabyte      | G1.Assassin                 | [40c84c9637](https://linux-hardware.org/?probe=40c84c9637) | Oct 31, 2021 |
| MSI           | MPG Z490 GAMING EDGE WIF... | [5b55e39c35](https://linux-hardware.org/?probe=5b55e39c35) | Oct 31, 2021 |
| ASUSTek       | M2N-E                       | [dfa80f4b9f](https://linux-hardware.org/?probe=dfa80f4b9f) | Oct 31, 2021 |
| HP            | 1587h                       | [0d99c162a3](https://linux-hardware.org/?probe=0d99c162a3) | Oct 31, 2021 |
| HP            | 1587h                       | [2aeea457fd](https://linux-hardware.org/?probe=2aeea457fd) | Oct 31, 2021 |
| Lenovo        | ThinkCentre M70e 0833A29    | [46ebbae78b](https://linux-hardware.org/?probe=46ebbae78b) | Oct 31, 2021 |
| Lenovo        | ThinkCentre M70e 0833A29    | [e13dd10e78](https://linux-hardware.org/?probe=e13dd10e78) | Oct 31, 2021 |
| Gigabyte      | G31M-ES2L                   | [23b6458508](https://linux-hardware.org/?probe=23b6458508) | Oct 30, 2021 |
| ASUSTek       | PRIME H310T R2.0            | [9f69e439bc](https://linux-hardware.org/?probe=9f69e439bc) | Oct 30, 2021 |
| Fujitsu       | D3633-S1 S26361-D3633-S1    | [0184e22e18](https://linux-hardware.org/?probe=0184e22e18) | Oct 30, 2021 |
| ASRock        | 960GM/U3S3 FX               | [7a2ec5ecff](https://linux-hardware.org/?probe=7a2ec5ecff) | Oct 30, 2021 |
| ASUSTek       | P8H61-M LX R2.0             | [4f64db367f](https://linux-hardware.org/?probe=4f64db367f) | Oct 30, 2021 |
| HP            | 0AA8h                       | [40da7a8ae9](https://linux-hardware.org/?probe=40da7a8ae9) | Oct 30, 2021 |
| Dell          | 0N826N A03                  | [ffb75356ef](https://linux-hardware.org/?probe=ffb75356ef) | Oct 30, 2021 |
| MSI           | 3664h                       | [7d44291de8](https://linux-hardware.org/?probe=7d44291de8) | Oct 30, 2021 |
| MSI           | B450M MORTAR MAX            | [cadb142111](https://linux-hardware.org/?probe=cadb142111) | Oct 29, 2021 |
| MSI           | B150M BAZOOKA               | [b396e1c4f4](https://linux-hardware.org/?probe=b396e1c4f4) | Oct 29, 2021 |
| ASUSTek       | M5A97 R2.0                  | [2ce7e668c7](https://linux-hardware.org/?probe=2ce7e668c7) | Oct 29, 2021 |
| Dell          | 0N826N A03                  | [db2a7eba35](https://linux-hardware.org/?probe=db2a7eba35) | Oct 29, 2021 |
| HP            | 0AECh D                     | [665bae2867](https://linux-hardware.org/?probe=665bae2867) | Oct 29, 2021 |
| Dell          | 0C3YXR A00                  | [3fbbe71d21](https://linux-hardware.org/?probe=3fbbe71d21) | Oct 28, 2021 |
| ASUSTek       | ROG CROSSHAIR VIII DARK ... | [89c1a7f472](https://linux-hardware.org/?probe=89c1a7f472) | Oct 28, 2021 |
| Dell          | 0773VG A00                  | [f17f22efdc](https://linux-hardware.org/?probe=f17f22efdc) | Oct 28, 2021 |
| ASUSTek       | CM6870                      | [1575e2c682](https://linux-hardware.org/?probe=1575e2c682) | Oct 28, 2021 |
| MSI           | A88X-G43                    | [1c7a02bd63](https://linux-hardware.org/?probe=1c7a02bd63) | Oct 28, 2021 |
| ASUSTek       | PRIME B450M-GAMING/BR       | [d10e4364a0](https://linux-hardware.org/?probe=d10e4364a0) | Oct 27, 2021 |
| ASUSTek       | P8H61-M LX R2.0             | [28ddff13b5](https://linux-hardware.org/?probe=28ddff13b5) | Oct 27, 2021 |
| Foxconn       | 2ABF                        | [380d5ab9f0](https://linux-hardware.org/?probe=380d5ab9f0) | Oct 27, 2021 |
| Dell          | 05CNYF A01                  | [95530db3a8](https://linux-hardware.org/?probe=95530db3a8) | Oct 27, 2021 |
| Foxconn       | 45CMX/45GMX/45CMX-K         | [0e8d69e9b8](https://linux-hardware.org/?probe=0e8d69e9b8) | Oct 27, 2021 |
| Acer          | Aspire X3450                | [8f27aff70b](https://linux-hardware.org/?probe=8f27aff70b) | Oct 27, 2021 |
| Gigabyte      | GA-970A-UD3                 | [aae0c56d3a](https://linux-hardware.org/?probe=aae0c56d3a) | Oct 27, 2021 |
| MSI           | H61M-P31                    | [e5bf692305](https://linux-hardware.org/?probe=e5bf692305) | Oct 26, 2021 |
| ASRock        | X570 Phantom Gaming-ITX/... | [16a6718949](https://linux-hardware.org/?probe=16a6718949) | Oct 26, 2021 |
| MSI           | Z270 PC MATE                | [24329438d1](https://linux-hardware.org/?probe=24329438d1) | Oct 26, 2021 |
| Unknown       | Unknown                     | [668f61352d](https://linux-hardware.org/?probe=668f61352d) | Oct 26, 2021 |
| ASRock        | Z590M-ITX/ax                | [1fcc4e6895](https://linux-hardware.org/?probe=1fcc4e6895) | Oct 26, 2021 |
| MSI           | MAG Z490 TOMAHAWK           | [f5ede0a97c](https://linux-hardware.org/?probe=f5ede0a97c) | Oct 25, 2021 |
| ASUSTek       | PRIME B350M-A               | [19eba77c24](https://linux-hardware.org/?probe=19eba77c24) | Oct 25, 2021 |
| MSI           | B450 TOMAHAWK MAX           | [97620628a8](https://linux-hardware.org/?probe=97620628a8) | Oct 25, 2021 |
| MSI           | B450 TOMAHAWK MAX           | [f15c4d9b54](https://linux-hardware.org/?probe=f15c4d9b54) | Oct 25, 2021 |
| Gigabyte      | P67A-UD3                    | [ecaeb257a3](https://linux-hardware.org/?probe=ecaeb257a3) | Oct 24, 2021 |
| Dell          | 0V8F20 A01                  | [8e371fe4cb](https://linux-hardware.org/?probe=8e371fe4cb) | Oct 24, 2021 |
| Dell          | 0J3C2F A01                  | [e8cf16b696](https://linux-hardware.org/?probe=e8cf16b696) | Oct 24, 2021 |
| AMI           | Cherry Trail CR             | [9333e233d6](https://linux-hardware.org/?probe=9333e233d6) | Oct 24, 2021 |
| AMI           | Cherry Trail CR             | [7d78a3c31f](https://linux-hardware.org/?probe=7d78a3c31f) | Oct 24, 2021 |
| ASUSTek       | H110M-C                     | [ba3ddf870d](https://linux-hardware.org/?probe=ba3ddf870d) | Oct 24, 2021 |
| ASUSTek       | PRIME Z390-P                | [681b537e82](https://linux-hardware.org/?probe=681b537e82) | Oct 23, 2021 |
| Lenovo        | NO DPK                      | [9c71a67df3](https://linux-hardware.org/?probe=9c71a67df3) | Oct 23, 2021 |
| Acer          | Aspire TC-281               | [5114976821](https://linux-hardware.org/?probe=5114976821) | Oct 23, 2021 |
| ASRock        | N3150DC-ITX                 | [6e3084cf7f](https://linux-hardware.org/?probe=6e3084cf7f) | Oct 23, 2021 |
| ASRock        | N3150DC-ITX                 | [c1808f5d2f](https://linux-hardware.org/?probe=c1808f5d2f) | Oct 23, 2021 |
| Dell          | 0T10XW A00                  | [971b85f5db](https://linux-hardware.org/?probe=971b85f5db) | Oct 23, 2021 |
| ASUSTek       | PRIME X470-PRO              | [48db1afdd3](https://linux-hardware.org/?probe=48db1afdd3) | Oct 23, 2021 |
| Supermicro    | X7DVL                       | [bcbe094156](https://linux-hardware.org/?probe=bcbe094156) | Oct 23, 2021 |
| Dell          | 051FJ8 A02                  | [d04dae2a56](https://linux-hardware.org/?probe=d04dae2a56) | Oct 23, 2021 |
| MSI           | MEG X570 UNIFY              | [7b9e7ec5f4](https://linux-hardware.org/?probe=7b9e7ec5f4) | Oct 23, 2021 |
| ASUSTek       | TUF GAMING X570-PLUS        | [e071387ed6](https://linux-hardware.org/?probe=e071387ed6) | Oct 22, 2021 |
| Lenovo        | NO DPK                      | [ad7c805198](https://linux-hardware.org/?probe=ad7c805198) | Oct 22, 2021 |
| Dell          | 0J3C2F A01                  | [a5ca7f2501](https://linux-hardware.org/?probe=a5ca7f2501) | Oct 22, 2021 |
| Gigabyte      | B550 AORUS PRO              | [f5c3648170](https://linux-hardware.org/?probe=f5c3648170) | Oct 22, 2021 |
| Gigabyte      | B75M-D3H                    | [74c2c9d725](https://linux-hardware.org/?probe=74c2c9d725) | Oct 22, 2021 |
| Dell          | 0N826N A03                  | [5fea6b8b9a](https://linux-hardware.org/?probe=5fea6b8b9a) | Oct 22, 2021 |
| Dell          | 0N826N A03                  | [eb508ab31e](https://linux-hardware.org/?probe=eb508ab31e) | Oct 22, 2021 |
| ICP / iEi     | SA58 V1.01                  | [bca4498e5d](https://linux-hardware.org/?probe=bca4498e5d) | Oct 21, 2021 |
| Dell          | 0200DY A01                  | [c67a8bb677](https://linux-hardware.org/?probe=c67a8bb677) | Oct 21, 2021 |
| Gigabyte      | B450M S2H                   | [71c19b42fc](https://linux-hardware.org/?probe=71c19b42fc) | Oct 21, 2021 |
| Google        | Guado                       | [5aba3d29f4](https://linux-hardware.org/?probe=5aba3d29f4) | Oct 21, 2021 |
| Google        | Guado                       | [2393c52b33](https://linux-hardware.org/?probe=2393c52b33) | Oct 21, 2021 |
| MSI           | B450M PRO-VDH MAX           | [b914028ca9](https://linux-hardware.org/?probe=b914028ca9) | Oct 20, 2021 |
| ASRock        | TRX40 Creator               | [8789f3f1e1](https://linux-hardware.org/?probe=8789f3f1e1) | Oct 20, 2021 |
| ASUSTek       | P5KPL-AM SE                 | [1d5227420f](https://linux-hardware.org/?probe=1d5227420f) | Oct 20, 2021 |
| Acer          | Aspire TC-885 V:1.1         | [b961168b44](https://linux-hardware.org/?probe=b961168b44) | Oct 20, 2021 |
| ASUSTek       | Q87M-E                      | [a549c95cbd](https://linux-hardware.org/?probe=a549c95cbd) | Oct 20, 2021 |
| MSI           | 2A9C                        | [a47442aa1f](https://linux-hardware.org/?probe=a47442aa1f) | Oct 19, 2021 |
| Gigabyte      | H61M-S1                     | [9978664bd7](https://linux-hardware.org/?probe=9978664bd7) | Oct 19, 2021 |
| Gigabyte      | H61M-S1                     | [b0fb0061f2](https://linux-hardware.org/?probe=b0fb0061f2) | Oct 19, 2021 |
| ASUSTek       | P7P55 LX                    | [d2e3eb969f](https://linux-hardware.org/?probe=d2e3eb969f) | Oct 18, 2021 |
| ASRock        | B450 Gaming K4              | [92647c0170](https://linux-hardware.org/?probe=92647c0170) | Oct 18, 2021 |
| ASRock        | B450 Gaming K4              | [c7372168fd](https://linux-hardware.org/?probe=c7372168fd) | Oct 18, 2021 |
| ASRock        | Z87 Extreme4                | [2ec87a3f6f](https://linux-hardware.org/?probe=2ec87a3f6f) | Oct 18, 2021 |
| Lenovo        | SHARKBAY SDK0E50512 STD     | [20ca9b4679](https://linux-hardware.org/?probe=20ca9b4679) | Oct 18, 2021 |
| ASUSTek       | ROG STRIX X570-E GAMING     | [7e6f5e6e9f](https://linux-hardware.org/?probe=7e6f5e6e9f) | Oct 18, 2021 |
| Acer          | Aspire XC600 v1.0           | [56dd661396](https://linux-hardware.org/?probe=56dd661396) | Oct 18, 2021 |
| Gigabyte      | GA-MA785GM-US2H             | [6522d9dc18](https://linux-hardware.org/?probe=6522d9dc18) | Oct 18, 2021 |
| HP            | 1998                        | [3228ce7734](https://linux-hardware.org/?probe=3228ce7734) | Oct 18, 2021 |
| EPSON DIRE... | ST170E                      | [dfa0ed56ab](https://linux-hardware.org/?probe=dfa0ed56ab) | Oct 18, 2021 |
| Apple         | Mac-F42C88C8 Proto1         | [fcc82222d9](https://linux-hardware.org/?probe=fcc82222d9) | Oct 17, 2021 |
| MSI           | Z77A-G45                    | [3d516c23c5](https://linux-hardware.org/?probe=3d516c23c5) | Oct 17, 2021 |
| ASUSTek       | M32CD_A_F_K20CD_K31CD       | [7313df4bd9](https://linux-hardware.org/?probe=7313df4bd9) | Oct 17, 2021 |
| Gigabyte      | Z77-D3H                     | [f7ffa54af0](https://linux-hardware.org/?probe=f7ffa54af0) | Oct 17, 2021 |
| Gigabyte      | M68MT-S2P                   | [d10202fe29](https://linux-hardware.org/?probe=d10202fe29) | Oct 17, 2021 |
| ASRock        | B560M-HDV                   | [200bfff8ba](https://linux-hardware.org/?probe=200bfff8ba) | Oct 17, 2021 |
| Gigabyte      | B550M AORUS ELITE           | [122a03804e](https://linux-hardware.org/?probe=122a03804e) | Oct 17, 2021 |
| Intel         | DG31PR AAE39516-304         | [b6addf8d7b](https://linux-hardware.org/?probe=b6addf8d7b) | Oct 17, 2021 |
| MSI           | MPG X570 GAMING PLUS        | [dd6513e107](https://linux-hardware.org/?probe=dd6513e107) | Oct 17, 2021 |
| ASUSTek       | P8H61-M LX R2.0             | [35b768567f](https://linux-hardware.org/?probe=35b768567f) | Oct 16, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [8c1989ae75](https://linux-hardware.org/?probe=8c1989ae75) | Oct 16, 2021 |
| HP            | 870C                        | [8b056a8a9f](https://linux-hardware.org/?probe=8b056a8a9f) | Oct 16, 2021 |
| Lenovo        | SHARKBAY 31900058 STD       | [8b50d81590](https://linux-hardware.org/?probe=8b50d81590) | Oct 16, 2021 |
| Dell          | 0G214D A00                  | [69857eb3a8](https://linux-hardware.org/?probe=69857eb3a8) | Oct 16, 2021 |
| Lenovo        | SHARKBAY 0B98401 WIN        | [7f15c21293](https://linux-hardware.org/?probe=7f15c21293) | Oct 16, 2021 |
| ASUSTek       | B250 MINING EXPERT          | [6c2357c3a8](https://linux-hardware.org/?probe=6c2357c3a8) | Oct 16, 2021 |
| Lenovo        | SHARKBAY 31900058 STD       | [6e15fcad52](https://linux-hardware.org/?probe=6e15fcad52) | Oct 15, 2021 |
| Pegatron      | Eureka3                     | [6499d1cf77](https://linux-hardware.org/?probe=6499d1cf77) | Oct 15, 2021 |
| Pegatron      | Eureka3                     | [a456734f94](https://linux-hardware.org/?probe=a456734f94) | Oct 15, 2021 |
| ASUSTek       | PRIME B350-PLUS             | [e646d30e4f](https://linux-hardware.org/?probe=e646d30e4f) | Oct 15, 2021 |
| ASUSTek       | Pro WS X570-ACE             | [152b670fcb](https://linux-hardware.org/?probe=152b670fcb) | Oct 15, 2021 |
| ASRock        | AB350 Gaming-ITX/ac         | [22b047f0a1](https://linux-hardware.org/?probe=22b047f0a1) | Oct 15, 2021 |
| ASRock        | 990FX Extreme3              | [5de7270820](https://linux-hardware.org/?probe=5de7270820) | Oct 11, 2021 |
| ASRock        | 990FX Extreme3              | [b4eb4dbe24](https://linux-hardware.org/?probe=b4eb4dbe24) | Oct 11, 2021 |
| Dell          | 0Y2MRG A00                  | [d60ca7a452](https://linux-hardware.org/?probe=d60ca7a452) | Oct 09, 2021 |
| HP            | 3647h                       | [729a9e9683](https://linux-hardware.org/?probe=729a9e9683) | Oct 05, 2021 |
| ASRock        | X370 Killer SLI/ac          | [52d4dd8f39](https://linux-hardware.org/?probe=52d4dd8f39) | Oct 02, 2021 |
| Dell          | 0RY007                      | [4e574a8988](https://linux-hardware.org/?probe=4e574a8988) | Oct 02, 2021 |
| Huanan        | X79 INTEL (INTEL Xeon E5... | [db26b44773](https://linux-hardware.org/?probe=db26b44773) | Oct 02, 2021 |
| Huanan        | X79 INTEL (INTEL Xeon E5... | [3be52ed98f](https://linux-hardware.org/?probe=3be52ed98f) | Oct 02, 2021 |
| Gigabyte      | B550 AORUS ELITE V2         | [b6f82cf92b](https://linux-hardware.org/?probe=b6f82cf92b) | Sep 30, 2021 |
| Intel         | DG41WV AAE90316-103         | [0055a963ef](https://linux-hardware.org/?probe=0055a963ef) | Sep 30, 2021 |
| ASUSTek       | A68HM-PLUS                  | [8ea8e6afe8](https://linux-hardware.org/?probe=8ea8e6afe8) | Sep 30, 2021 |
| ASRock        | 990FX Extreme4              | [9c631b51b1](https://linux-hardware.org/?probe=9c631b51b1) | Sep 28, 2021 |
| Gigabyte      | H81M-D2V                    | [e8749db36a](https://linux-hardware.org/?probe=e8749db36a) | Sep 27, 2021 |
| Gigabyte      | H81M-D2V                    | [aa1a6086e7](https://linux-hardware.org/?probe=aa1a6086e7) | Sep 27, 2021 |
| Gigabyte      | H81M-D2V                    | [b05cdb0bab](https://linux-hardware.org/?probe=b05cdb0bab) | Sep 26, 2021 |
| ASRock        | X399M Taichi                | [eba541c6b9](https://linux-hardware.org/?probe=eba541c6b9) | Sep 25, 2021 |
| Gigabyte      | H81M-S                      | [357f7466e6](https://linux-hardware.org/?probe=357f7466e6) | Sep 25, 2021 |
| ASRock        | Z390M Pro4                  | [138ae00012](https://linux-hardware.org/?probe=138ae00012) | Sep 23, 2021 |
| Medion        | B360H4-EM V1.0              | [1985156471](https://linux-hardware.org/?probe=1985156471) | Sep 19, 2021 |
| Gigabyte      | B85M-D3H                    | [9de4382874](https://linux-hardware.org/?probe=9de4382874) | Sep 15, 2021 |
| HP            | 3032h                       | [3fad749d1a](https://linux-hardware.org/?probe=3fad749d1a) | Sep 12, 2021 |
| Huanan        | X99 F8D V2.2                | [c080ec772f](https://linux-hardware.org/?probe=c080ec772f) | Sep 03, 2021 |
| Huanan        | X99 F8D V2.2                | [30fe8d6bb3](https://linux-hardware.org/?probe=30fe8d6bb3) | Aug 26, 2021 |
| ASUSTek       | ROG ZENITH II EXTREME       | [1a371ea24e](https://linux-hardware.org/?probe=1a371ea24e) | Aug 16, 2021 |
| Fujitsu       | D3400-B2 S26361-D3400-B2    | [067c79a9fe](https://linux-hardware.org/?probe=067c79a9fe) | Aug 13, 2021 |
| MSI           | MAG B550M MORTAR            | [912b2a77a2](https://linux-hardware.org/?probe=912b2a77a2) | Aug 05, 2021 |
| Huanan        | X99 F8D V2.2                | [74e4c61bbf](https://linux-hardware.org/?probe=74e4c61bbf) | Jul 23, 2021 |
| Huanan        | X99 F8D V2.2                | [02ad72fb54](https://linux-hardware.org/?probe=02ad72fb54) | Jul 21, 2021 |
| Gigabyte      | F2A55M-HD2                  | [6a69f09403](https://linux-hardware.org/?probe=6a69f09403) | Jul 15, 2021 |

System
------

Kernel
------

Version of the Linux kernel

![Kernel](./images/pie_chart/os_kernel.svg)


| Version                     | Desktops | Percent |
|-----------------------------|----------|---------|
| 5.13.0-22-generic           | 88       | 25.43%  |
| 5.13.0-21-generic           | 82       | 23.7%   |
| 5.13.0-20-generic           | 70       | 20.23%  |
| 5.13.0-19-generic           | 58       | 16.76%  |
| 5.13.0-16-generic           | 12       | 3.47%   |
| 5.13.0-23-generic           | 9        | 2.6%    |
| 5.15.2-051502-generic       | 5        | 1.45%   |
| 5.13.0-14-generic           | 3        | 0.87%   |
| 5.14.0-051400rc7-lowlatency | 2        | 0.58%   |
| 5.13.0-22-lowlatency        | 2        | 0.58%   |
| 5.13.0-17-generic           | 2        | 0.58%   |
| 5.13.0-12-generic           | 2        | 0.58%   |
| 5.8.0-50-generic            | 1        | 0.29%   |
| 5.15.0                      | 1        | 0.29%   |
| 5.14.15                     | 1        | 0.29%   |
| 5.14.14-xanmod2             | 1        | 0.29%   |
| 5.14.14-051414-generic      | 1        | 0.29%   |
| 5.13.0-192110311031-generic | 1        | 0.29%   |
| 5.13.0-1010-oracle          | 1        | 0.29%   |
| 5.13.0-051300-generic       | 1        | 0.29%   |
| 5.11.0-44-generic           | 1        | 0.29%   |
| 5.11.0-25-generic           | 1        | 0.29%   |
| 5.11.0-20-generic           | 1        | 0.29%   |

Kernel Family
-------------

Linux kernel without a distro release

![Kernel Family](./images/pie_chart/os_kernel_family.svg)


| Version | Desktops | Percent |
|---------|----------|---------|
| 5.13.0  | 320      | 95.52%  |
| 5.15.2  | 5        | 1.49%   |
| 5.11.0  | 3        | 0.9%    |
| 5.14.14 | 2        | 0.6%    |
| 5.14.0  | 2        | 0.6%    |
| 5.8.0   | 1        | 0.3%    |
| 5.15.0  | 1        | 0.3%    |
| 5.14.15 | 1        | 0.3%    |

Kernel Major Ver.
-----------------

Linux kernel major version

![Kernel Major Ver.](./images/pie_chart/os_kernel_major.svg)


| Version | Desktops | Percent |
|---------|----------|---------|
| 5.13    | 320      | 95.52%  |
| 5.15    | 6        | 1.79%   |
| 5.14    | 5        | 1.49%   |
| 5.11    | 3        | 0.9%    |
| 5.8     | 1        | 0.3%    |

Arch
----

OS architecture (x86_64, i586, etc.)

![Arch](./images/pie_chart/os_arch.svg)


| Name   | Desktops | Percent |
|--------|----------|---------|
| x86_64 | 332      | 100%    |

DE
--

Desktop Environment

![DE](./images/pie_chart/os_de.svg)


| Name            | Desktops | Percent |
|-----------------|----------|---------|
| GNOME           | 305      | 91.04%  |
| Unknown         | 16       | 4.78%   |
| X-Cinnamon      | 6        | 1.79%   |
| GNOME Flashback | 3        | 0.9%    |
| i3              | 2        | 0.6%    |
| Unity           | 1        | 0.3%    |
| sway            | 1        | 0.3%    |
| Cinnamon        | 1        | 0.3%    |

Display Server
--------------

X11 or Wayland

![Display Server](./images/pie_chart/os_display_server.svg)


| Name    | Desktops | Percent |
|---------|----------|---------|
| Wayland | 187      | 55.82%  |
| X11     | 136      | 40.6%   |
| Tty     | 6        | 1.79%   |
| Unknown | 6        | 1.79%   |

Display Manager
---------------

SDDM, LightDM, etc.

![Display Manager](./images/pie_chart/os_display_manager.svg)


| Name    | Desktops | Percent |
|---------|----------|---------|
| GDM3    | 230      | 69.28%  |
| GDM     | 70       | 21.08%  |
| Unknown | 24       | 7.23%   |
| LightDM | 7        | 2.11%   |
| SDDM    | 1        | 0.3%    |

OS Lang
-------

Language

![OS Lang](./images/pie_chart/os_lang.svg)


| Lang  | Desktops | Percent |
|-------|----------|---------|
| en_US | 126      | 37.5%   |
| de_DE | 61       | 18.15%  |
| fr_FR | 17       | 5.06%   |
| it_IT | 14       | 4.17%   |
| en_AU | 12       | 3.57%   |
| pt_BR | 10       | 2.98%   |
| en_GB | 10       | 2.98%   |
| pl_PL | 9        | 2.68%   |
| en_CA | 9        | 2.68%   |
| ru_RU | 6        | 1.79%   |
| C     | 6        | 1.79%   |
| nl_NL | 5        | 1.49%   |
| es_ES | 5        | 1.49%   |
| zh_CN | 4        | 1.19%   |
| ja_JP | 4        | 1.19%   |
| en_IN | 4        | 1.19%   |
| es_MX | 3        | 0.89%   |
| es_AR | 3        | 0.89%   |
| de_CH | 3        | 0.89%   |
| sv_SE | 2        | 0.6%    |
| ru_UA | 2        | 0.6%    |
| nb_NO | 2        | 0.6%    |
| hu_HU | 2        | 0.6%    |
| fr_BE | 2        | 0.6%    |
| et_EE | 2        | 0.6%    |
| es_PE | 2        | 0.6%    |
| el_GR | 2        | 0.6%    |
| tr_TR | 1        | 0.3%    |
| sk_SK | 1        | 0.3%    |
| ko_KR | 1        | 0.3%    |
| hr_HR | 1        | 0.3%    |
| es_BO | 1        | 0.3%    |
| en_PH | 1        | 0.3%    |
| da_DK | 1        | 0.3%    |
| cs_CZ | 1        | 0.3%    |
| ar_EG | 1        | 0.3%    |

Boot Mode
---------

EFI or BIOS

![Boot Mode](./images/pie_chart/os_boot_mode.svg)


| Mode | Desktops | Percent |
|------|----------|---------|
| BIOS | 257      | 77.41%  |
| EFI  | 75       | 22.59%  |

Filesystem
----------

Type of filesystem

![Filesystem](./images/pie_chart/os_filesystem.svg)


| Type    | Desktops | Percent |
|---------|----------|---------|
| Ext4    | 294      | 88.55%  |
| Overlay | 19       | 5.72%   |
| Zfs     | 11       | 3.31%   |
| Btrfs   | 5        | 1.51%   |
| Xfs     | 1        | 0.3%    |
| Ext3    | 1        | 0.3%    |
| Ext2    | 1        | 0.3%    |

Part. scheme
------------

Scheme of partitioning

![Part. scheme](./images/pie_chart/os_part_scheme.svg)


| Type    | Desktops | Percent |
|---------|----------|---------|
| Unknown | 261      | 78.61%  |
| GPT     | 64       | 19.28%  |
| MBR     | 7        | 2.11%   |

Dual Boot with Linux/BSD
------------------------

Hosting more than one Linux/BSD

![Dual Boot with Linux/BSD](./images/pie_chart/os_dual_boot.svg)


| Dual boot | Desktops | Percent |
|-----------|----------|---------|
| No        | 262      | 78.44%  |
| Yes       | 72       | 21.56%  |

Dual Boot (Win)
---------------

Hosting Linux and Windows

![Dual Boot (Win)](./images/pie_chart/os_dual_boot_win.svg)


| Dual boot | Desktops | Percent |
|-----------|----------|---------|
| No        | 178      | 53.61%  |
| Yes       | 154      | 46.39%  |

Board
-----

Vendor
------

Motherboard manufacturer

![Vendor](./images/pie_chart/node_vendor.svg)


| Name                | Desktops | Percent |
|---------------------|----------|---------|
| ASUSTek Computer    | 80       | 24.1%   |
| Gigabyte Technology | 56       | 16.87%  |
| MSI                 | 53       | 15.96%  |
| Dell                | 34       | 10.24%  |
| ASRock              | 31       | 9.34%   |
| Hewlett-Packard     | 25       | 7.53%   |
| Lenovo              | 14       | 4.22%   |
| Acer                | 7        | 2.11%   |
| Pegatron            | 4        | 1.2%    |
| Medion              | 3        | 0.9%    |
| Fujitsu             | 3        | 0.9%    |
| Foxconn             | 3        | 0.9%    |
| EVGA                | 3        | 0.9%    |
| Intel               | 2        | 0.6%    |
| Huanan              | 2        | 0.6%    |
| Supermicro          | 1        | 0.3%    |
| Positivo            | 1        | 0.3%    |
| OEM                 | 1        | 0.3%    |
| JGINYUE             | 1        | 0.3%    |
| ICP / iEi           | 1        | 0.3%    |
| Google              | 1        | 0.3%    |
| EPSON DIRECT        | 1        | 0.3%    |
| ECS                 | 1        | 0.3%    |
| Biostar             | 1        | 0.3%    |
| Apple               | 1        | 0.3%    |
| AMI                 | 1        | 0.3%    |
| Unknown             | 1        | 0.3%    |

Model
-----

Motherboard model

![Model](./images/pie_chart/node_model.svg)


| Name                              | Desktops | Percent |
|-----------------------------------|----------|---------|
| Dell OptiPlex 7010                | 5        | 1.51%   |
| ASUS All Series                   | 5        | 1.51%   |
| ASUS PRIME B450M-A                | 4        | 1.2%    |
| MSI MS-7D25                       | 3        | 0.9%    |
| MSI MS-7C35                       | 3        | 0.9%    |
| Gigabyte X570 AORUS MASTER        | 3        | 0.9%    |
| ASUS TUF GAMING X570-PLUS         | 3        | 0.9%    |
| MSI MS-7C94                       | 2        | 0.6%    |
| MSI MS-7C80                       | 2        | 0.6%    |
| MSI MS-7C52                       | 2        | 0.6%    |
| MSI MS-7C37                       | 2        | 0.6%    |
| MSI MS-7C02                       | 2        | 0.6%    |
| MSI MS-7B09                       | 2        | 0.6%    |
| MSI MS-7A38                       | 2        | 0.6%    |
| MSI MS-7821                       | 2        | 0.6%    |
| HP Z800 Workstation               | 2        | 0.6%    |
| HP EliteDesk 800 G1 SFF           | 2        | 0.6%    |
| HP Compaq 8200 Elite CMT PC       | 2        | 0.6%    |
| Gigabyte GA-78LMT-USB3            | 2        | 0.6%    |
| Dell OptiPlex 9010                | 2        | 0.6%    |
| Dell OptiPlex 780                 | 2        | 0.6%    |
| Dell OptiPlex 3020                | 2        | 0.6%    |
| Dell OptiPlex 3010                | 2        | 0.6%    |
| Dell Inspiron 545                 | 2        | 0.6%    |
| ASUS TUF GAMING Z690-PLUS WIFI D4 | 2        | 0.6%    |
| ASUS PRIME A320M-K                | 2        | 0.6%    |
| ASUS M4A89GTD-PRO/USB3            | 2        | 0.6%    |
| ASUS H110M-C                      | 2        | 0.6%    |
| ASUS CROSSHAIR VI HERO            | 2        | 0.6%    |
| ASUS B250 MINING EXPERT           | 2        | 0.6%    |
| ASRock Z590M-ITX/ax               | 2        | 0.6%    |
| ASRock Z370 Pro4                  | 2        | 0.6%    |
| Supermicro X7DVL                  | 1        | 0.3%    |
| Positivo POS-MIH61CF              | 1        | 0.3%    |
| Pegatron WH637AA-UUW CQ5307SC     | 1        | 0.3%    |
| Pegatron VC903AA-ABF p6145fr      | 1        | 0.3%    |
| Pegatron p6730de                  | 1        | 0.3%    |
| Pegatron HPE-590t                 | 1        | 0.3%    |
| OEM TOP77D                        | 1        | 0.3%    |
| MSI Pro 3000/3080                 | 1        | 0.3%    |
| MSI PPPPP-CCC#MMMMMMMM            | 1        | 0.3%    |
| MSI MS-7D16                       | 1        | 0.3%    |
| MSI MS-7D13                       | 1        | 0.3%    |
| MSI MS-7D06                       | 1        | 0.3%    |
| MSI MS-7C88                       | 1        | 0.3%    |
| MSI MS-7C81                       | 1        | 0.3%    |
| MSI MS-7C79                       | 1        | 0.3%    |
| MSI MS-7C56                       | 1        | 0.3%    |
| MSI MS-7B89                       | 1        | 0.3%    |
| MSI MS-7B84                       | 1        | 0.3%    |
| MSI MS-7B54                       | 1        | 0.3%    |
| MSI MS-7B51                       | 1        | 0.3%    |
| MSI MS-7A75                       | 1        | 0.3%    |
| MSI MS-7A72                       | 1        | 0.3%    |
| MSI MS-7A62                       | 1        | 0.3%    |
| MSI MS-7998                       | 1        | 0.3%    |
| MSI MS-7982                       | 1        | 0.3%    |
| MSI MS-7971                       | 1        | 0.3%    |
| MSI MS-7917                       | 1        | 0.3%    |
| MSI MS-7846                       | 1        | 0.3%    |

Model Family
------------

Motherboard model prefix

![Model Family](./images/pie_chart/node_model_family.svg)


| Name                   | Desktops | Percent |
|------------------------|----------|---------|
| Dell OptiPlex          | 21       | 6.33%   |
| ASUS PRIME             | 18       | 5.42%   |
| Lenovo ThinkCentre     | 9        | 2.71%   |
| HP Compaq              | 9        | 2.71%   |
| ASUS ROG               | 8        | 2.41%   |
| Gigabyte X570          | 6        | 1.81%   |
| ASUS TUF               | 6        | 1.81%   |
| HP EliteDesk           | 5        | 1.51%   |
| Dell Inspiron          | 5        | 1.51%   |
| ASUS All               | 5        | 1.51%   |
| Dell Precision         | 4        | 1.2%    |
| Acer Aspire            | 4        | 1.2%    |
| MSI MS-7D25            | 3        | 0.9%    |
| MSI MS-7C35            | 3        | 0.9%    |
| Gigabyte GA-78LMT-USB3 | 3        | 0.9%    |
| MSI MS-7C94            | 2        | 0.6%    |
| MSI MS-7C80            | 2        | 0.6%    |
| MSI MS-7C52            | 2        | 0.6%    |
| MSI MS-7C37            | 2        | 0.6%    |
| MSI MS-7C02            | 2        | 0.6%    |
| MSI MS-7B09            | 2        | 0.6%    |
| MSI MS-7A38            | 2        | 0.6%    |
| MSI MS-7821            | 2        | 0.6%    |
| HP Z800                | 2        | 0.6%    |
| HP ProDesk             | 2        | 0.6%    |
| Gigabyte X470          | 2        | 0.6%    |
| Gigabyte B550          | 2        | 0.6%    |
| Gigabyte B450M         | 2        | 0.6%    |
| Dell XPS               | 2        | 0.6%    |
| ASUS STRIX             | 2        | 0.6%    |
| ASUS P8H61             | 2        | 0.6%    |
| ASUS M5A97             | 2        | 0.6%    |
| ASUS M5A78L-M          | 2        | 0.6%    |
| ASUS M4A89GTD-PRO      | 2        | 0.6%    |
| ASUS H110M-C           | 2        | 0.6%    |
| ASUS CROSSHAIR         | 2        | 0.6%    |
| ASUS B250              | 2        | 0.6%    |
| ASRock Z590M-ITX       | 2        | 0.6%    |
| ASRock Z370            | 2        | 0.6%    |
| ASRock 990FX           | 2        | 0.6%    |
| Supermicro X7DVL       | 1        | 0.3%    |
| Positivo POS-MIH61CF   | 1        | 0.3%    |
| Pegatron WH637AA-UUW   | 1        | 0.3%    |
| Pegatron VC903AA-ABF   | 1        | 0.3%    |
| Pegatron p6730de       | 1        | 0.3%    |
| Pegatron HPE-590t      | 1        | 0.3%    |
| OEM TOP77D             | 1        | 0.3%    |
| MSI Pro                | 1        | 0.3%    |
| MSI PPPPP-CCC#MMMMMMMM | 1        | 0.3%    |
| MSI MS-7D16            | 1        | 0.3%    |
| MSI MS-7D13            | 1        | 0.3%    |
| MSI MS-7D06            | 1        | 0.3%    |
| MSI MS-7C88            | 1        | 0.3%    |
| MSI MS-7C81            | 1        | 0.3%    |
| MSI MS-7C79            | 1        | 0.3%    |
| MSI MS-7C56            | 1        | 0.3%    |
| MSI MS-7B89            | 1        | 0.3%    |
| MSI MS-7B84            | 1        | 0.3%    |
| MSI MS-7B54            | 1        | 0.3%    |
| MSI MS-7B51            | 1        | 0.3%    |

MFG Year
--------

Motherboard manufacture year

![MFG Year](./images/pie_chart/node_year.svg)


| Year | Desktops | Percent |
|------|----------|---------|
| 2021 | 70       | 21.08%  |
| 2020 | 37       | 11.14%  |
| 2019 | 31       | 9.34%   |
| 2018 | 27       | 8.13%   |
| 2010 | 23       | 6.93%   |
| 2014 | 20       | 6.02%   |
| 2013 | 20       | 6.02%   |
| 2011 | 20       | 6.02%   |
| 2015 | 19       | 5.72%   |
| 2012 | 17       | 5.12%   |
| 2009 | 15       | 4.52%   |
| 2016 | 12       | 3.61%   |
| 2008 | 11       | 3.31%   |
| 2007 | 5        | 1.51%   |
| 2017 | 4        | 1.2%    |
| 2006 | 1        | 0.3%    |

Form Factor
-----------

Physical design of the computer

![Form Factor](./images/pie_chart/node_formfactor.svg)


| Name    | Desktops | Percent |
|---------|----------|---------|
| Desktop | 332      | 100%    |

Secure Boot
-----------

Enabled or disabled

![Secure Boot](./images/pie_chart/node_secureboot.svg)


| State    | Desktops | Percent |
|----------|----------|---------|
| Disabled | 326      | 98.19%  |
| Enabled  | 6        | 1.81%   |

Coreboot
--------

Have coreboot on board

![Coreboot](./images/pie_chart/node_coreboot.svg)


| Used | Desktops | Percent |
|------|----------|---------|
| No   | 330      | 99.4%   |
| Yes  | 2        | 0.6%    |

RAM Size
--------

Total RAM memory

![RAM Size](./images/pie_chart/node_ram_total.svg)


| Size in GB  | Desktops | Percent |
|-------------|----------|---------|
| 16.01-24.0  | 77       | 23.12%  |
| 32.01-64.0  | 67       | 20.12%  |
| 8.01-16.0   | 61       | 18.32%  |
| 4.01-8.0    | 60       | 18.02%  |
| 3.01-4.0    | 38       | 11.41%  |
| 64.01-256.0 | 17       | 5.11%   |
| 24.01-32.0  | 7        | 2.1%    |
| 1.01-2.0    | 5        | 1.5%    |
| 2.01-3.0    | 1        | 0.3%    |

RAM Used
--------

Used RAM memory

![RAM Used](./images/pie_chart/node_ram_used.svg)


| Used GB    | Desktops | Percent |
|------------|----------|---------|
| 1.01-2.0   | 145      | 42.4%   |
| 2.01-3.0   | 94       | 27.49%  |
| 4.01-8.0   | 50       | 14.62%  |
| 3.01-4.0   | 39       | 11.4%   |
| 8.01-16.0  | 9        | 2.63%   |
| 0.51-1.0   | 3        | 0.88%   |
| 16.01-24.0 | 2        | 0.58%   |

Total Drives
------------

Number of drives on board

![Total Drives](./images/pie_chart/node_total_drives.svg)


| Drives | Desktops | Percent |
|--------|----------|---------|
| 1      | 126      | 37.61%  |
| 2      | 96       | 28.66%  |
| 3      | 44       | 13.13%  |
| 4      | 33       | 9.85%   |
| 5      | 20       | 5.97%   |
| 6      | 6        | 1.79%   |
| 7      | 5        | 1.49%   |
| 11     | 2        | 0.6%    |
| 9      | 1        | 0.3%    |
| 8      | 1        | 0.3%    |
| 0      | 1        | 0.3%    |

Has CD-ROM
----------

Has CD-ROM on board

![Has CD-ROM](./images/pie_chart/node_has_cdrom.svg)


| Presented | Desktops | Percent |
|-----------|----------|---------|
| No        | 170      | 51.05%  |
| Yes       | 163      | 48.95%  |

Has Ethernet
------------

Has Ethernet on board

![Has Ethernet](./images/pie_chart/node_has_ethernet.svg)


| Presented | Desktops | Percent |
|-----------|----------|---------|
| Yes       | 329      | 99.1%   |
| No        | 3        | 0.9%    |

Has WiFi
--------

Has WiFi module

![Has WiFi](./images/pie_chart/node_has_wifi.svg)


| Presented | Desktops | Percent |
|-----------|----------|---------|
| No        | 174      | 52.41%  |
| Yes       | 158      | 47.59%  |

Has Bluetooth
-------------

Has Bluetooth module

![Has Bluetooth](./images/pie_chart/node_has_bluetooth.svg)


| Presented | Desktops | Percent |
|-----------|----------|---------|
| No        | 214      | 64.26%  |
| Yes       | 119      | 35.74%  |

Location
--------

Country
-------

Geographic location (country)

![Country](./images/pie_chart/node_location.svg)


| Country      | Desktops | Percent |
|--------------|----------|---------|
| USA          | 76       | 22.89%  |
| Germany      | 66       | 19.88%  |
| France       | 19       | 5.72%   |
| Italy        | 18       | 5.42%   |
| Poland       | 12       | 3.61%   |
| Brazil       | 12       | 3.61%   |
| Australia    | 12       | 3.61%   |
| Canada       | 11       | 3.31%   |
| UK           | 9        | 2.71%   |
| Russia       | 7        | 2.11%   |
| Switzerland  | 6        | 1.81%   |
| Netherlands  | 6        | 1.81%   |
| Mexico       | 6        | 1.81%   |
| Ukraine      | 5        | 1.51%   |
| China        | 5        | 1.51%   |
| Japan        | 4        | 1.2%    |
| India        | 4        | 1.2%    |
| Greece       | 4        | 1.2%    |
| Sweden       | 3        | 0.9%    |
| Spain        | 3        | 0.9%    |
| Norway       | 3        | 0.9%    |
| Lithuania    | 3        | 0.9%    |
| Belgium      | 3        | 0.9%    |
| Argentina    | 3        | 0.9%    |
| Turkey       | 2        | 0.6%    |
| South Korea  | 2        | 0.6%    |
| Peru         | 2        | 0.6%    |
| Hungary      | 2        | 0.6%    |
| Estonia      | 2        | 0.6%    |
| Denmark      | 2        | 0.6%    |
| Thailand     | 1        | 0.3%    |
| South Africa | 1        | 0.3%    |
| Slovenia     | 1        | 0.3%    |
| Serbia       | 1        | 0.3%    |
| Saudi Arabia | 1        | 0.3%    |
| Portugal     | 1        | 0.3%    |
| Philippines  | 1        | 0.3%    |
| New Zealand  | 1        | 0.3%    |
| Malaysia     | 1        | 0.3%    |
| Kazakhstan   | 1        | 0.3%    |
| Israel       | 1        | 0.3%    |
| Iran         | 1        | 0.3%    |
| Georgia      | 1        | 0.3%    |
| Finland      | 1        | 0.3%    |
| Egypt        | 1        | 0.3%    |
| Czechia      | 1        | 0.3%    |
| Croatia      | 1        | 0.3%    |
| Chile        | 1        | 0.3%    |
| Bulgaria     | 1        | 0.3%    |
| Bolivia      | 1        | 0.3%    |

City
----

Geographic location (city)

![City](./images/pie_chart/node_city.svg)


| City              | Desktops | Percent |
|-------------------|----------|---------|
| Cleveland         | 10       | 2.99%   |
| Berlin            | 5        | 1.49%   |
| Sydney            | 4        | 1.19%   |
| Milan             | 4        | 1.19%   |
| Wuhan             | 3        | 0.9%    |
| Warsaw            | 3        | 0.9%    |
| Wroclaw           | 2        | 0.6%    |
| Wittlich          | 2        | 0.6%    |
| Washington        | 2        | 0.6%    |
| Vladivostok       | 2        | 0.6%    |
| Vilnius           | 2        | 0.6%    |
| Tijuana           | 2        | 0.6%    |
| Stuttgart         | 2        | 0.6%    |
| S??o Paulo        | 2        | 0.6%    |
| Poznan            | 2        | 0.6%    |
| Perth             | 2        | 0.6%    |
| Oslo              | 2        | 0.6%    |
| Oakland           | 2        | 0.6%    |
| Munich            | 2        | 0.6%    |
| Madrid            | 2        | 0.6%    |
| Lima              | 2        | 0.6%    |
| Landsberg am Lech | 2        | 0.6%    |
| Kyiv              | 2        | 0.6%    |
| Houston           | 2        | 0.6%    |
| Essen             | 2        | 0.6%    |
| El Paso           | 2        | 0.6%    |
| Cannes            | 2        | 0.6%    |
| Brussels          | 2        | 0.6%    |
| Brisbane          | 2        | 0.6%    |
| Bodenheim         | 2        | 0.6%    |
| Birmingham        | 2        | 0.6%    |
| Belo Horizonte    | 2        | 0.6%    |
| Athens            | 2        | 0.6%    |
| Amsterdam         | 2        | 0.6%    |
| Zevenaar          | 1        | 0.3%    |
| Zaventem          | 1        | 0.3%    |
| Zagreb            | 1        | 0.3%    |
| Yakima            | 1        | 0.3%    |
| Xanthi            | 1        | 0.3%    |
| Wynau             | 1        | 0.3%    |
| Wuppertal         | 1        | 0.3%    |
| Woodstock         | 1        | 0.3%    |
| Winsen            | 1        | 0.3%    |
| Winnipeg          | 1        | 0.3%    |
| Wildeshausen      | 1        | 0.3%    |
| Wiesbaden         | 1        | 0.3%    |
| West Malling      | 1        | 0.3%    |
| Waterloo          | 1        | 0.3%    |
| Walthamstow       | 1        | 0.3%    |
| Waldsassen        | 1        | 0.3%    |
| Wadern            | 1        | 0.3%    |
| Volgograd         | 1        | 0.3%    |
| Villeparisis      | 1        | 0.3%    |
| Villanova         | 1        | 0.3%    |
| Vancouver         | 1        | 0.3%    |
| Utrera            | 1        | 0.3%    |
| Tuscaloosa        | 1        | 0.3%    |
| Tulsa             | 1        | 0.3%    |
| Tucson            | 1        | 0.3%    |
| Trondheim         | 1        | 0.3%    |

Drives
------

Drive Vendor
------------

Hard drive vendors

![Drive Vendor](./images/pie_chart/drive_vendor.svg)


| Vendor                    | Desktops | Drives | Percent |
|---------------------------|----------|--------|---------|
| WDC                       | 111      | 162    | 18.38%  |
| Samsung Electronics       | 108      | 170    | 17.88%  |
| Seagate                   | 106      | 157    | 17.55%  |
| Kingston                  | 35       | 44     | 5.79%   |
| Crucial                   | 34       | 37     | 5.63%   |
| SanDisk                   | 28       | 33     | 4.64%   |
| Hitachi                   | 27       | 34     | 4.47%   |
| Toshiba                   | 25       | 32     | 4.14%   |
| A-DATA Technology         | 11       | 12     | 1.82%   |
| Phison                    | 10       | 12     | 1.66%   |
| Silicon Motion            | 9        | 12     | 1.49%   |
| Intel                     | 8        | 9      | 1.32%   |
| PNY                       | 7        | 9      | 1.16%   |
| Micron/Crucial Technology | 6        | 6      | 0.99%   |
| HGST                      | 6        | 7      | 0.99%   |
| Phison Electronics        | 5        | 6      | 0.83%   |
| Unknown                   | 4        | 6      | 0.66%   |
| SK Hynix                  | 3        | 8      | 0.5%    |
| Realtek Semiconductor     | 3        | 3      | 0.5%    |
| MAXTOR                    | 3        | 3      | 0.5%    |
| Intenso                   | 3        | 3      | 0.5%    |
| China                     | 3        | 3      | 0.5%    |
| SPCC                      | 2        | 2      | 0.33%   |
| SABRENT                   | 2        | 2      | 0.33%   |
| PLEXTOR                   | 2        | 2      | 0.33%   |
| Patriot                   | 2        | 2      | 0.33%   |
| OCZ                       | 2        | 2      | 0.33%   |
| Netac                     | 2        | 3      | 0.33%   |
| LITEONIT                  | 2        | 2      | 0.33%   |
| LITEON                    | 2        | 2      | 0.33%   |
| LDLC                      | 2        | 2      | 0.33%   |
| KIOXIA-EXCERIA            | 2        | 2      | 0.33%   |
| Hewlett-Packard           | 2        | 2      | 0.33%   |
| GOODRAM                   | 2        | 3      | 0.33%   |
| Corsair                   | 2        | 2      | 0.33%   |
| Unknown                   | 2        | 2      | 0.33%   |
| XPG                       | 1        | 1      | 0.17%   |
| Transcend                 | 1        | 1      | 0.17%   |
| SSSTC                     | 1        | 1      | 0.17%   |
| Q200                      | 1        | 2      | 0.17%   |
| OWC                       | 1        | 1      | 0.17%   |
| NTC                       | 1        | 1      | 0.17%   |
| Maxone                    | 1        | 1      | 0.17%   |
| MARVELL                   | 1        | 1      | 0.17%   |
| KLEVV                     | 1        | 2      | 0.17%   |
| KingDian                  | 1        | 1      | 0.17%   |
| kimtigo                   | 1        | 1      | 0.17%   |
| JMicron                   | 1        | 1      | 0.17%   |
| INTEL SS                  | 1        | 1      | 0.17%   |
| Green House               | 1        | 1      | 0.17%   |
| Gigabyte Technology       | 1        | 1      | 0.17%   |
| ExcelStor                 | 1        | 1      | 0.17%   |
| BAITITON                  | 1        | 1      | 0.17%   |
| ASMT                      | 1        | 1      | 0.17%   |
| Apacer                    | 1        | 1      | 0.17%   |
| 980Plus                   | 1        | 1      | 0.17%   |
| 2-Power                   | 1        | 2      | 0.17%   |

Drive Model
-----------

Hard drive models

![Drive Model](./images/pie_chart/drive_model.svg)


| Model                               | Desktops | Percent |
|-------------------------------------|----------|---------|
| Seagate ST500DM002-1BD142 500GB     | 11       | 1.53%   |
| Kingston SA400S37240G 240GB SSD     | 11       | 1.53%   |
| Seagate ST2000DM008-2FR102 2TB      | 10       | 1.39%   |
| Samsung SSD 850 EVO 250GB           | 10       | 1.39%   |
| Samsung SSD 970 EVO 500GB           | 8        | 1.11%   |
| Samsung SSD 850 EVO 500GB           | 8        | 1.11%   |
| Samsung NVMe SSD Drive 250GB        | 8        | 1.11%   |
| Samsung SSD 860 EVO 500GB           | 7        | 0.97%   |
| Samsung SSD 860 EVO 1TB             | 7        | 0.97%   |
| Seagate ST1000DM003-1CH162 1TB      | 6        | 0.84%   |
| Samsung NVMe SSD Drive 500GB        | 6        | 0.84%   |
| Samsung NVMe SSD Drive 1TB          | 6        | 0.84%   |
| Kingston SA400S37120G 120GB SSD     | 6        | 0.84%   |
| Crucial CT500MX500SSD1 500GB        | 6        | 0.84%   |
| Seagate ST3250318AS 250GB           | 5        | 0.7%    |
| Sandisk NVMe SSD Drive 500GB        | 5        | 0.7%    |
| Phison PCIe SSD 256GB               | 5        | 0.7%    |
| Kingston SV300S37A120G 120GB SSD    | 5        | 0.7%    |
| Kingston SA400S37480G 480GB SSD     | 5        | 0.7%    |
| Hitachi HUS724030ALE641 3TB         | 5        | 0.7%    |
| WDC WD10EZEX-08WN4A0 1TB            | 4        | 0.56%   |
| Silicon Motion NVMe SSD Drive 256GB | 4        | 0.56%   |
| Seagate ST1000DM003-1ER162 1TB      | 4        | 0.56%   |
| Sandisk NVMe SSD Drive 1TB          | 4        | 0.56%   |
| Samsung HD501LJ 500GB               | 4        | 0.56%   |
| Crucial CT240BX500SSD1 240GB        | 4        | 0.56%   |
| WDC WD20EZRX-00D8PB0 2TB            | 3        | 0.42%   |
| WDC WD10EZEX-00BN5A0 1TB            | 3        | 0.42%   |
| Toshiba DT01ACA050 500GB            | 3        | 0.42%   |
| Silicon Motion NVMe SSD Drive 512GB | 3        | 0.42%   |
| Seagate ST4000DM004-2CV104 4TB      | 3        | 0.42%   |
| Seagate ST3160812AS 160GB           | 3        | 0.42%   |
| Seagate ST2000DM001-1ER164 2TB      | 3        | 0.42%   |
| Seagate ST2000DL003-9VT166 2TB      | 3        | 0.42%   |
| Seagate Expansion 1TB               | 3        | 0.42%   |
| SanDisk Ultra II 480GB SSD          | 3        | 0.42%   |
| SanDisk SSD PLUS 240GB              | 3        | 0.42%   |
| Samsung SSD 980 PRO 1TB             | 3        | 0.42%   |
| Samsung SSD 970 EVO Plus 1TB        | 3        | 0.42%   |
| Samsung SSD 870 QVO 1TB             | 3        | 0.42%   |
| Samsung SSD 870 EVO 250GB           | 3        | 0.42%   |
| Samsung SSD 870 EVO 1TB             | 3        | 0.42%   |
| Samsung SSD 860 EVO 250GB           | 3        | 0.42%   |
| Samsung NVMe SSD Drive 512GB        | 3        | 0.42%   |
| Phison NVMe SSD Drive 256GB         | 3        | 0.42%   |
| Micron/Crucial NVMe SSD Drive 500GB | 3        | 0.42%   |
| Crucial CT120BX500SSD1 120GB        | 3        | 0.42%   |
| WDC WDS120G2G0A-00JH30 120GB SSD    | 2        | 0.28%   |
| WDC WDBNCE0010PNC 1TB SSD           | 2        | 0.28%   |
| WDC WD80EZAZ-11TDBA0 8TB            | 2        | 0.28%   |
| WDC WD6400AACS-00G8B1 640GB         | 2        | 0.28%   |
| WDC WD5000AAKX-001CA0 500GB         | 2        | 0.28%   |
| WDC WD5000AAKS-00UU3A0 500GB        | 2        | 0.28%   |
| WDC WD40EZRZ-00GXCB0 4TB            | 2        | 0.28%   |
| WDC WD40EFRX-68N32N0 4TB            | 2        | 0.28%   |
| WDC WD4003FZEX-00Z4SA0 4TB          | 2        | 0.28%   |
| WDC WD30EZRZ-00Z5HB0 3TB            | 2        | 0.28%   |
| WDC WD2500AAKX-753CA1 250GB         | 2        | 0.28%   |
| WDC WD20EZRZ-00Z5HB0 2TB            | 2        | 0.28%   |
| WDC WD20EZRX-00DC0B0 2TB            | 2        | 0.28%   |

HDD Vendor
----------

Hard disk drive vendors

![HDD Vendor](./images/pie_chart/drive_hdd_vendor.svg)


| Vendor              | Desktops | Drives | Percent |
|---------------------|----------|--------|---------|
| Seagate             | 106      | 155    | 36.3%   |
| WDC                 | 100      | 148    | 34.25%  |
| Hitachi             | 27       | 34     | 9.25%   |
| Toshiba             | 21       | 28     | 7.19%   |
| Samsung Electronics | 21       | 26     | 7.19%   |
| HGST                | 6        | 7      | 2.05%   |
| MAXTOR              | 3        | 3      | 1.03%   |
| Unknown             | 2        | 2      | 0.68%   |
| SABRENT             | 2        | 2      | 0.68%   |
| Maxone              | 1        | 1      | 0.34%   |
| MARVELL             | 1        | 1      | 0.34%   |
| Hewlett-Packard     | 1        | 1      | 0.34%   |
| Unknown             | 1        | 1      | 0.34%   |

SSD Vendor
----------

Solid state drive vendors

![SSD Vendor](./images/pie_chart/drive_ssd_vendor.svg)


| Vendor              | Desktops | Drives | Percent |
|---------------------|----------|--------|---------|
| Samsung Electronics | 57       | 81     | 27.27%  |
| Kingston            | 32       | 41     | 15.31%  |
| Crucial             | 29       | 31     | 13.88%  |
| SanDisk             | 18       | 22     | 8.61%   |
| WDC                 | 12       | 12     | 5.74%   |
| A-DATA Technology   | 9        | 10     | 4.31%   |
| PNY                 | 6        | 7      | 2.87%   |
| Intel               | 6        | 7      | 2.87%   |
| Toshiba             | 3        | 3      | 1.44%   |
| China               | 3        | 3      | 1.44%   |
| PLEXTOR             | 2        | 2      | 0.96%   |
| Patriot             | 2        | 2      | 0.96%   |
| OCZ                 | 2        | 2      | 0.96%   |
| Netac               | 2        | 3      | 0.96%   |
| LITEONIT            | 2        | 2      | 0.96%   |
| LITEON              | 2        | 2      | 0.96%   |
| LDLC                | 2        | 2      | 0.96%   |
| KIOXIA-EXCERIA      | 2        | 2      | 0.96%   |
| Intenso             | 2        | 2      | 0.96%   |
| Transcend           | 1        | 1      | 0.48%   |
| SPCC                | 1        | 1      | 0.48%   |
| SK Hynix            | 1        | 2      | 0.48%   |
| Q200                | 1        | 2      | 0.48%   |
| KLEVV               | 1        | 2      | 0.48%   |
| KingDian            | 1        | 1      | 0.48%   |
| kimtigo             | 1        | 1      | 0.48%   |
| INTEL SS            | 1        | 1      | 0.48%   |
| Hewlett-Packard     | 1        | 1      | 0.48%   |
| Green House         | 1        | 1      | 0.48%   |
| GOODRAM             | 1        | 1      | 0.48%   |
| Corsair             | 1        | 1      | 0.48%   |
| BAITITON            | 1        | 1      | 0.48%   |
| ASMT                | 1        | 1      | 0.48%   |
| Apacer              | 1        | 1      | 0.48%   |
| 2-Power             | 1        | 2      | 0.48%   |

Drive Kind
----------

HDD or SSD

![Drive Kind](./images/pie_chart/drive_kind.svg)


| Kind    | Desktops | Drives | Percent |
|---------|----------|--------|---------|
| HDD     | 223      | 409    | 43.81%  |
| SSD     | 170      | 256    | 33.4%   |
| NVMe    | 105      | 144    | 20.63%  |
| Unknown | 10       | 12     | 1.96%   |
| MMC     | 1        | 1      | 0.2%    |

Drive Connector
---------------

SATA, SAS, NVMe, etc.

![Drive Connector](./images/pie_chart/drive_bus.svg)


| Type | Desktops | Drives | Percent |
|------|----------|--------|---------|
| SATA | 297      | 649    | 69.72%  |
| NVMe | 105      | 144    | 24.65%  |
| SAS  | 23       | 28     | 5.4%    |
| MMC  | 1        | 1      | 0.23%   |

Drive Size
----------

Size of hard drive

![Drive Size](./images/pie_chart/drive_size.svg)


| Size in TB | Desktops | Drives | Percent |
|------------|----------|--------|---------|
| 0.01-0.5   | 219      | 363    | 50.69%  |
| 0.51-1.0   | 116      | 159    | 26.85%  |
| 1.01-2.0   | 52       | 67     | 12.04%  |
| 3.01-4.0   | 18       | 34     | 4.17%   |
| 2.01-3.0   | 16       | 23     | 3.7%    |
| 4.01-10.0  | 7        | 15     | 1.62%   |
| 10.01-20.0 | 3        | 3      | 0.69%   |
| 0          | 1        | 1      | 0.23%   |

Space Total
-----------

Amount of disk space available on the file system

![Space Total](./images/pie_chart/drive_space_total.svg)


| Size in GB     | Desktops | Percent |
|----------------|----------|---------|
| 101-250        | 89       | 26.73%  |
| 501-1000       | 60       | 18.02%  |
| 251-500        | 54       | 16.22%  |
| 1001-2000      | 37       | 11.11%  |
| 1-20           | 26       | 7.81%   |
| More than 3000 | 23       | 6.91%   |
| 2001-3000      | 20       | 6.01%   |
| 51-100         | 15       | 4.5%    |
| 21-50          | 8        | 2.4%    |
| Unknown        | 1        | 0.3%    |

Space Used
----------

Amount of used disk space

![Space Used](./images/pie_chart/drive_space_used.svg)


| Used GB        | Desktops | Percent |
|----------------|----------|---------|
| 1-20           | 126      | 37.28%  |
| 21-50          | 53       | 15.68%  |
| 101-250        | 46       | 13.61%  |
| 51-100         | 35       | 10.36%  |
| 501-1000       | 24       | 7.1%    |
| 251-500        | 22       | 6.51%   |
| 1001-2000      | 16       | 4.73%   |
| More than 3000 | 11       | 3.25%   |
| 2001-3000      | 4        | 1.18%   |
| Unknown        | 1        | 0.3%    |

Malfunc. Drives
---------------

Drive models with a malfunction

![Malfunc. Drives](./images/pie_chart/drive_malfunc.svg)


| Model                                 | Desktops | Drives | Percent |
|---------------------------------------|----------|--------|---------|
| Seagate ST1000DM003-1CH162 1TB        | 2        | 2      | 11.11%  |
| WDC WD5000AAKX-083CA1 500GB           | 1        | 1      | 5.56%   |
| WDC WD30EZRX-00DC0B0 3TB              | 1        | 1      | 5.56%   |
| WDC WD2500HHTZ-04N21V0 250GB          | 1        | 1      | 5.56%   |
| WDC WD1600AAJS-00L7A0 160GB           | 1        | 1      | 5.56%   |
| WDC WD10EZEX-00BN5A0 1TB              | 1        | 1      | 5.56%   |
| WDC WD1002FAEX-00Z3A0 1TB             | 1        | 1      | 5.56%   |
| Toshiba MQ01ABD100 1TB                | 1        | 2      | 5.56%   |
| Seagate ST500DM002-1BD142 500GB       | 1        | 1      | 5.56%   |
| Seagate ST3500320AS 500GB             | 1        | 1      | 5.56%   |
| Seagate ST31000528AS 1TB              | 1        | 1      | 5.56%   |
| Seagate ST1000LM024 HN-M101MBB 1TB    | 1        | 1      | 5.56%   |
| Samsung Electronics SSD 870 EVO 500GB | 1        | 1      | 5.56%   |
| Samsung Electronics SSD 870 EVO 1TB   | 1        | 1      | 5.56%   |
| MAXTOR 6G160E0 160GB                  | 1        | 1      | 5.56%   |
| Kingston SV300S37A120G 120GB SSD      | 1        | 1      | 5.56%   |
| Hitachi HDS721680PLA380 80GB          | 1        | 1      | 5.56%   |

Malfunc. Drive Vendor
---------------------

Vendors of faulty drives

![Malfunc. Drive Vendor](./images/pie_chart/drive_malfunc_vendor.svg)


| Vendor              | Desktops | Drives | Percent |
|---------------------|----------|--------|---------|
| WDC                 | 6        | 6      | 35.29%  |
| Seagate             | 6        | 6      | 35.29%  |
| Toshiba             | 1        | 2      | 5.88%   |
| Samsung Electronics | 1        | 2      | 5.88%   |
| MAXTOR              | 1        | 1      | 5.88%   |
| Kingston            | 1        | 1      | 5.88%   |
| Hitachi             | 1        | 1      | 5.88%   |

Malfunc. HDD Vendor
-------------------

Vendors of faulty HDD drives

![Malfunc. HDD Vendor](./images/pie_chart/drive_malfunc_hdd_vendor.svg)


| Vendor  | Desktops | Drives | Percent |
|---------|----------|--------|---------|
| WDC     | 6        | 6      | 40%     |
| Seagate | 6        | 6      | 40%     |
| Toshiba | 1        | 2      | 6.67%   |
| MAXTOR  | 1        | 1      | 6.67%   |
| Hitachi | 1        | 1      | 6.67%   |

Malfunc. Drive Kind
-------------------

Kinds of faulty drives

![Malfunc. Drive Kind](./images/pie_chart/drive_malfunc_kind.svg)


| Kind | Desktops | Drives | Percent |
|------|----------|--------|---------|
| HDD  | 13       | 16     | 86.67%  |
| SSD  | 2        | 3      | 13.33%  |

Failed Drives
-------------

Failed drive models

Zero info for selected period =(

Failed Drive Vendor
-------------------

Failed drive vendors

Zero info for selected period =(

Drive Status
------------

Number of failed and malfunc. drives

![Drive Status](./images/pie_chart/drive_status.svg)


| Status   | Desktops | Drives | Percent |
|----------|----------|--------|---------|
| Detected | 255      | 593    | 73.49%  |
| Works    | 77       | 210    | 22.19%  |
| Malfunc  | 15       | 19     | 4.32%   |

Storage controller
------------------

Storage Vendor
--------------

Storage controller vendors

![Storage Vendor](./images/pie_chart/storage_vendor.svg)


| Vendor                       | Desktops | Percent |
|------------------------------|----------|---------|
| Intel                        | 213      | 43.03%  |
| AMD                          | 107      | 21.62%  |
| Samsung Electronics          | 50       | 10.1%   |
| Phison Electronics           | 18       | 3.64%   |
| ASMedia Technology           | 15       | 3.03%   |
| JMicron Technology           | 13       | 2.63%   |
| Marvell Technology Group     | 12       | 2.42%   |
| Silicon Motion               | 11       | 2.22%   |
| Sandisk                      | 11       | 2.22%   |
| Micron/Crucial Technology    | 10       | 2.02%   |
| Nvidia                       | 9        | 1.82%   |
| SK Hynix                     | 3        | 0.61%   |
| Realtek Semiconductor        | 3        | 0.61%   |
| LSI Logic / Symbios Logic    | 3        | 0.61%   |
| Kingston Technology Company  | 3        | 0.61%   |
| ADATA Technology             | 3        | 0.61%   |
| Broadcom / LSI               | 2        | 0.4%    |
| VIA Technologies             | 1        | 0.2%    |
| Toshiba America Info Systems | 1        | 0.2%    |
| Silicon Image                | 1        | 0.2%    |
| Shenzhen Longsys Electronics | 1        | 0.2%    |
| Micron Technology            | 1        | 0.2%    |
| Lite-On IT Corp. / Plextor   | 1        | 0.2%    |
| Hewlett-Packard              | 1        | 0.2%    |
| Advanced System Products     | 1        | 0.2%    |
| 3ware                        | 1        | 0.2%    |

Storage Model
-------------

Storage controller models

![Storage Model](./images/pie_chart/storage_model.svg)


| Model                                                                                   | Desktops | Percent |
|-----------------------------------------------------------------------------------------|----------|---------|
| AMD FCH SATA Controller [AHCI mode]                                                     | 63       | 10.23%  |
| Samsung NVMe SSD Controller SM981/PM981/PM983                                           | 31       | 5.03%   |
| Intel 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode]          | 24       | 3.9%    |
| AMD 400 Series Chipset SATA Controller                                                  | 19       | 3.08%   |
| Intel SATA Controller [RAID mode]                                                       | 18       | 2.92%   |
| Intel 6 Series/C200 Series Chipset Family 6 port Desktop SATA AHCI Controller           | 18       | 2.92%   |
| AMD SB7x0/SB8x0/SB9x0 IDE Controller                                                    | 18       | 2.92%   |
| Intel NM10/ICH7 Family SATA Controller [IDE mode]                                       | 16       | 2.6%    |
| Intel 7 Series/C210 Series Chipset Family 6-port SATA Controller [AHCI mode]            | 16       | 2.6%    |
| AMD SB7x0/SB8x0/SB9x0 SATA Controller [AHCI mode]                                       | 15       | 2.44%   |
| Intel 200 Series PCH SATA controller [AHCI mode]                                        | 14       | 2.27%   |
| Intel Q170/Q150/B150/H170/H110/Z170/CM236 Chipset SATA Controller [AHCI Mode]           | 13       | 2.11%   |
| Intel Cannon Lake PCH SATA AHCI Controller                                              | 13       | 2.11%   |
| Intel 82801G (ICH7 Family) IDE Controller                                               | 13       | 2.11%   |
| ASMedia ASM1062 Serial ATA Controller                                                   | 13       | 2.11%   |
| Samsung NVMe SSD Controller PM9A1/PM9A3/980PRO                                          | 11       | 1.79%   |
| AMD SB7x0/SB8x0/SB9x0 SATA Controller [IDE mode]                                        | 11       | 1.79%   |
| Silicon Motion SM2263EN/SM2263XT SSD Controller                                         | 10       | 1.62%   |
| Phison E12 NVMe Controller                                                              | 10       | 1.62%   |
| AMD Starship/Matisse Chipset SATA Controller [AHCI mode]                                | 10       | 1.62%   |
| Intel 600 Series Chipset Family SATA AHCI Controller                                    | 9        | 1.46%   |
| Intel 500 Series Chipset Family SATA AHCI Controller                                    | 9        | 1.46%   |
| Samsung NVMe SSD Controller SM961/PM961/SM963                                           | 7        | 1.14%   |
| AMD FCH SATA Controller D                                                               | 7        | 1.14%   |
| Micron/Crucial P2 NVMe PCIe SSD                                                         | 6        | 0.97%   |
| JMicron JMB363 SATA/IDE Controller                                                      | 6        | 0.97%   |
| Intel C600/X79 series chipset SATA RAID Controller                                      | 6        | 0.97%   |
| Intel 82801JI (ICH10 Family) 4 port SATA IDE Controller #1                              | 6        | 0.97%   |
| Intel 6 Series/C200 Series Chipset Family Desktop SATA Controller (IDE mode, ports 4-5) | 6        | 0.97%   |
| Intel 6 Series/C200 Series Chipset Family Desktop SATA Controller (IDE mode, ports 0-3) | 6        | 0.97%   |
| Phison E16 PCIe4 NVMe Controller                                                        | 5        | 0.81%   |
| Intel Comet Lake SATA AHCI Controller                                                   | 5        | 0.81%   |
| Intel 82801JI (ICH10 Family) 2 port SATA IDE Controller #2                              | 5        | 0.81%   |
| Intel 82801JD/DO (ICH10 Family) SATA AHCI Controller                                    | 5        | 0.81%   |
| Intel 4 Series Chipset PT IDER Controller                                               | 5        | 0.81%   |
| Sandisk WD Black SN750 / PC SN730 NVMe SSD                                              | 4        | 0.65%   |
| Nvidia MCP61 SATA Controller                                                            | 4        | 0.65%   |
| JMicron JMB368 IDE controller                                                           | 4        | 0.65%   |
| Intel 9 Series Chipset Family SATA Controller [AHCI Mode]                               | 4        | 0.65%   |
| AMD X370 Series Chipset SATA Controller                                                 | 4        | 0.65%   |
| AMD 300 Series Chipset SATA Controller                                                  | 4        | 0.65%   |
| Sandisk WD PC SN810 / Black SN850 NVMe SSD                                              | 3        | 0.49%   |
| Sandisk WD Blue SN550 NVMe SSD                                                          | 3        | 0.49%   |
| Samsung NVMe SSD Controller 980                                                         | 3        | 0.49%   |
| Realtek RTS5763DL NVMe SSD Controller                                                   | 3        | 0.49%   |
| Marvell Group 88SE9172 SATA 6Gb/s Controller                                            | 3        | 0.49%   |
| JMicron JMB361 AHCI/IDE                                                                 | 3        | 0.49%   |
| Intel Volume Management Device NVMe RAID Controller                                     | 3        | 0.49%   |
| Intel 82801IR/IO/IH (ICH9R/DO/DH) 6 port SATA Controller [AHCI mode]                    | 3        | 0.49%   |
| Intel 631xESB/632xESB IDE Controller                                                    | 3        | 0.49%   |
| AMD X399 Series Chipset SATA Controller                                                 | 3        | 0.49%   |
| ADATA XPG SX8200 Pro PCIe Gen3x4 M.2 2280 Solid State Drive                             | 3        | 0.49%   |
| Sandisk Non-Volatile memory controller                                                  | 2        | 0.32%   |
| Phison E18 PCIe4 NVMe Controller                                                        | 2        | 0.32%   |
| Nvidia MCP55 SATA Controller                                                            | 2        | 0.32%   |
| Nvidia MCP55 IDE                                                                        | 2        | 0.32%   |
| Nvidia MCP51 Serial ATA Controller                                                      | 2        | 0.32%   |
| Nvidia MCP51 IDE                                                                        | 2        | 0.32%   |
| Micron/Crucial P1 NVMe PCIe SSD                                                         | 2        | 0.32%   |
| Micron/Crucial Non-Volatile memory controller                                           | 2        | 0.32%   |

Storage Kind
------------

Kind of storage controller (IDE, SATA, NVMe, SAS, ...)

![Storage Kind](./images/pie_chart/storage_kind.svg)


| Kind | Desktops | Percent |
|------|----------|---------|
| SATA | 260      | 52.85%  |
| NVMe | 104      | 21.14%  |
| IDE  | 86       | 17.48%  |
| RAID | 34       | 6.91%   |
| SCSI | 5        | 1.02%   |
| SAS  | 3        | 0.61%   |

Processor
---------

CPU Vendor
----------

Processor vendors

![CPU Vendor](./images/pie_chart/cpu_vendor.svg)


| Vendor | Desktops | Percent |
|--------|----------|---------|
| Intel  | 216      | 65.06%  |
| AMD    | 116      | 34.94%  |

CPU Model
---------

Processor models

![CPU Model](./images/pie_chart/cpu_model.svg)


| Model                                          | Desktops | Percent |
|------------------------------------------------|----------|---------|
| Intel Core i7-3770 CPU @ 3.40GHz               | 8        | 2.41%   |
| Intel Core i7-8700K CPU @ 3.70GHz              | 6        | 1.81%   |
| Intel Core i3-3220 CPU @ 3.30GHz               | 6        | 1.81%   |
| AMD Ryzen 5 5600X 6-Core Processor             | 6        | 1.81%   |
| AMD Ryzen 5 3600 6-Core Processor              | 6        | 1.81%   |
| AMD FX-6300 Six-Core Processor                 | 6        | 1.81%   |
| Intel Core i7-7700K CPU @ 4.20GHz              | 5        | 1.51%   |
| Intel Core i5-2400 CPU @ 3.10GHz               | 5        | 1.51%   |
| Intel Core 2 Duo CPU E8400 @ 3.00GHz           | 5        | 1.51%   |
| AMD Ryzen 7 5800X 8-Core Processor             | 5        | 1.51%   |
| Intel Core i7-4790K CPU @ 4.00GHz              | 4        | 1.2%    |
| Intel Core i7-4770K CPU @ 3.50GHz              | 4        | 1.2%    |
| Intel Core i5-6500 CPU @ 3.20GHz               | 4        | 1.2%    |
| Intel Core i5-4570 CPU @ 3.20GHz               | 4        | 1.2%    |
| Intel 12th Gen Core i5-12600K                  | 4        | 1.2%    |
| AMD Ryzen 9 3900X 12-Core Processor            | 4        | 1.2%    |
| AMD Ryzen 7 3700X 8-Core Processor             | 4        | 1.2%    |
| AMD Ryzen 5 3400G with Radeon Vega Graphics    | 4        | 1.2%    |
| Intel Pentium Dual-Core CPU E5300 @ 2.60GHz    | 3        | 0.9%    |
| Intel Core i9-9900K CPU @ 3.60GHz              | 3        | 0.9%    |
| Intel Core i7-6700K CPU @ 4.00GHz              | 3        | 0.9%    |
| Intel Core i7-4790 CPU @ 3.60GHz               | 3        | 0.9%    |
| Intel Core i5-8400 CPU @ 2.80GHz               | 3        | 0.9%    |
| Intel Core i5-3570 CPU @ 3.40GHz               | 3        | 0.9%    |
| Intel Core i3-2120 CPU @ 3.30GHz               | 3        | 0.9%    |
| Intel Core 2 Quad CPU Q6600 @ 2.40GHz          | 3        | 0.9%    |
| Intel Core 2 Duo CPU E7500 @ 2.93GHz           | 3        | 0.9%    |
| AMD Ryzen 9 5950X 16-Core Processor            | 3        | 0.9%    |
| AMD Ryzen 7 5700G with Radeon Graphics         | 3        | 0.9%    |
| AMD Ryzen 7 2700X Eight-Core Processor         | 3        | 0.9%    |
| AMD Ryzen 5 5600G with Radeon Graphics         | 3        | 0.9%    |
| AMD Ryzen 3 3200G with Radeon Vega Graphics    | 3        | 0.9%    |
| AMD FX-8320 Eight-Core Processor               | 3        | 0.9%    |
| Intel Pentium Dual-Core CPU E5800 @ 3.20GHz    | 2        | 0.6%    |
| Intel Pentium Dual-Core CPU E5400 @ 2.70GHz    | 2        | 0.6%    |
| Intel Core i9-10900K CPU @ 3.70GHz             | 2        | 0.6%    |
| Intel Core i7-9700K CPU @ 3.60GHz              | 2        | 0.6%    |
| Intel Core i7-2600 CPU @ 3.40GHz               | 2        | 0.6%    |
| Intel Core i7 CPU 920 @ 2.67GHz                | 2        | 0.6%    |
| Intel Core i5-7400 CPU @ 3.00GHz               | 2        | 0.6%    |
| Intel Core i5-6600K CPU @ 3.50GHz              | 2        | 0.6%    |
| Intel Core i5-4670K CPU @ 3.40GHz              | 2        | 0.6%    |
| Intel Core i5-4590T CPU @ 2.00GHz              | 2        | 0.6%    |
| Intel Core i5-4590 CPU @ 3.30GHz               | 2        | 0.6%    |
| Intel Core i5-3570K CPU @ 3.40GHz              | 2        | 0.6%    |
| Intel Core i5-3470 CPU @ 3.20GHz               | 2        | 0.6%    |
| Intel Core i5-2500 CPU @ 3.30GHz               | 2        | 0.6%    |
| Intel Core i5-10600K CPU @ 4.10GHz             | 2        | 0.6%    |
| Intel Core i3-4130 CPU @ 3.40GHz               | 2        | 0.6%    |
| Intel Core i3-2100 CPU @ 3.10GHz               | 2        | 0.6%    |
| Intel Core i3-10100F CPU @ 3.60GHz             | 2        | 0.6%    |
| Intel Core 2 Quad CPU Q9550 @ 2.83GHz          | 2        | 0.6%    |
| Intel Core 2 Duo CPU E8500 @ 3.16GHz           | 2        | 0.6%    |
| Intel 12th Gen Core i9-12900K                  | 2        | 0.6%    |
| Intel 12th Gen Core i7-12700K                  | 2        | 0.6%    |
| Intel 11th Gen Core i5-11500 @ 2.70GHz         | 2        | 0.6%    |
| AMD Ryzen Threadripper 1920X 12-Core Processor | 2        | 0.6%    |
| AMD Ryzen 9 5900X 12-Core Processor            | 2        | 0.6%    |
| AMD Ryzen 7 1700X Eight-Core Processor         | 2        | 0.6%    |
| AMD Ryzen 7 1700 Eight-Core Processor          | 2        | 0.6%    |

CPU Model Family
----------------

Processor model prefix

![CPU Model Family](./images/pie_chart/cpu_family.svg)


| Model                   | Desktops | Percent |
|-------------------------|----------|---------|
| Intel Core i7           | 57       | 17.17%  |
| Intel Core i5           | 53       | 15.96%  |
| AMD Ryzen 5             | 27       | 8.13%   |
| Intel Core i3           | 23       | 6.93%   |
| AMD Ryzen 7             | 22       | 6.63%   |
| Intel Xeon              | 15       | 4.52%   |
| AMD FX                  | 15       | 4.52%   |
| Intel Core 2 Duo        | 14       | 4.22%   |
| Other                   | 13       | 3.92%   |
| AMD Ryzen 9             | 10       | 3.01%   |
| Intel Core 2 Quad       | 9        | 2.71%   |
| Intel Pentium Dual-Core | 8        | 2.41%   |
| Intel Core i9           | 7        | 2.11%   |
| AMD Ryzen 3             | 6        | 1.81%   |
| Intel Celeron           | 5        | 1.51%   |
| AMD Ryzen Threadripper  | 5        | 1.51%   |
| AMD A8                  | 5        | 1.51%   |
| AMD Phenom II X4        | 4        | 1.2%    |
| AMD Athlon II X2        | 4        | 1.2%    |
| AMD Athlon 64 X2        | 4        | 1.2%    |
| Intel Pentium Dual      | 3        | 0.9%    |
| Intel Pentium           | 3        | 0.9%    |
| AMD Phenom II X6        | 3        | 0.9%    |
| Intel Core 2            | 2        | 0.6%    |
| AMD A10                 | 2        | 0.6%    |
| Intel Pentium D         | 1        | 0.3%    |
| Intel Genuine           | 1        | 0.3%    |
| Intel Celeron D         | 1        | 0.3%    |
| Intel Atom              | 1        | 0.3%    |
| AMD Ryzen 7 PRO         | 1        | 0.3%    |
| AMD Ryzen 5 PRO         | 1        | 0.3%    |
| AMD Phenom II X2        | 1        | 0.3%    |
| AMD Phenom              | 1        | 0.3%    |
| AMD Athlon II X4        | 1        | 0.3%    |
| AMD Athlon Dual Core    | 1        | 0.3%    |
| AMD Athlon              | 1        | 0.3%    |
| AMD A6                  | 1        | 0.3%    |
| AMD A4                  | 1        | 0.3%    |

CPU Cores
---------

Number of processor cores

![CPU Cores](./images/pie_chart/cpu_cores.svg)


| Number | Desktops | Percent |
|--------|----------|---------|
| 4      | 121      | 36.45%  |
| 2      | 77       | 23.19%  |
| 6      | 51       | 15.36%  |
| 8      | 40       | 12.05%  |
| 12     | 13       | 3.92%   |
| 3      | 9        | 2.71%   |
| 16     | 8        | 2.41%   |
| 10     | 8        | 2.41%   |
| 1      | 2        | 0.6%    |
| 32     | 1        | 0.3%    |
| 28     | 1        | 0.3%    |
| 24     | 1        | 0.3%    |

CPU Sockets
-----------

Number of sockets

![CPU Sockets](./images/pie_chart/cpu_sockets.svg)


| Number | Desktops | Percent |
|--------|----------|---------|
| 1      | 324      | 97.59%  |
| 2      | 8        | 2.41%   |

CPU Threads
-----------

Threads per core (Hyper-Threading)

![CPU Threads](./images/pie_chart/cpu_threads.svg)


| Number | Desktops | Percent |
|--------|----------|---------|
| 2      | 191      | 57.53%  |
| 1      | 141      | 42.47%  |

CPU Op-Modes
------------

CPU Operation Modes (32-bit, 64-bit)

![CPU Op-Modes](./images/pie_chart/cpu_op_modes.svg)


| Op mode        | Desktops | Percent |
|----------------|----------|---------|
| 32-bit, 64-bit | 332      | 100%    |

CPU Microcode
-------------

Microcode number

![CPU Microcode](./images/pie_chart/cpu_microcode.svg)


| Number     | Desktops | Percent |
|------------|----------|---------|
| Unknown    | 233      | 69.35%  |
| 0x306c3    | 7        | 2.08%   |
| 0x306a9    | 7        | 2.08%   |
| 0x906ea    | 6        | 1.79%   |
| 0x90672    | 6        | 1.79%   |
| 0x206a7    | 6        | 1.79%   |
| 0x08701013 | 6        | 1.79%   |
| 0x06000852 | 5        | 1.49%   |
| 0x906ed    | 4        | 1.19%   |
| 0x906e9    | 4        | 1.19%   |
| 0x0a50000c | 4        | 1.19%   |
| 0x0a201016 | 4        | 1.19%   |
| 0x0a201009 | 4        | 1.19%   |
| 0xa0671    | 3        | 0.89%   |
| 0xa0655    | 3        | 0.89%   |
| 0x506e3    | 3        | 0.89%   |
| 0x0800820d | 3        | 0.89%   |
| 0x206d7    | 2        | 0.6%    |
| 0x106a5    | 2        | 0.6%    |
| 0x1067a    | 2        | 0.6%    |
| 0x08108109 | 2        | 0.6%    |
| 0x010000dc | 2        | 0.6%    |
| 0xf47      | 1        | 0.3%    |
| 0x906ec    | 1        | 0.3%    |
| 0x6fd      | 1        | 0.3%    |
| 0x6fb      | 1        | 0.3%    |
| 0x506e8    | 1        | 0.3%    |
| 0x50654    | 1        | 0.3%    |
| 0x406c3    | 1        | 0.3%    |
| 0x306f2    | 1        | 0.3%    |
| 0x306d4    | 1        | 0.3%    |
| 0x08701021 | 1        | 0.3%    |
| 0x08600103 | 1        | 0.3%    |
| 0x08001138 | 1        | 0.3%    |
| 0x08001137 | 1        | 0.3%    |
| 0x06006118 | 1        | 0.3%    |
| 0x0600063e | 1        | 0.3%    |
| 0x010000db | 1        | 0.3%    |
| 0x010000c8 | 1        | 0.3%    |
| 0x010000c7 | 1        | 0.3%    |

CPU Microarch
-------------

Microarchitecture

![CPU Microarch](./images/pie_chart/cpu_microarch.svg)


| Name             | Desktops | Percent |
|------------------|----------|---------|
| Haswell          | 35       | 10.54%  |
| KabyLake         | 34       | 10.24%  |
| Penryn           | 29       | 8.73%   |
| IvyBridge        | 29       | 8.73%   |
| Zen 3            | 23       | 6.93%   |
| SandyBridge      | 22       | 6.63%   |
| Zen 2            | 20       | 6.02%   |
| Piledriver       | 19       | 5.72%   |
| Zen+             | 16       | 4.82%   |
| Zen              | 14       | 4.22%   |
| K10              | 14       | 4.22%   |
| Skylake          | 13       | 3.92%   |
| CometLake        | 12       | 3.61%   |
| Core             | 11       | 3.31%   |
| Nehalem          | 7        | 2.11%   |
| Alderlake Hybrid | 6        | 1.81%   |
| K8 Hammer        | 5        | 1.51%   |
| Unknown          | 4        | 1.2%    |
| Westmere         | 3        | 0.9%    |
| Icelake          | 3        | 0.9%    |
| Broadwell        | 3        | 0.9%    |
| Silvermont       | 2        | 0.6%    |
| NetBurst         | 2        | 0.6%    |
| Excavator        | 2        | 0.6%    |
| Bulldozer        | 2        | 0.6%    |
| K10 Llano        | 1        | 0.3%    |
| Goldmont plus    | 1        | 0.3%    |

Graphics
--------

GPU Vendor
----------

Vendors of graphics cards

![GPU Vendor](./images/pie_chart/gpu_vendor.svg)


| Vendor | Desktops | Percent |
|--------|----------|---------|
| Nvidia | 139      | 40.64%  |
| AMD    | 104      | 30.41%  |
| Intel  | 99       | 28.95%  |

GPU Model
---------

Graphics card models

![GPU Model](./images/pie_chart/gpu_model.svg)


| Model                                                                                    | Desktops | Percent |
|------------------------------------------------------------------------------------------|----------|---------|
| Intel Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller              | 16       | 4.57%   |
| Nvidia GK208B [GeForce GT 710]                                                           | 15       | 4.29%   |
| Intel Xeon E3-1200 v2/3rd Gen Core processor Graphics Controller                         | 12       | 3.43%   |
| Intel CoffeeLake-S GT2 [UHD Graphics 630]                                                | 11       | 3.14%   |
| Intel 4 Series Chipset Integrated Graphics Controller                                    | 9        | 2.57%   |
| AMD Ellesmere [Radeon RX 470/480/570/570X/580/580X/590]                                  | 9        | 2.57%   |
| Intel AlderLake-S GT1                                                                    | 8        | 2.29%   |
| Intel 2nd Generation Core Processor Family Integrated Graphics Controller                | 8        | 2.29%   |
| AMD Cezanne                                                                              | 7        | 2%      |
| Intel HD Graphics 530                                                                    | 6        | 1.71%   |
| AMD Picasso/Raven 2 [Radeon Vega Series / Radeon Vega Mobile Series]                     | 6        | 1.71%   |
| AMD Navi 10 [Radeon RX 5600 OEM/5600 XT / 5700/5700 XT]                                  | 6        | 1.71%   |
| AMD Cedar [Radeon HD 5000/6000/7350/8350 Series]                                         | 6        | 1.71%   |
| Nvidia GT218 [GeForce 210]                                                               | 5        | 1.43%   |
| Nvidia GP106 [GeForce GTX 1060 6GB]                                                      | 5        | 1.43%   |
| Nvidia GK208B [GeForce GT 730]                                                           | 5        | 1.43%   |
| AMD Lexa PRO [Radeon 540/540X/550/550X / RX 540X/550/550X]                               | 5        | 1.43%   |
| Nvidia TU104 [GeForce RTX 2070 SUPER]                                                    | 4        | 1.14%   |
| Nvidia GP108 [GeForce GT 1030]                                                           | 4        | 1.14%   |
| Nvidia GP107 [GeForce GTX 1050 Ti]                                                       | 4        | 1.14%   |
| Nvidia GP104 [GeForce GTX 1070]                                                          | 4        | 1.14%   |
| Intel IvyBridge GT2 [HD Graphics 4000]                                                   | 4        | 1.14%   |
| Intel HD Graphics 630                                                                    | 4        | 1.14%   |
| Intel CometLake-S GT2 [UHD Graphics 630]                                                 | 4        | 1.14%   |
| Nvidia GT218 [GeForce 8400 GS Rev. 3]                                                    | 3        | 0.86%   |
| Nvidia GP107 [GeForce GTX 1050]                                                          | 3        | 0.86%   |
| Nvidia GP106 [GeForce GTX 1060 3GB]                                                      | 3        | 0.86%   |
| Nvidia GP104 [GeForce GTX 1080]                                                          | 3        | 0.86%   |
| Nvidia GM206 [GeForce GTX 960]                                                           | 3        | 0.86%   |
| Nvidia GF108 [GeForce GT 730]                                                            | 3        | 0.86%   |
| Intel RocketLake-S GT1 [UHD Graphics 750]                                                | 3        | 0.86%   |
| Intel 4th Generation Core Processor Family Integrated Graphics Controller                | 3        | 0.86%   |
| AMD RS780L [Radeon 3000]                                                                 | 3        | 0.86%   |
| AMD Richland [Radeon HD 8570D]                                                           | 3        | 0.86%   |
| AMD Raven Ridge [Radeon Vega Series / Radeon Vega Mobile Series]                         | 3        | 0.86%   |
| AMD Navi 22 [Radeon RX 6700/6700 XT / 6800M]                                             | 3        | 0.86%   |
| AMD Navi 21 [Radeon RX 6800/6800 XT / 6900 XT]                                           | 3        | 0.86%   |
| AMD Curacao XT / Trinidad XT [Radeon R7 370 / R9 270X/370X]                              | 3        | 0.86%   |
| AMD Barts XT [Radeon HD 6870]                                                            | 3        | 0.86%   |
| Nvidia TU116 [GeForce GTX 1660 SUPER]                                                    | 2        | 0.57%   |
| Nvidia GP100GL [Quadro GP100]                                                            | 2        | 0.57%   |
| Nvidia GM204 [GeForce GTX 970]                                                           | 2        | 0.57%   |
| Nvidia GM107 [GeForce GTX 750]                                                           | 2        | 0.57%   |
| Nvidia GK110 [GeForce GTX 780]                                                           | 2        | 0.57%   |
| Nvidia GK107 [GeForce GT 640]                                                            | 2        | 0.57%   |
| Nvidia GK106 [GeForce GTX 660]                                                           | 2        | 0.57%   |
| Nvidia GK104 [GeForce GTX 770]                                                           | 2        | 0.57%   |
| Nvidia GF119 [GeForce GT 610]                                                            | 2        | 0.57%   |
| Nvidia GF119 [GeForce GT 520]                                                            | 2        | 0.57%   |
| Nvidia GF106 [GeForce GT 440]                                                            | 2        | 0.57%   |
| Nvidia GA102 [GeForce RTX 3080 Ti]                                                       | 2        | 0.57%   |
| Nvidia G98 [GeForce 8400 GS Rev. 2]                                                      | 2        | 0.57%   |
| Intel Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Integrated Graphics Controller | 2        | 0.57%   |
| Intel 82G33/G31 Express Integrated Graphics Controller                                   | 2        | 0.57%   |
| AMD Turks XT [Radeon HD 6670/7670]                                                       | 2        | 0.57%   |
| AMD RV730 PRO [Radeon HD 4650]                                                           | 2        | 0.57%   |
| AMD Navi 14 [Radeon RX 5500/5500M / Pro 5500M]                                           | 2        | 0.57%   |
| AMD Juniper XT [Radeon HD 5770]                                                          | 2        | 0.57%   |
| AMD Hawaii PRO [Radeon R9 290/390]                                                       | 2        | 0.57%   |
| AMD Caicos [Radeon HD 6450/7450/8450 / R5 230 OEM]                                       | 2        | 0.57%   |

GPU Combo
---------

Combinations of graphics cards

![GPU Combo](./images/pie_chart/gpu_combo.svg)


| Name           | Desktops | Percent |
|----------------|----------|---------|
| 1 x Nvidia     | 130      | 39.16%  |
| 1 x AMD        | 96       | 28.92%  |
| 1 x Intel      | 91       | 27.41%  |
| 2 x AMD        | 4        | 1.2%    |
| Intel + Nvidia | 4        | 1.2%    |
| 2 x Nvidia     | 3        | 0.9%    |
| Intel + AMD    | 3        | 0.9%    |
| AMD + Nvidia   | 1        | 0.3%    |

GPU Driver
----------

Free vs proprietary

![GPU Driver](./images/pie_chart/gpu_driver.svg)


| Driver      | Desktops | Percent |
|-------------|----------|---------|
| Free        | 243      | 72.75%  |
| Proprietary | 79       | 23.65%  |
| Unknown     | 12       | 3.59%   |

GPU Memory
----------

Total video memory

![GPU Memory](./images/pie_chart/gpu_memory.svg)


| Size in GB | Desktops | Percent |
|------------|----------|---------|
| Unknown    | 235      | 70.57%  |
| 1.01-2.0   | 28       | 8.41%   |
| 7.01-8.0   | 17       | 5.11%   |
| 0.51-1.0   | 15       | 4.5%    |
| 3.01-4.0   | 11       | 3.3%    |
| 0.01-0.5   | 9        | 2.7%    |
| 8.01-16.0  | 7        | 2.1%    |
| 5.01-6.0   | 6        | 1.8%    |
| 2.01-3.0   | 5        | 1.5%    |

Monitor
-------

Monitor Vendor
--------------

Monitor vendors

![Monitor Vendor](./images/pie_chart/mon_vendor.svg)


| Vendor               | Desktops | Percent |
|----------------------|----------|---------|
| Samsung Electronics  | 54       | 15.47%  |
| Dell                 | 42       | 12.03%  |
| Goldstar             | 35       | 10.03%  |
| Hewlett-Packard      | 23       | 6.59%   |
| Acer                 | 19       | 5.44%   |
| Philips              | 18       | 5.16%   |
| AOC                  | 17       | 4.87%   |
| BenQ                 | 15       | 4.3%    |
| Ancor Communications | 15       | 4.3%    |
| ASUSTek Computer     | 10       | 2.87%   |
| Lenovo               | 9        | 2.58%   |
| ViewSonic            | 8        | 2.29%   |
| Iiyama               | 8        | 2.29%   |
| Sony                 | 6        | 1.72%   |
| Unknown              | 5        | 1.43%   |
| NEC Computers        | 5        | 1.43%   |
| Medion               | 5        | 1.43%   |
| Vizio                | 3        | 0.86%   |
| MSI                  | 3        | 0.86%   |
| Hyundai ImageQuest   | 3        | 0.86%   |
| Fujitsu Siemens      | 3        | 0.86%   |
| Compaq Computer      | 3        | 0.86%   |
| Toshiba              | 2        | 0.57%   |
| SKY                  | 2        | 0.57%   |
| Sceptre Tech         | 2        | 0.57%   |
| Packard Bell         | 2        | 0.57%   |
| HannStar             | 2        | 0.57%   |
| ___                  | 1        | 0.29%   |
| Westinghouse         | 1        | 0.29%   |
| VIZ                  | 1        | 0.29%   |
| VIE                  | 1        | 0.29%   |
| Sun                  | 1        | 0.29%   |
| Sanyo                | 1        | 0.29%   |
| SAC                  | 1        | 0.29%   |
| PZG                  | 1        | 0.29%   |
| Panasonic            | 1        | 0.29%   |
| NVISION              | 1        | 0.29%   |
| NUL                  | 1        | 0.29%   |
| MStar                | 1        | 0.29%   |
| LYC                  | 1        | 0.29%   |
| LG Electronics       | 1        | 0.29%   |
| LED                  | 1        | 0.29%   |
| IPS                  | 1        | 0.29%   |
| HJW                  | 1        | 0.29%   |
| Hitachi              | 1        | 0.29%   |
| HannStar Display     | 1        | 0.29%   |
| Grundig              | 1        | 0.29%   |
| Gigabyte Technology  | 1        | 0.29%   |
| FUS                  | 1        | 0.29%   |
| Fujitsu              | 1        | 0.29%   |
| Eizo                 | 1        | 0.29%   |
| DENON                | 1        | 0.29%   |
| CVT                  | 1        | 0.29%   |
| CHD                  | 1        | 0.29%   |
| Belinea              | 1        | 0.29%   |
| AUS                  | 1        | 0.29%   |
| Unknown              | 1        | 0.29%   |

Monitor Model
-------------

Monitor models

![Monitor Model](./images/pie_chart/mon_model.svg)


| Model                                                                 | Desktops | Percent |
|-----------------------------------------------------------------------|----------|---------|
| ASUSTek Computer VP278 AUS27AE 1920x1080 598x336mm 27.0-inch          | 4        | 1.08%   |
| Samsung Electronics C27F390 SAM0D32 1920x1080 600x340mm 27.2-inch     | 3        | 0.81%   |
| Samsung Electronics C24F390 SAM0D2C 1920x1080 520x290mm 23.4-inch     | 3        | 0.81%   |
| Philips PHL 223V5 PHLC0CF 1920x1080 480x270mm 21.7-inch               | 3        | 0.81%   |
| Goldstar 2D FHD LG TV GSM59C6 1920x1080 509x286mm 23.0-inch           | 3        | 0.81%   |
| ViewSonic VA703-3Series VSC631E 1280x1024 338x270mm 17.0-inch         | 2        | 0.54%   |
| Philips PHL 274E5 PHLC0C8 1920x1080 600x340mm 27.2-inch               | 2        | 0.54%   |
| Goldstar Ultra HD GSM5B09 3840x2160 600x340mm 27.2-inch               | 2        | 0.54%   |
| Goldstar HDR WFHD GSM7714 2560x1080 798x334mm 34.1-inch               | 2        | 0.54%   |
| Goldstar FULL HD GSM5B55 1920x1080 480x270mm 21.7-inch                | 2        | 0.54%   |
| Dell U2412M DELA07A 1920x1200 518x324mm 24.1-inch                     | 2        | 0.54%   |
| Dell 1704FPV DEL3015 1280x1024 338x270mm 17.0-inch                    | 2        | 0.54%   |
| Compaq Computer Compaq Q1859 CPQ2826 1366x768 410x230mm 18.5-inch     | 2        | 0.54%   |
| BenQ GL2580 BNQ78E5 1920x1080 544x303mm 24.5-inch                     | 2        | 0.54%   |
| ASUSTek Computer VZ239 AUS23CC 1920x1080 509x286mm 23.0-inch          | 2        | 0.54%   |
| ASUSTek Computer VP28U AUS28B1 3840x2160 621x341mm 27.9-inch          | 2        | 0.54%   |
| AOC 27E1 AOC2701 1920x1080 598x336mm 27.0-inch                        | 2        | 0.54%   |
| Ancor Communications ASUS VS247 ACI249A 1920x1080 521x293mm 23.5-inch | 2        | 0.54%   |
| Ancor Communications ASUS VP228 ACI22C3 1920x1080 480x270mm 21.7-inch | 2        | 0.54%   |
| Acer K272HL ACR0523 1920x1080 600x340mm 27.2-inch                     | 2        | 0.54%   |
| ___ LCDTV16 ___0101 1600x1200 1600x900mm 72.3-inch                    | 1        | 0.27%   |
| Westinghouse LTV-19w6 WDE1906 1440x900 408x255mm 18.9-inch            | 1        | 0.27%   |
| Vizio D55-D2 VIZ1004 1920x1080 477x268mm 21.5-inch                    | 1        | 0.27%   |
| Vizio D32h-D1 VIZ1002 1360x768 697x392mm 31.5-inch                    | 1        | 0.27%   |
| Vizio D32f-F1 VIZ1027 1920x1080 698x392mm 31.5-inch                   | 1        | 0.27%   |
| VIZ LCD Monitor V405-H9 3840x2160                                     | 1        | 0.27%   |
| ViewSonic XG270QC VSCC438 2560x1440 597x336mm 27.0-inch               | 1        | 0.27%   |
| ViewSonic VX2757 VSCF931 1920x1080 598x336mm 27.0-inch                | 1        | 0.27%   |
| ViewSonic VP2468 Series VSCB032 1920x1080 527x296mm 23.8-inch         | 1        | 0.27%   |
| ViewSonic VA721 VSC6E19 1280x1024 340x270mm 17.1-inch                 | 1        | 0.27%   |
| ViewSonic VA2261 VSC0F30 1920x1080 477x268mm 21.5-inch                | 1        | 0.27%   |
| ViewSonic PF790-2 VSC4500 1600x1200 353x265mm 17.4-inch               | 1        | 0.27%   |
| VIE A2256 VIEE001 1920x1080 509x286mm 23.0-inch                       | 1        | 0.27%   |
| Unknown LCDTV16 0101 1920x1080 1600x900mm 72.3-inch                   | 1        | 0.27%   |
| Unknown LCD Monitor XXX WXGA TV 1360x768                              | 1        | 0.27%   |
| Unknown LCD Monitor XXX WXGA TV                                       | 1        | 0.27%   |
| Unknown LCD Monitor Sanyo Electric Co.,Ltd. SANYO-TV 1360x765         | 1        | 0.27%   |
| Unknown LCD Monitor PHILIPS FTV 1360x768                              | 1        | 0.27%   |
| Unknown LCD Monitor Maxdata/XXXXXXX BelArtist22W 5520x2160            | 1        | 0.27%   |
| Toshiba TV TSB0206 1920x1080 886x498mm 40.0-inch                      | 1        | 0.27%   |
| Toshiba TV TSB0108 1920x1080 890x500mm 40.2-inch                      | 1        | 0.27%   |
| Sun SCEI MONITOR SCE0301 1920x1080 522x294mm 23.6-inch                | 1        | 0.27%   |
| Sony TV SNY5501 1024x768 1600x900mm 72.3-inch                         | 1        | 0.27%   |
| Sony TV SNY2801 1920x1080 1600x900mm 72.3-inch                        | 1        | 0.27%   |
| Sony TV *00 SNY8604 3840x2160 1218x685mm 55.0-inch                    | 1        | 0.27%   |
| Sony TV *00 SNY4B04 3840x2160 1600x900mm 72.3-inch                    | 1        | 0.27%   |
| Sony SDM-HS74P SNY3070 1280x1024 338x270mm 17.0-inch                  | 1        | 0.27%   |
| Sony LCD Monitor TV 1360x768                                          | 1        | 0.27%   |
| SKY TV-monitor SKY0001 1920x1080 697x392mm 31.5-inch                  | 1        | 0.27%   |
| SKY TV Monitor SKY1502 3840x2160 1430x800mm 64.5-inch                 | 1        | 0.27%   |
| Sceptre Tech E248W-1920 SPT099D 1920x1080 443x249mm 20.0-inch         | 1        | 0.27%   |
| Sceptre Tech E205W-1600 SPT080D 1600x900 477x268mm 21.5-inch          | 1        | 0.27%   |
| Sanyo TV SAN0206 1920x1080 1600x900mm 72.3-inch                       | 1        | 0.27%   |
| Samsung Electronics V32F390 SAM0D5C 1920x1080 700x390mm 31.5-inch     | 1        | 0.27%   |
| Samsung Electronics U32J59x SAM0F33 3840x2160 697x392mm 31.5-inch     | 1        | 0.27%   |
| Samsung Electronics U28E590 SAM0C4D 3840x2160 607x345mm 27.5-inch     | 1        | 0.27%   |
| Samsung Electronics U24E590 SAM0CD2 3840x2160 521x293mm 23.5-inch     | 1        | 0.27%   |
| Samsung Electronics T22C350 SAM0AB7 1920x1080 477x268mm 21.5-inch     | 1        | 0.27%   |
| Samsung Electronics SyncMaster SAM0598 1360x768 410x230mm 18.5-inch   | 1        | 0.27%   |
| Samsung Electronics SyncMaster SAM0587 1920x1200 518x324mm 24.1-inch  | 1        | 0.27%   |

Monitor Resolution
------------------

Monitor screen resolution

![Monitor Resolution](./images/pie_chart/mon_resolution.svg)


| Resolution         | Desktops | Percent |
|--------------------|----------|---------|
| 1920x1080 (FHD)    | 150      | 43.73%  |
| 3840x2160 (4K)     | 31       | 9.04%   |
| 1280x1024 (SXGA)   | 29       | 8.45%   |
| 2560x1440 (QHD)    | 25       | 7.29%   |
| 1680x1050 (WSXGA+) | 24       | 7%      |
| 1920x1200 (WUXGA)  | 12       | 3.5%    |
| 1440x900 (WXGA+)   | 12       | 3.5%    |
| Unknown            | 11       | 3.21%   |
| 1360x768           | 8        | 2.33%   |
| 1366x768 (WXGA)    | 7        | 2.04%   |
| 3440x1440          | 6        | 1.75%   |
| 3840x1080          | 5        | 1.46%   |
| 2560x1080          | 5        | 1.46%   |
| 1600x900 (HD+)     | 5        | 1.46%   |
| 3200x1080          | 2        | 0.58%   |
| 5520x2160          | 1        | 0.29%   |
| 4480x1440          | 1        | 0.29%   |
| 4480x1080          | 1        | 0.29%   |
| 3840x1600          | 1        | 0.29%   |
| 2560x1600          | 1        | 0.29%   |
| 1920x540           | 1        | 0.29%   |
| 1600x1200          | 1        | 0.29%   |
| 1400x1050          | 1        | 0.29%   |
| 1360x765           | 1        | 0.29%   |
| 1280x768           | 1        | 0.29%   |
| 1024x768 (XGA)     | 1        | 0.29%   |

Monitor Diagonal
----------------

Diagonal size in inches

![Monitor Diagonal](./images/pie_chart/mon_diagonal.svg)


| Inches  | Desktops | Percent |
|---------|----------|---------|
| 27      | 54       | 15.93%  |
| 23      | 44       | 12.98%  |
| 24      | 37       | 10.91%  |
| 21      | 37       | 10.91%  |
| Unknown | 31       | 9.14%   |
| 19      | 22       | 6.49%   |
| 22      | 16       | 4.72%   |
| 17      | 15       | 4.42%   |
| 18      | 14       | 4.13%   |
| 31      | 11       | 3.24%   |
| 34      | 9        | 2.65%   |
| 72      | 8        | 2.36%   |
| 20      | 7        | 2.06%   |
| 26      | 5        | 1.47%   |
| 40      | 4        | 1.18%   |
| 84      | 3        | 0.88%   |
| 65      | 2        | 0.59%   |
| 49      | 2        | 0.59%   |
| 33      | 2        | 0.59%   |
| 74      | 1        | 0.29%   |
| 64      | 1        | 0.29%   |
| 63      | 1        | 0.29%   |
| 59      | 1        | 0.29%   |
| 57      | 1        | 0.29%   |
| 52      | 1        | 0.29%   |
| 48      | 1        | 0.29%   |
| 46      | 1        | 0.29%   |
| 42      | 1        | 0.29%   |
| 41      | 1        | 0.29%   |
| 38      | 1        | 0.29%   |
| 37      | 1        | 0.29%   |
| 32      | 1        | 0.29%   |
| 30      | 1        | 0.29%   |
| 15      | 1        | 0.29%   |
| 14      | 1        | 0.29%   |

Monitor Width
-------------

Physical width

![Monitor Width](./images/pie_chart/mon_width.svg)


| Width in mm | Desktops | Percent |
|-------------|----------|---------|
| 501-600     | 127      | 38.37%  |
| 401-500     | 80       | 24.17%  |
| Unknown     | 31       | 9.37%   |
| 601-700     | 18       | 5.44%   |
| 351-400     | 16       | 4.83%   |
| 301-350     | 15       | 4.53%   |
| 701-800     | 12       | 3.63%   |
| 1501-2000   | 12       | 3.63%   |
| 1001-1500   | 11       | 3.32%   |
| 801-900     | 6        | 1.81%   |
| 901-1000    | 2        | 0.6%    |
| 201-300     | 1        | 0.3%    |

Aspect Ratio
------------

Proportional relationship between the width and the height

![Aspect Ratio](./images/pie_chart/mon_ratio.svg)


| Ratio   | Desktops | Percent |
|---------|----------|---------|
| 16/9    | 194      | 61.39%  |
| 16/10   | 44       | 13.92%  |
| Unknown | 29       | 9.18%   |
| 5/4     | 28       | 8.86%   |
| 21/9    | 11       | 3.48%   |
| 4/3     | 4        | 1.27%   |
| 3/2     | 3        | 0.95%   |
| 6/5     | 2        | 0.63%   |
| 1.00    | 1        | 0.32%   |

Monitor Area
------------

Area in inch²

![Monitor Area](./images/pie_chart/mon_area.svg)


| Area in inch² | Desktops | Percent |
|----------------|----------|---------|
| 201-250        | 110      | 32.93%  |
| 301-350        | 57       | 17.07%  |
| 151-200        | 42       | 12.57%  |
| Unknown        | 31       | 9.28%   |
| 351-500        | 25       | 7.49%   |
| More than 1000 | 22       | 6.59%   |
| 141-150        | 20       | 5.99%   |
| 251-300        | 16       | 4.79%   |
| 501-1000       | 9        | 2.69%   |
| 101-110        | 2        | 0.6%    |

Pixel Density
-------------

Pixels per inch

![Pixel Density](./images/pie_chart/mon_density.svg)


| Density | Desktops | Percent |
|---------|----------|---------|
| 51-100  | 199      | 61.8%   |
| 101-120 | 56       | 17.39%  |
| Unknown | 31       | 9.63%   |
| 1-50    | 19       | 5.9%    |
| 121-160 | 9        | 2.8%    |
| 161-240 | 8        | 2.48%   |

Multiple Monitors
-----------------

Total monitors connected

![Multiple Monitors](./images/pie_chart/mon_total.svg)


| Total | Desktops | Percent |
|-------|----------|---------|
| 1     | 249      | 74.55%  |
| 2     | 61       | 18.26%  |
| 0     | 16       | 4.79%   |
| 3     | 8        | 2.4%    |

Network
-------

Net Controller Vendor
---------------------

Controller vendors

![Net Controller Vendor](./images/pie_chart/net_vendor.svg)


| Vendor                          | Desktops | Percent |
|---------------------------------|----------|---------|
| Realtek Semiconductor           | 201      | 42.14%  |
| Intel                           | 148      | 31.03%  |
| Qualcomm Atheros                | 32       | 6.71%   |
| Broadcom                        | 19       | 3.98%   |
| TP-Link                         | 12       | 2.52%   |
| Ralink Technology               | 8        | 1.68%   |
| Nvidia                          | 6        | 1.26%   |
| Marvell Technology Group        | 5        | 1.05%   |
| Aquantia                        | 5        | 1.05%   |
| Ralink                          | 4        | 0.84%   |
| Qualcomm Atheros Communications | 4        | 0.84%   |
| Microsoft                       | 4        | 0.84%   |
| NetGear                         | 3        | 0.63%   |
| Linksys                         | 3        | 0.63%   |
| ASUSTek Computer                | 3        | 0.63%   |
| Micro Star International        | 2        | 0.42%   |
| DisplayLink                     | 2        | 0.42%   |
| VIA Technologies                | 1        | 0.21%   |
| Tenda                           | 1        | 0.21%   |
| STMicroelectronics              | 1        | 0.21%   |
| SILICON Laboratories            | 1        | 0.21%   |
| Samsung Electronics             | 1        | 0.21%   |
| Microchip Technology            | 1        | 0.21%   |
| MediaTek                        | 1        | 0.21%   |
| IMC Networks                    | 1        | 0.21%   |
| GDMicroelectronics              | 1        | 0.21%   |
| Elecom                          | 1        | 0.21%   |
| D-Link System                   | 1        | 0.21%   |
| D-Link                          | 1        | 0.21%   |
| Broadcom Limited                | 1        | 0.21%   |
| Belkin Components               | 1        | 0.21%   |
| AVM                             | 1        | 0.21%   |
| ASIX Electronics                | 1        | 0.21%   |

Net Controller Model
--------------------

Controller models

![Net Controller Model](./images/pie_chart/net_model.svg)


| Model                                                                      | Desktops | Percent |
|----------------------------------------------------------------------------|----------|---------|
| Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller          | 142      | 26.2%   |
| Realtek RTL8125 2.5GbE Controller                                          | 30       | 5.54%   |
| Intel Wi-Fi 6 AX200                                                        | 23       | 4.24%   |
| Intel I211 Gigabit Network Connection                                      | 22       | 4.06%   |
| Intel 82579LM Gigabit Network Connection (Lewisville)                      | 19       | 3.51%   |
| Intel Ethernet Connection (2) I219-V                                       | 15       | 2.77%   |
| Intel Ethernet Connection I217-LM                                          | 10       | 1.85%   |
| Intel Ethernet Connection (7) I219-V                                       | 10       | 1.85%   |
| Intel Ethernet Controller I225-V                                           | 9        | 1.66%   |
| Realtek RTL810xE PCI Express Fast Ethernet controller                      | 7        | 1.29%   |
| Intel Wi-Fi 6 AX210/AX211/AX411 160MHz                                     | 7        | 1.29%   |
| Intel 82567LM-3 Gigabit Network Connection                                 | 7        | 1.29%   |
| Intel Dual Band Wireless-AC 3168NGW [Stone Peak]                           | 6        | 1.11%   |
| Realtek RTL88x2bu [AC1200 Techkey]                                         | 5        | 0.92%   |
| Realtek RTL8188EUS 802.11n Wireless Network Adapter                        | 5        | 0.92%   |
| Realtek RTL8169 PCI Gigabit Ethernet Controller                            | 5        | 0.92%   |
| Realtek 802.11ac NIC                                                       | 5        | 0.92%   |
| Intel Wireless-AC 9260                                                     | 5        | 0.92%   |
| Intel Cannon Lake PCH CNVi WiFi                                            | 5        | 0.92%   |
| Intel 600 Series Chipset Family Wireless-AC 9560                           | 5        | 0.92%   |
| Broadcom BCM4360 802.11ac Wireless Network Adapter                         | 5        | 0.92%   |
| TP-Link TL-WN823N v2/v3 [Realtek RTL8192EU]                                | 4        | 0.74%   |
| Ralink MT7601U Wireless Adapter                                            | 4        | 0.74%   |
| Qualcomm Atheros Killer E220x Gigabit Ethernet Controller                  | 4        | 0.74%   |
| Qualcomm Atheros AR9271 802.11n                                            | 4        | 0.74%   |
| Intel Ethernet Connection (2) I219-LM                                      | 4        | 0.74%   |
| Intel Ethernet Connection (14) I219-V                                      | 4        | 0.74%   |
| Intel 82579V Gigabit Network Connection                                    | 4        | 0.74%   |
| Realtek RTL8192CU 802.11n WLAN Adapter                                     | 3        | 0.55%   |
| Realtek RTL8153 Gigabit Ethernet Adapter                                   | 3        | 0.55%   |
| Qualcomm Atheros QCA9565 / AR9565 Wireless Network Adapter                 | 3        | 0.55%   |
| Qualcomm Atheros AR8161 Gigabit Ethernet                                   | 3        | 0.55%   |
| Nvidia MCP61 Ethernet                                                      | 3        | 0.55%   |
| Intel Wireless 7260                                                        | 3        | 0.55%   |
| Intel Ethernet Connection I217-V                                           | 3        | 0.55%   |
| Intel Ethernet Connection (2) I218-V                                       | 3        | 0.55%   |
| Intel Comet Lake PCH CNVi WiFi                                             | 3        | 0.55%   |
| Realtek RTL8821CE 802.11ac PCIe Wireless Network Adapter                   | 2        | 0.37%   |
| Realtek RTL8192CE PCIe Wireless Network Adapter                            | 2        | 0.37%   |
| Qualcomm Atheros QCA8171 Gigabit Ethernet                                  | 2        | 0.37%   |
| Qualcomm Atheros Killer E2400 Gigabit Ethernet Controller                  | 2        | 0.37%   |
| Qualcomm Atheros AR9485 Wireless Network Adapter                           | 2        | 0.37%   |
| Qualcomm Atheros AR9462 Wireless Network Adapter                           | 2        | 0.37%   |
| Qualcomm Atheros AR93xx Wireless Network Adapter                           | 2        | 0.37%   |
| Qualcomm Atheros AR8151 v2.0 Gigabit Ethernet                              | 2        | 0.37%   |
| Qualcomm Atheros AR8121/AR8113/AR8114 Gigabit or Fast Ethernet             | 2        | 0.37%   |
| Nvidia MCP51 Ethernet Controller                                           | 2        | 0.37%   |
| Microsoft Xbox 360 Wireless Adapter                                        | 2        | 0.37%   |
| Microsoft Wireless XBox Controller Dongle                                  | 2        | 0.37%   |
| Micro Star International MS-3871 802.11bgn Wireless Module [Ralink RT8070] | 2        | 0.37%   |
| Marvell Group 88E8056 PCI-E Gigabit Ethernet Controller                    | 2        | 0.37%   |
| Linksys WUSB54GC v1 802.11g Adapter [Ralink RT73]                          | 2        | 0.37%   |
| Intel Wireless 8260                                                        | 2        | 0.37%   |
| Intel I210 Gigabit Network Connection                                      | 2        | 0.37%   |
| Intel Ethernet Connection (7) I219-LM                                      | 2        | 0.37%   |
| Intel Ethernet Connection (2) I218-LM                                      | 2        | 0.37%   |
| Intel 82574L Gigabit Network Connection                                    | 2        | 0.37%   |
| Intel 80003ES2LAN Gigabit Ethernet Controller (Copper)                     | 2        | 0.37%   |
| Broadcom NetXtreme BCM5764M Gigabit Ethernet PCIe                          | 2        | 0.37%   |
| Broadcom NetXtreme BCM5754 Gigabit Ethernet PCI Express                    | 2        | 0.37%   |

Wireless Vendor
---------------

Wireless vendors

![Wireless Vendor](./images/pie_chart/net_wireless_vendor.svg)


| Vendor                          | Desktops | Percent |
|---------------------------------|----------|---------|
| Intel                           | 63       | 37.5%   |
| Realtek Semiconductor           | 32       | 19.05%  |
| Qualcomm Atheros                | 13       | 7.74%   |
| TP-Link                         | 11       | 6.55%   |
| Broadcom                        | 9        | 5.36%   |
| Ralink Technology               | 8        | 4.76%   |
| Ralink                          | 4        | 2.38%   |
| Qualcomm Atheros Communications | 4        | 2.38%   |
| Microsoft                       | 4        | 2.38%   |
| NetGear                         | 3        | 1.79%   |
| Linksys                         | 3        | 1.79%   |
| ASUSTek Computer                | 3        | 1.79%   |
| Micro Star International        | 2        | 1.19%   |
| Tenda                           | 1        | 0.6%    |
| MediaTek                        | 1        | 0.6%    |
| IMC Networks                    | 1        | 0.6%    |
| Elecom                          | 1        | 0.6%    |
| D-Link System                   | 1        | 0.6%    |
| D-Link                          | 1        | 0.6%    |
| Broadcom Limited                | 1        | 0.6%    |
| Belkin Components               | 1        | 0.6%    |
| AVM                             | 1        | 0.6%    |

Wireless Model
--------------

Wireless models

![Wireless Model](./images/pie_chart/net_wireless_model.svg)


| Model                                                                      | Desktops | Percent |
|----------------------------------------------------------------------------|----------|---------|
| Intel Wi-Fi 6 AX200                                                        | 23       | 13.69%  |
| Intel Wi-Fi 6 AX210/AX211/AX411 160MHz                                     | 7        | 4.17%   |
| Intel Dual Band Wireless-AC 3168NGW [Stone Peak]                           | 6        | 3.57%   |
| Realtek RTL88x2bu [AC1200 Techkey]                                         | 5        | 2.98%   |
| Realtek RTL8188EUS 802.11n Wireless Network Adapter                        | 5        | 2.98%   |
| Realtek 802.11ac NIC                                                       | 5        | 2.98%   |
| Intel Wireless-AC 9260                                                     | 5        | 2.98%   |
| Intel Cannon Lake PCH CNVi WiFi                                            | 5        | 2.98%   |
| Intel 600 Series Chipset Family Wireless-AC 9560                           | 5        | 2.98%   |
| Broadcom BCM4360 802.11ac Wireless Network Adapter                         | 5        | 2.98%   |
| TP-Link TL-WN823N v2/v3 [Realtek RTL8192EU]                                | 4        | 2.38%   |
| Ralink MT7601U Wireless Adapter                                            | 4        | 2.38%   |
| Qualcomm Atheros AR9271 802.11n                                            | 4        | 2.38%   |
| Realtek RTL8192CU 802.11n WLAN Adapter                                     | 3        | 1.79%   |
| Qualcomm Atheros QCA9565 / AR9565 Wireless Network Adapter                 | 3        | 1.79%   |
| Intel Wireless 7260                                                        | 3        | 1.79%   |
| Intel Comet Lake PCH CNVi WiFi                                             | 3        | 1.79%   |
| Realtek RTL8821CE 802.11ac PCIe Wireless Network Adapter                   | 2        | 1.19%   |
| Realtek RTL8192CE PCIe Wireless Network Adapter                            | 2        | 1.19%   |
| Qualcomm Atheros AR9485 Wireless Network Adapter                           | 2        | 1.19%   |
| Qualcomm Atheros AR9462 Wireless Network Adapter                           | 2        | 1.19%   |
| Qualcomm Atheros AR93xx Wireless Network Adapter                           | 2        | 1.19%   |
| Microsoft Xbox 360 Wireless Adapter                                        | 2        | 1.19%   |
| Microsoft Wireless XBox Controller Dongle                                  | 2        | 1.19%   |
| Micro Star International MS-3871 802.11bgn Wireless Module [Ralink RT8070] | 2        | 1.19%   |
| Linksys WUSB54GC v1 802.11g Adapter [Ralink RT73]                          | 2        | 1.19%   |
| Intel Wireless 8260                                                        | 2        | 1.19%   |
| Broadcom Network controller                                                | 2        | 1.19%   |
| ASUS AC51 802.11a/b/g/n/ac Wireless Adapter [Mediatek MT7610U]             | 2        | 1.19%   |
| TP-Link TL-WN822N Version 4 RTL8192EU                                      | 1        | 0.6%    |
| TP-Link TL-WN722N v2/v3 [Realtek RTL8188EUS]                               | 1        | 0.6%    |
| TP-Link Archer T4U ver.3                                                   | 1        | 0.6%    |
| TP-Link Archer T3U [Realtek RTL8812BU]                                     | 1        | 0.6%    |
| TP-Link Archer T2U PLUS [RTL8821AU]                                        | 1        | 0.6%    |
| TP-Link AC600 wireless Realtek RTL8811AU [Archer T2U Nano]                 | 1        | 0.6%    |
| TP-Link 802.11ac WLAN Adapter                                              | 1        | 0.6%    |
| Tenda U12                                                                  | 1        | 0.6%    |
| Realtek RTL8814AU 802.11a/b/g/n/ac Wireless Adapter                        | 1        | 0.6%    |
| Realtek RTL8812AU 802.11a/b/g/n/ac 2T2R DB WLAN Adapter                    | 1        | 0.6%    |
| Realtek RTL8812AE 802.11ac PCIe Wireless Network Adapter                   | 1        | 0.6%    |
| Realtek RTL8191SU 802.11n WLAN Adapter                                     | 1        | 0.6%    |
| Realtek RTL8188FTV 802.11b/g/n 1T1R 2.4G WLAN Adapter                      | 1        | 0.6%    |
| Realtek RTL8188EE Wireless Network Adapter                                 | 1        | 0.6%    |
| Realtek RTL8188CUS 802.11n WLAN Adapter                                    | 1        | 0.6%    |
| Realtek RTL8188CE 802.11b/g/n WiFi Adapter                                 | 1        | 0.6%    |
| Realtek RTL8187 Wireless Adapter                                           | 1        | 0.6%    |
| Realtek 802.11ac+Bluetooth 5.0 Adapter                                     | 1        | 0.6%    |
| Ralink RT5572 Wireless Adapter                                             | 1        | 0.6%    |
| Ralink RT5372 Wireless Adapter                                             | 1        | 0.6%    |
| Ralink RT5370 Wireless Adapter                                             | 1        | 0.6%    |
| Ralink RT2870/RT3070 Wireless Adapter                                      | 1        | 0.6%    |
| Ralink RT3090 Wireless 802.11n 1T/1R PCIe                                  | 1        | 0.6%    |
| Ralink RT2800 802.11n PCI                                                  | 1        | 0.6%    |
| Ralink RT2561/RT61 rev B 802.11g                                           | 1        | 0.6%    |
| Ralink RT2561/RT61 802.11g PCI                                             | 1        | 0.6%    |
| Qualcomm Atheros QCA9377 802.11ac Wireless Network Adapter                 | 1        | 0.6%    |
| Qualcomm Atheros QCA6174 802.11ac Wireless Network Adapter                 | 1        | 0.6%    |
| Qualcomm Atheros AR9287 Wireless Network Adapter (PCI-Express)             | 1        | 0.6%    |
| Qualcomm Atheros AR9227 Wireless Network Adapter                           | 1        | 0.6%    |
| NetGear WNA3100(v1) Wireless-N 300 [Broadcom BCM43231]                     | 1        | 0.6%    |

Ethernet Vendor
---------------

Ethernet vendors

![Ethernet Vendor](./images/pie_chart/net_ethernet_vendor.svg)


| Vendor                   | Desktops | Percent |
|--------------------------|----------|---------|
| Realtek Semiconductor    | 187      | 51.23%  |
| Intel                    | 126      | 34.52%  |
| Qualcomm Atheros         | 19       | 5.21%   |
| Broadcom                 | 10       | 2.74%   |
| Nvidia                   | 6        | 1.64%   |
| Marvell Technology Group | 5        | 1.37%   |
| Aquantia                 | 5        | 1.37%   |
| DisplayLink              | 2        | 0.55%   |
| VIA Technologies         | 1        | 0.27%   |
| TP-Link                  | 1        | 0.27%   |
| Samsung Electronics      | 1        | 0.27%   |
| Microchip Technology     | 1        | 0.27%   |
| ASIX Electronics         | 1        | 0.27%   |

Ethernet Model
--------------

Ethernet models

![Ethernet Model](./images/pie_chart/net_ethernet_model.svg)


| Model                                                                         | Desktops | Percent |
|-------------------------------------------------------------------------------|----------|---------|
| Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller             | 142      | 38.27%  |
| Realtek RTL8125 2.5GbE Controller                                             | 30       | 8.09%   |
| Intel I211 Gigabit Network Connection                                         | 22       | 5.93%   |
| Intel 82579LM Gigabit Network Connection (Lewisville)                         | 19       | 5.12%   |
| Intel Ethernet Connection (2) I219-V                                          | 15       | 4.04%   |
| Intel Ethernet Connection I217-LM                                             | 10       | 2.7%    |
| Intel Ethernet Connection (7) I219-V                                          | 10       | 2.7%    |
| Intel Ethernet Controller I225-V                                              | 9        | 2.43%   |
| Realtek RTL810xE PCI Express Fast Ethernet controller                         | 7        | 1.89%   |
| Intel 82567LM-3 Gigabit Network Connection                                    | 7        | 1.89%   |
| Realtek RTL8169 PCI Gigabit Ethernet Controller                               | 5        | 1.35%   |
| Qualcomm Atheros Killer E220x Gigabit Ethernet Controller                     | 4        | 1.08%   |
| Intel Ethernet Connection (2) I219-LM                                         | 4        | 1.08%   |
| Intel Ethernet Connection (14) I219-V                                         | 4        | 1.08%   |
| Intel 82579V Gigabit Network Connection                                       | 4        | 1.08%   |
| Realtek RTL8153 Gigabit Ethernet Adapter                                      | 3        | 0.81%   |
| Qualcomm Atheros AR8161 Gigabit Ethernet                                      | 3        | 0.81%   |
| Nvidia MCP61 Ethernet                                                         | 3        | 0.81%   |
| Intel Ethernet Connection I217-V                                              | 3        | 0.81%   |
| Intel Ethernet Connection (2) I218-V                                          | 3        | 0.81%   |
| Qualcomm Atheros QCA8171 Gigabit Ethernet                                     | 2        | 0.54%   |
| Qualcomm Atheros Killer E2400 Gigabit Ethernet Controller                     | 2        | 0.54%   |
| Qualcomm Atheros AR8151 v2.0 Gigabit Ethernet                                 | 2        | 0.54%   |
| Qualcomm Atheros AR8121/AR8113/AR8114 Gigabit or Fast Ethernet                | 2        | 0.54%   |
| Nvidia MCP51 Ethernet Controller                                              | 2        | 0.54%   |
| Marvell Group 88E8056 PCI-E Gigabit Ethernet Controller                       | 2        | 0.54%   |
| Intel I210 Gigabit Network Connection                                         | 2        | 0.54%   |
| Intel Ethernet Connection (7) I219-LM                                         | 2        | 0.54%   |
| Intel Ethernet Connection (2) I218-LM                                         | 2        | 0.54%   |
| Intel 82574L Gigabit Network Connection                                       | 2        | 0.54%   |
| Intel 80003ES2LAN Gigabit Ethernet Controller (Copper)                        | 2        | 0.54%   |
| Broadcom NetXtreme BCM5764M Gigabit Ethernet PCIe                             | 2        | 0.54%   |
| Broadcom NetXtreme BCM5754 Gigabit Ethernet PCI Express                       | 2        | 0.54%   |
| Broadcom NetXtreme BCM5719 Gigabit Ethernet PCIe                              | 2        | 0.54%   |
| Broadcom NetLink BCM57781 Gigabit Ethernet PCIe                               | 2        | 0.54%   |
| Aquantia AQC111 NBase-T/IEEE 802.3bz Ethernet Controller [AQtion]             | 2        | 0.54%   |
| Aquantia AQC107 NBase-T/IEEE 802.3bz Ethernet Controller [AQtion]             | 2        | 0.54%   |
| VIA VT6102/VT6103 [Rhine-II]                                                  | 1        | 0.27%   |
| TP-Link USB 10/100/1000 LAN                                                   | 1        | 0.27%   |
| Samsung Galaxy series, misc. (tethering mode)                                 | 1        | 0.27%   |
| Realtek RTL-8100/8101L/8139 PCI Fast Ethernet Adapter                         | 1        | 0.27%   |
| Realtek Killer E2600 Gigabit Ethernet Controller                              | 1        | 0.27%   |
| Qualcomm Atheros Killer E2500 Gigabit Ethernet Controller                     | 1        | 0.27%   |
| Qualcomm Atheros Attansic L2 Fast Ethernet                                    | 1        | 0.27%   |
| Qualcomm Atheros Attansic L1 Gigabit Ethernet                                 | 1        | 0.27%   |
| Qualcomm Atheros AR8131 Gigabit Ethernet                                      | 1        | 0.27%   |
| Nvidia MCP55 Ethernet                                                         | 1        | 0.27%   |
| Microchip LAN9500A/LAN9500Ai                                                  | 1        | 0.27%   |
| Marvell Group 88E8071 PCI-E Gigabit Ethernet Controller                       | 1        | 0.27%   |
| Marvell Group 88E8070 based Ethernet Controller                               | 1        | 0.27%   |
| Marvell Group 88E8057 PCI-E Gigabit Ethernet Controller                       | 1        | 0.27%   |
| Intel NM10/ICH7 Family LAN Controller                                         | 1        | 0.27%   |
| Intel Ethernet Connection (5) I219-LM                                         | 1        | 0.27%   |
| Intel Ethernet Connection (17) I219-V                                         | 1        | 0.27%   |
| Intel Ethernet Connection (11) I219-LM                                        | 1        | 0.27%   |
| Intel 82578DM Gigabit Network Connection                                      | 1        | 0.27%   |
| Intel 82576 Gigabit Network Connection                                        | 1        | 0.27%   |
| Intel 82571EB/82571GB Gigabit Ethernet Controller D0/D1 (copper applications) | 1        | 0.27%   |
| Intel 82567V-2 Gigabit Network Connection                                     | 1        | 0.27%   |
| Intel 82566DM-2 Gigabit Network Connection                                    | 1        | 0.27%   |

Net Controller Kind
-------------------

Ethernet, WiFi or modem

![Net Controller Kind](./images/pie_chart/net_kind.svg)


| Kind     | Desktops | Percent |
|----------|----------|---------|
| Ethernet | 329      | 67.14%  |
| WiFi     | 158      | 32.24%  |
| Modem    | 3        | 0.61%   |

Used Controller
---------------

Currently used network controller

![Used Controller](./images/pie_chart/net_used.svg)


| Kind     | Desktops | Percent |
|----------|----------|---------|
| Ethernet | 283      | 69.88%  |
| WiFi     | 122      | 30.12%  |

NICs
----

Total network controllers on board

![NICs](./images/pie_chart/net_nics.svg)


| Total | Desktops | Percent |
|-------|----------|---------|
| 1     | 209      | 62.95%  |
| 2     | 94       | 28.31%  |
| 3     | 23       | 6.93%   |
| 5     | 2        | 0.6%    |
| 0     | 2        | 0.6%    |
| 6     | 1        | 0.3%    |
| 4     | 1        | 0.3%    |

IPv6
----

IPv6 vs IPv4

![IPv6](./images/pie_chart/node_ipv6.svg)


| Used | Desktops | Percent |
|------|----------|---------|
| No   | 213      | 63.2%   |
| Yes  | 124      | 36.8%   |

Bluetooth
---------

Bluetooth Vendor
----------------

Controller vendors

![Bluetooth Vendor](./images/pie_chart/bt_vendor.svg)


| Vendor                          | Desktops | Percent |
|---------------------------------|----------|---------|
| Intel                           | 55       | 46.22%  |
| Cambridge Silicon Radio         | 35       | 29.41%  |
| ASUSTek Computer                | 8        | 6.72%   |
| Realtek Semiconductor           | 6        | 5.04%   |
| Qualcomm Atheros Communications | 5        | 4.2%    |
| Broadcom                        | 4        | 3.36%   |
| Micro Star International        | 2        | 1.68%   |
| Belkin Components               | 2        | 1.68%   |
| Edimax Technology               | 1        | 0.84%   |
| Apple                           | 1        | 0.84%   |

Bluetooth Model
---------------

Controller models

![Bluetooth Model](./images/pie_chart/bt_model.svg)


| Model                                                 | Desktops | Percent |
|-------------------------------------------------------|----------|---------|
| Cambridge Silicon Radio Bluetooth Dongle (HCI mode)   | 35       | 29.41%  |
| Intel AX200 Bluetooth                                 | 21       | 17.65%  |
| Intel Bluetooth Device                                | 20       | 16.81%  |
| Realtek Bluetooth Radio                               | 5        | 4.2%    |
| Intel Wireless-AC 9260 Bluetooth Adapter              | 5        | 4.2%    |
| Intel Wireless-AC 3168 Bluetooth                      | 5        | 4.2%    |
| Intel Bluetooth wireless interface                    | 4        | 3.36%   |
| Broadcom BCM20702A0 Bluetooth 4.0                     | 4        | 3.36%   |
| Qualcomm Atheros  Bluetooth Device                    | 3        | 2.52%   |
| ASUS ASUS USB-BT500                                   | 3        | 2.52%   |
| Micro Star International Bluetooth Device             | 2        | 1.68%   |
| ASUS Broadcom BCM20702A0 Bluetooth                    | 2        | 1.68%   |
| Realtek  Bluetooth 4.2 Adapter                        | 1        | 0.84%   |
| Qualcomm Atheros Bluetooth USB Host Controller        | 1        | 0.84%   |
| Qualcomm Atheros AR9462 Bluetooth                     | 1        | 0.84%   |
| Edimax Bluetooth Adapter                              | 1        | 0.84%   |
| Belkin Components F8T065BF Mini Bluetooth 4.0 Adapter | 1        | 0.84%   |
| Belkin Components F8T001v2 Bluetooth                  | 1        | 0.84%   |
| ASUS Qualcomm Bluetooth 4.1                           | 1        | 0.84%   |
| ASUS Broadcom BCM20702 Single-Chip Bluetooth 4.0 + LE | 1        | 0.84%   |
| ASUS Bluetooth Adapter                                | 1        | 0.84%   |
| Apple Bluetooth HCI                                   | 1        | 0.84%   |

Sound
-----

Sound Vendor
------------

Sound card vendors

![Sound Vendor](./images/pie_chart/snd_vendor.svg)


| Vendor                     | Desktops | Percent |
|----------------------------|----------|---------|
| Intel                      | 201      | 36.88%  |
| Nvidia                     | 134      | 24.59%  |
| AMD                        | 133      | 24.4%   |
| Creative Labs              | 17       | 3.12%   |
| C-Media Electronics        | 12       | 2.2%    |
| Logitech                   | 6        | 1.1%    |
| GN Netcom                  | 4        | 0.73%   |
| Corsair                    | 3        | 0.55%   |
| ASUSTek Computer           | 3        | 0.55%   |
| VIA Technologies           | 2        | 0.37%   |
| Tenx Technology            | 2        | 0.37%   |
| Razer USA                  | 2        | 0.37%   |
| Generalplus Technology     | 2        | 0.37%   |
| Focusrite-Novation         | 2        | 0.37%   |
| Yamaha                     | 1        | 0.18%   |
| XMOS                       | 1        | 0.18%   |
| Unknown                    | 1        | 0.18%   |
| Texas Instruments          | 1        | 0.18%   |
| Samson Technologies        | 1        | 0.18%   |
| Realtek Semiconductor      | 1        | 0.18%   |
| PreSonus Audio Electronics | 1        | 0.18%   |
| Plantronics                | 1        | 0.18%   |
| Panasonic (Matsushita)     | 1        | 0.18%   |
| Micro Star International   | 1        | 0.18%   |
| MAG Technology             | 1        | 0.18%   |
| M-Audio                    | 1        | 0.18%   |
| JMTek                      | 1        | 0.18%   |
| FIFINE Microphones         | 1        | 0.18%   |
| FENDER MUSICAL INSTRUMENTS | 1        | 0.18%   |
| Digidesign                 | 1        | 0.18%   |
| DCMT Technology            | 1        | 0.18%   |
| Creative Technology        | 1        | 0.18%   |
| Cambridge Silicon Radio    | 1        | 0.18%   |
| Bose                       | 1        | 0.18%   |
| Audio-Technica             | 1        | 0.18%   |
| Astro Gaming               | 1        | 0.18%   |

Sound Model
-----------

Sound card models

![Sound Model](./images/pie_chart/snd_model.svg)


| Model                                                                             | Desktops | Percent |
|-----------------------------------------------------------------------------------|----------|---------|
| AMD Starship/Matisse HD Audio Controller                                          | 31       | 4.85%   |
| AMD SBx00 Azalia (Intel HDA)                                                      | 27       | 4.23%   |
| Intel 8 Series/C220 Series Chipset High Definition Audio Controller               | 26       | 4.07%   |
| Intel 6 Series/C200 Series Chipset Family High Definition Audio Controller        | 25       | 3.91%   |
| Nvidia GK208 HDMI/DP Audio Controller                                             | 21       | 3.29%   |
| Intel 7 Series/C216 Chipset Family High Definition Audio Controller               | 21       | 3.29%   |
| AMD Family 17h (Models 10h-1fh) HD Audio Controller                               | 20       | 3.13%   |
| Intel Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller                  | 18       | 2.82%   |
| Intel NM10/ICH7 Family High Definition Audio Controller                           | 16       | 2.5%    |
| AMD Family 17h (Models 00h-0fh) HD Audio Controller                               | 16       | 2.5%    |
| Intel 200 Series PCH HD Audio                                                     | 15       | 2.35%   |
| Intel 100 Series/C230 Series Chipset Family HD Audio Controller                   | 14       | 2.19%   |
| Intel Cannon Lake PCH cAVS                                                        | 13       | 2.03%   |
| Nvidia GP106 High Definition Audio Controller                                     | 10       | 1.56%   |
| Intel 82801JI (ICH10 Family) HD Audio Controller                                  | 10       | 1.56%   |
| Nvidia High Definition Audio Controller                                           | 9        | 1.41%   |
| Intel 600 Series Chipset Family Smart Sound Technology (SST) Audio Controller     | 9        | 1.41%   |
| AMD Raven/Raven2/Fenghuang HDMI/DP Audio Controller                               | 9        | 1.41%   |
| AMD Ellesmere HDMI Audio [Radeon RX 470/480 / 570/580/590]                        | 9        | 1.41%   |
| Nvidia GP104 High Definition Audio Controller                                     | 8        | 1.25%   |
| AMD Renoir Radeon High Definition Audio Controller                                | 8        | 1.25%   |
| AMD Navi 10 HDMI Audio                                                            | 8        | 1.25%   |
| AMD Baffin HDMI/DP Audio [Radeon RX 550 640SP / RX 560/560X]                      | 8        | 1.25%   |
| Nvidia TU104 HD Audio Controller                                                  | 7        | 1.1%    |
| Nvidia GP107GL High Definition Audio Controller                                   | 7        | 1.1%    |
| Intel 82801JD/DO (ICH10 Family) HD Audio Controller                               | 7        | 1.1%    |
| Intel 82801I (ICH9 Family) HD Audio Controller                                    | 7        | 1.1%    |
| Creative Labs Sound Core3D [Sound Blaster Recon3D / Z-Series]                     | 7        | 1.1%    |
| AMD FCH Azalia Controller                                                         | 7        | 1.1%    |
| Nvidia GF108 High Definition Audio Controller                                     | 6        | 0.94%   |
| AMD Oland/Hainan/Cape Verde/Pitcairn HDMI Audio [Radeon HD 7000 Series]           | 6        | 0.94%   |
| AMD Navi 21 HDMI Audio [Radeon RX 6800/6800 XT / 6900 XT]                         | 6        | 0.94%   |
| Nvidia TU116 High Definition Audio Controller                                     | 5        | 0.78%   |
| Nvidia GM107 High Definition Audio Controller [GeForce 940MX]                     | 5        | 0.78%   |
| Nvidia GF119 HDMI Audio Controller                                                | 5        | 0.78%   |
| Intel Audio device                                                                | 5        | 0.78%   |
| Intel 9 Series Chipset Family HD Audio Controller                                 | 5        | 0.78%   |
| AMD Trinity HDMI Audio Controller                                                 | 5        | 0.78%   |
| AMD Cedar HDMI Audio [Radeon HD 5400/6300/7300 Series]                            | 5        | 0.78%   |
| AMD Caicos HDMI Audio [Radeon HD 6450 / 7450/8450/8490 OEM / R5 230/235/235X OEM] | 5        | 0.78%   |
| Nvidia MCP61 High Definition Audio                                                | 4        | 0.63%   |
| Nvidia GP108 High Definition Audio Controller                                     | 4        | 0.63%   |
| Nvidia GM206 High Definition Audio Controller                                     | 4        | 0.63%   |
| Nvidia GM204 High Definition Audio Controller                                     | 4        | 0.63%   |
| Nvidia GK110 High Definition Audio Controller                                     | 4        | 0.63%   |
| Nvidia GK107 HDMI Audio Controller                                                | 4        | 0.63%   |
| Nvidia GK106 HDMI Audio Controller                                                | 4        | 0.63%   |
| Nvidia GA102 High Definition Audio Controller                                     | 4        | 0.63%   |
| Intel Tiger Lake-H HD Audio Controller                                            | 4        | 0.63%   |
| Intel Comet Lake PCH cAVS                                                         | 4        | 0.63%   |
| Intel C610/X99 series chipset HD Audio Controller                                 | 4        | 0.63%   |
| Intel C600/X79 series chipset High Definition Audio Controller                    | 4        | 0.63%   |
| Creative Labs EMU20k2 [Sound Blaster X-Fi Titanium Series]                        | 4        | 0.63%   |
| C-Media Electronics Audio Adapter (Unitek Y-247A)                                 | 4        | 0.63%   |
| AMD RV710/730 HDMI Audio [Radeon HD 4000 series]                                  | 4        | 0.63%   |
| Nvidia GK104 HDMI Audio Controller                                                | 3        | 0.47%   |
| Nvidia GF106 High Definition Audio Controller                                     | 3        | 0.47%   |
| Nvidia Audio device                                                               | 3        | 0.47%   |
| Intel 5 Series/3400 Series Chipset High Definition Audio                          | 3        | 0.47%   |
| Creative Labs EMU10k2/CA0100/CA0102/CA10200 [Sound Blaster Audigy Series]         | 3        | 0.47%   |

Memory
------

Memory Vendor
-------------

Memory module vendors

![Memory Vendor](./images/pie_chart/memory_vendor.svg)


| Vendor              | Desktops | Percent |
|---------------------|----------|---------|
| Kingston            | 20       | 14.39%  |
| Corsair             | 20       | 14.39%  |
| G.Skill             | 19       | 13.67%  |
| Crucial             | 19       | 13.67%  |
| Unknown             | 18       | 12.95%  |
| Samsung Electronics | 13       | 9.35%   |
| SK Hynix            | 5        | 3.6%    |
| Micron Technology   | 5        | 3.6%    |
| Team                | 4        | 2.88%   |
| Patriot             | 3        | 2.16%   |
| PNY                 | 2        | 1.44%   |
| Nanya Technology    | 2        | 1.44%   |
| A-DATA Technology   | 2        | 1.44%   |
| Transcend           | 1        | 0.72%   |
| Ramaxel Technology  | 1        | 0.72%   |
| Hewlett-Packard     | 1        | 0.72%   |
| GEIL                | 1        | 0.72%   |
| Elpida              | 1        | 0.72%   |
| ASint Technology    | 1        | 0.72%   |
| Unknown             | 1        | 0.72%   |

Memory Model
------------

Memory module models

![Memory Model](./images/pie_chart/memory_model.svg)


| Model                                                   | Desktops | Percent |
|---------------------------------------------------------|----------|---------|
| Unknown RAM Module 4GB DIMM 1333MT/s                    | 6        | 4%      |
| G.Skill RAM F4-3200C16-8GVKB 8GB DIMM DDR4 3200MT/s     | 4        | 2.67%   |
| Corsair RAM CMW16GX4M2C3200C16 8GB DIMM DDR4 3266MT/s   | 3        | 2%      |
| Kingston RAM KHX3200C16D4/8GX 8192MB DIMM DDR4 3533MT/s | 2        | 1.33%   |
| G.Skill RAM F4-3200C16-16GVK 16GB DIMM DDR4 3600MT/s    | 2        | 1.33%   |
| G.Skill RAM F3-2133C9-8GXH 8GB DIMM DDR3 2133MT/s       | 2        | 1.33%   |
| Corsair RAM CMX8GX3M2A1600C9 4GB DIMM DDR3 1800MT/s     | 2        | 1.33%   |
| Corsair RAM CMK16GX4M2B3200C16 8GB DIMM DDR4 3600MT/s   | 2        | 1.33%   |
| Unknown RAM Module 8GB DIMM DDR4 2667MT/s               | 1        | 0.67%   |
| Unknown RAM Module 8GB DIMM DDR3 1600MT/s               | 1        | 0.67%   |
| Unknown RAM Module 8GB DIMM DDR3 1066MT/s               | 1        | 0.67%   |
| Unknown RAM Module 8GB DIMM 400MT/s                     | 1        | 0.67%   |
| Unknown RAM Module 8GB DIMM 1333MT/s                    | 1        | 0.67%   |
| Unknown RAM Module 4GB DIMM DDR3 1333MT/s               | 1        | 0.67%   |
| Unknown RAM Module 4GB DIMM 1600MT/s                    | 1        | 0.67%   |
| Unknown RAM Module 4GB DDR3 1600MT/s                    | 1        | 0.67%   |
| Unknown RAM Module 2GB DIMM SDRAM                       | 1        | 0.67%   |
| Unknown RAM Module 2GB DIMM DDR3 1067MT/s               | 1        | 0.67%   |
| Unknown RAM Module 2GB DIMM 800MT/s                     | 1        | 0.67%   |
| Unknown RAM Module 2GB DIMM 1600MT/s                    | 1        | 0.67%   |
| Unknown RAM Module 2GB DIMM 1333MT/s                    | 1        | 0.67%   |
| Unknown RAM Module 1GB DIMM SDRAM                       | 1        | 0.67%   |
| Unknown RAM Module 1GB DIMM 800MT/s                     | 1        | 0.67%   |
| Unknown RAM 3000 C16 Series 8192MB DIMM DDR4 2133MT/s   | 1        | 0.67%   |
| Transcend RAM TS512MLK72W6H 4GB DIMM DDR3 1600MT/s      | 1        | 0.67%   |
| Team RAM TEAMGROUP-UD4-3600 8192MB DIMM DDR4 3600MT/s   | 1        | 0.67%   |
| Team RAM TEAMGROUP-UD4-3200 16384MB DIMM DDR4 3200MT/s  | 1        | 0.67%   |
| Team RAM TEAMGROUP-UD4-3000 8GB DIMM DDR4 3067MT/s      | 1        | 0.67%   |
| Team RAM TEAMGROUP-UD3-1600 8GB DIMM DDR3 1600MT/s      | 1        | 0.67%   |
| SK Hynix RAM Module 2GB DIMM DDR3 1333MT/s              | 1        | 0.67%   |
| SK Hynix RAM HYMP125U64CP8-S6 2GB DIMM DDR2 49926MT/s   | 1        | 0.67%   |
| SK Hynix RAM HMT451U6BFR8C-PB 4GB DIMM DDR3 1600MT/s    | 1        | 0.67%   |
| SK Hynix RAM HMT351U6CFR8C-PB 4GB DIMM DDR3 1800MT/s    | 1        | 0.67%   |
| SK Hynix RAM HMT351S6CFR8C-PB 4GB DIMM DDR3 1600MT/s    | 1        | 0.67%   |
| SK Hynix RAM HMT325U6CFR8C-PB 2GB DIMM DDR3 1600MT/s    | 1        | 0.67%   |
| Samsung RAM Module 8GB DIMM DDR4 2667MT/s               | 1        | 0.67%   |
| Samsung RAM Module 32GB DIMM DDR4 2933MT/s              | 1        | 0.67%   |
| Samsung RAM M471A4G43MB1-CTD 32GB SODIMM DDR4 2667MT/s  | 1        | 0.67%   |
| Samsung RAM M471A1K43CB1-CTD 8GB DIMM DDR4 2667MT/s     | 1        | 0.67%   |
| Samsung RAM M393B1K70DH0 8GB DIMM DDR3 1866MT/s         | 1        | 0.67%   |
| Samsung RAM M391B5273CH0-CH9 4GB DIMM DDR3 1333MT/s     | 1        | 0.67%   |
| Samsung RAM M391B5173QH0-YK0 4GB DIMM DDR3 1600MT/s     | 1        | 0.67%   |
| Samsung RAM M378B5673FH0-CF8 2048MB DIMM DDR3 1067MT/s  | 1        | 0.67%   |
| Samsung RAM M378B5273DH0-CH9 4GB SODIMM DDR3 1333MT/s   | 1        | 0.67%   |
| Samsung RAM M378B5273CH0-CK0 4096MB DIMM DDR3 2000MT/s  | 1        | 0.67%   |
| Samsung RAM M378B1G73QH0-CK0 8GB DIMM DDR3 1600MT/s     | 1        | 0.67%   |
| Samsung RAM M378A5244CB0-CRC 4GB DIMM DDR4 2400MT/s     | 1        | 0.67%   |
| Samsung RAM M378A1K43BB2-CRC 8GB DIMM DDR4 3400MT/s     | 1        | 0.67%   |
| Ramaxel RAM RMR1870ED48E8F1066 2GB DIMM DDR3 1067MT/s   | 1        | 0.67%   |
| PNY RAM 8GBF1X08RHLL42-135-K 8GB DIMM DDR4 2667MT/s     | 1        | 0.67%   |
| PNY RAM 16GF2X08QFHH36-135-K 16GB DIMM DDR4 3200MT/s    | 1        | 0.67%   |
| Patriot RAM 4400 C19 Series 8GB DIMM DDR4 4266MT/s      | 1        | 0.67%   |
| Patriot RAM 2133 CL11 Series 8GB DIMM DDR3 2400MT/s     | 1        | 0.67%   |
| Patriot RAM 2133 C14 Series 4GB DIMM DDR4 2133MT/s      | 1        | 0.67%   |
| Nanya RAM NT2GT64U8HD0BY-AD 2GB DIMM DDR2 2048MT/s      | 1        | 0.67%   |
| Nanya RAM M2F4GH64CB8HB6N-CG 4GB DIMM DDR3 1333MT/s     | 1        | 0.67%   |
| Micron RAM CT51264BD160B.C16F 4GB DIMM DDR3 1600MT/s    | 1        | 0.67%   |
| Micron RAM 4ATF51264AZ-2G3B1 4GB DIMM DDR4 2800MT/s     | 1        | 0.67%   |
| Micron RAM 18HTF25672AY-800G1 2GB DIMM DDR2 1600MT/s    | 1        | 0.67%   |
| Micron RAM 16HTF51264AZ-667C1 4GB DIMM DDR2 667MT/s     | 1        | 0.67%   |

Memory Kind
-----------

Memory module kinds

![Memory Kind](./images/pie_chart/memory_kind.svg)


| Kind    | Desktops | Percent |
|---------|----------|---------|
| DDR4    | 73       | 60.83%  |
| DDR3    | 32       | 26.67%  |
| Unknown | 9        | 7.5%    |
| DDR2    | 3        | 2.5%    |
| SDRAM   | 2        | 1.67%   |
| DDR     | 1        | 0.83%   |

Memory Form Factor
------------------

Physical design of the memory module

![Memory Form Factor](./images/pie_chart/memory_formfactor.svg)


| Name    | Desktops | Percent |
|---------|----------|---------|
| DIMM    | 115      | 96.64%  |
| SODIMM  | 3        | 2.52%   |
| Unknown | 1        | 0.84%   |

Memory Size
-----------

Memory module size

![Memory Size](./images/pie_chart/memory_size.svg)


| Size  | Desktops | Percent |
|-------|----------|---------|
| 8192  | 60       | 44.78%  |
| 4096  | 28       | 20.9%   |
| 16384 | 23       | 17.16%  |
| 2048  | 11       | 8.21%   |
| 32768 | 9        | 6.72%   |
| 1024  | 3        | 2.24%   |

Memory Speed
------------

Memory module speed

![Memory Speed](./images/pie_chart/memory_speed.svg)


| Speed   | Desktops | Percent |
|---------|----------|---------|
| 3200    | 24       | 18.18%  |
| 1600    | 14       | 10.61%  |
| 1333    | 13       | 9.85%   |
| 3600    | 11       | 8.33%   |
| 2667    | 11       | 8.33%   |
| 2133    | 9        | 6.82%   |
| 2400    | 7        | 5.3%    |
| 1800    | 4        | 3.03%   |
| 3266    | 3        | 2.27%   |
| 2933    | 3        | 2.27%   |
| 1867    | 3        | 2.27%   |
| 3733    | 2        | 1.52%   |
| 3533    | 2        | 1.52%   |
| 2666    | 2        | 1.52%   |
| 1067    | 2        | 1.52%   |
| 800     | 2        | 1.52%   |
| Unknown | 2        | 1.52%   |
| 49926   | 1        | 0.76%   |
| 4266    | 1        | 0.76%   |
| 4000    | 1        | 0.76%   |
| 3866    | 1        | 0.76%   |
| 3800    | 1        | 0.76%   |
| 3400    | 1        | 0.76%   |
| 3067    | 1        | 0.76%   |
| 3000    | 1        | 0.76%   |
| 2800    | 1        | 0.76%   |
| 2733    | 1        | 0.76%   |
| 2134    | 1        | 0.76%   |
| 2048    | 1        | 0.76%   |
| 2000    | 1        | 0.76%   |
| 1866    | 1        | 0.76%   |
| 1066    | 1        | 0.76%   |
| 667     | 1        | 0.76%   |
| 533     | 1        | 0.76%   |
| 400     | 1        | 0.76%   |

Printers & scanners
-------------------

Printer Vendor
--------------

Printer device vendors

![Printer Vendor](./images/pie_chart/printer_vendor.svg)


| Vendor              | Desktops | Percent |
|---------------------|----------|---------|
| Samsung Electronics | 7        | 33.33%  |
| Canon               | 6        | 28.57%  |
| Hewlett-Packard     | 5        | 23.81%  |
| Brother Industries  | 3        | 14.29%  |

Printer Model
-------------

Printer device models

![Printer Model](./images/pie_chart/printer_model.svg)


| Model                                | Desktops | Percent |
|--------------------------------------|----------|---------|
| Samsung SCX-3400 Series              | 2        | 9.52%   |
| Samsung M283x Series                 | 2        | 9.52%   |
| Samsung ML-216x Series Laser Printer | 1        | 4.76%   |
| Samsung ML-1865                      | 1        | 4.76%   |
| Samsung C43x Series                  | 1        | 4.76%   |
| HP OfficeJet 5200 series             | 1        | 4.76%   |
| HP LaserJet 400 colorMFP M475dn      | 1        | 4.76%   |
| HP ENVY Photo 6200 series            | 1        | 4.76%   |
| HP DeskJet F4100 Printer series      | 1        | 4.76%   |
| HP DeskJet 3700 series               | 1        | 4.76%   |
| Canon TR8600 series                  | 1        | 4.76%   |
| Canon TR4500 series                  | 1        | 4.76%   |
| Canon PIXMA MP280                    | 1        | 4.76%   |
| Canon PIXMA MG3600 Series            | 1        | 4.76%   |
| Canon MF731C/733C                    | 1        | 4.76%   |
| Canon iP7200 series                  | 1        | 4.76%   |
| Brother Printer                      | 1        | 4.76%   |
| Brother HL-3140CW series             | 1        | 4.76%   |
| Brother HL-1430 Laser Printer        | 1        | 4.76%   |

Scanner Vendor
--------------

Scanner device vendors

![Scanner Vendor](./images/pie_chart/scanner_vendor.svg)


| Vendor      | Desktops | Percent |
|-------------|----------|---------|
| Canon       | 5        | 62.5%   |
| Seiko Epson | 3        | 37.5%   |

Scanner Model
-------------

Scanner device models

![Scanner Model](./images/pie_chart/scanner_model.svg)


| Model                                         | Desktops | Percent |
|-----------------------------------------------|----------|---------|
| Canon CanoScan LiDE 210                       | 2        | 25%     |
| Seiko Epson Scanner                           | 1        | 12.5%   |
| Seiko Epson GT-9400UF [Perfection 3170]       | 1        | 12.5%   |
| Seiko Epson GT-9300UF [Perfection 2400 PHOTO] | 1        | 12.5%   |
| Canon CanoScan N1240U/LiDE 30                 | 1        | 12.5%   |
| Canon CanoScan LIDE 25                        | 1        | 12.5%   |
| Canon CanoScan LiDE 200                       | 1        | 12.5%   |

Camera
------

Camera Vendor
-------------

Camera device vendors

![Camera Vendor](./images/pie_chart/camera_vendor.svg)


| Vendor                        | Desktops | Percent |
|-------------------------------|----------|---------|
| Logitech                      | 30       | 48.39%  |
| Microsoft                     | 5        | 8.06%   |
| Microdia                      | 4        | 6.45%   |
| Generalplus Technology        | 4        | 6.45%   |
| ARC International             | 3        | 4.84%   |
| Apple                         | 3        | 4.84%   |
| Sunplus Innovation Technology | 2        | 3.23%   |
| Samsung Electronics           | 2        | 3.23%   |
| Huawei Technologies           | 2        | 3.23%   |
| Z-Star Microelectronics       | 1        | 1.61%   |
| Unknown                       | 1        | 1.61%   |
| PrehKeyTec                    | 1        | 1.61%   |
| Philips (or NXP)              | 1        | 1.61%   |
| Jieli Technology              | 1        | 1.61%   |
| HD WEBCAM                     | 1        | 1.61%   |
| Creative Technology           | 1        | 1.61%   |

Camera Model
------------

Camera device models

![Camera Model](./images/pie_chart/camera_model.svg)


| Model                                     | Desktops | Percent |
|-------------------------------------------|----------|---------|
| Logitech Webcam C270                      | 7        | 11.29%  |
| Logitech HD Pro Webcam C920               | 4        | 6.45%   |
| Logitech Webcam C930e                     | 3        | 4.84%   |
| Logitech HD Webcam C910                   | 3        | 4.84%   |
| ARC International Camera                  | 3        | 4.84%   |
| Samsung Galaxy A5 (MTP)                   | 2        | 3.23%   |
| Microsoft LifeCam HD-3000                 | 2        | 3.23%   |
| Microdia USB 2.0 Camera                   | 2        | 3.23%   |
| Logitech HD Webcam C615                   | 2        | 3.23%   |
| Logitech C922 Pro Stream Webcam           | 2        | 3.23%   |
| Logitech BRIO                             | 2        | 3.23%   |
| Huawei UVC Camera                         | 2        | 3.23%   |
| Generalplus GENERAL WEBCAM                | 2        | 3.23%   |
| Generalplus 808 Camera                    | 2        | 3.23%   |
| Z-Star Lenovo USB 2.0 UVC Camera          | 1        | 1.61%   |
| Unknown HD camera                         | 1        | 1.61%   |
| Sunplus HD 720P webcam                    | 1        | 1.61%   |
| Sunplus Depstech webcam MIC               | 1        | 1.61%   |
| PrehKeyTec TA-0120-AS                     | 1        | 1.61%   |
| Philips (or NXP) SPC 520/525NC PC Camera  | 1        | 1.61%   |
| Microsoft LifeCam VX-7000 (UVC-compliant) | 1        | 1.61%   |
| Microsoft LifeCam Studio                  | 1        | 1.61%   |
| Microsoft LifeCam HD-5000                 | 1        | 1.61%   |
| Microdia Webcam Vitade AF                 | 1        | 1.61%   |
| Microdia Sonix USB 2.0 Camera             | 1        | 1.61%   |
| Logitech Webcam C310                      | 1        | 1.61%   |
| Logitech Webcam C300                      | 1        | 1.61%   |
| Logitech Webcam C110                      | 1        | 1.61%   |
| Logitech QuickCam Pro for Notebooks       | 1        | 1.61%   |
| Logitech QuickCam Pro 9000                | 1        | 1.61%   |
| Logitech HD Webcam C525                   | 1        | 1.61%   |
| Logitech HD Webcam B910                   | 1        | 1.61%   |
| Jieli USB PHY 2.0                         | 1        | 1.61%   |
| HD WEBCAM HD WEBCAM                       | 1        | 1.61%   |
| Creative Live! Cam Chat HD [VF0700]       | 1        | 1.61%   |
| Apple iPhone 5/5C/5S/6/SE                 | 1        | 1.61%   |
| Apple iPhone 4S                           | 1        | 1.61%   |
| Apple iPhone 4                            | 1        | 1.61%   |

Security
--------

Fingerprint Vendor
------------------

Fingerprint sensor vendors

![Fingerprint Vendor](./images/pie_chart/fingerprint_vendor.svg)


| Vendor                | Desktops | Percent |
|-----------------------|----------|---------|
| Elan Microelectronics | 1        | 100%    |

Fingerprint Model
-----------------

Fingerprint sensor models

![Fingerprint Model](./images/pie_chart/fingerprint_model.svg)


| Model                                       | Desktops | Percent |
|---------------------------------------------|----------|---------|
| Elan fingerprint sensor [FeinTech FPS00200] | 1        | 100%    |

Chipcard Vendor
---------------

Chipcard module vendors

![Chipcard Vendor](./images/pie_chart/chipcard_vendor.svg)


| Vendor                | Desktops | Percent |
|-----------------------|----------|---------|
| Realtek Semiconductor | 1        | 100%    |

Chipcard Model
--------------

Chipcard module models

![Chipcard Model](./images/pie_chart/chipcard_model.svg)


| Model                                             | Desktops | Percent |
|---------------------------------------------------|----------|---------|
| Realtek Semiconductor Smart Card Reader Interface | 1        | 100%    |

Unsupported
-----------

Unsupported Devices
-------------------

Total unsupported devices on board

![Unsupported Devices](./images/pie_chart/device_unsupported.svg)


| Total | Desktops | Percent |
|-------|----------|---------|
| 0     | 290      | 87.09%  |
| 1     | 38       | 11.41%  |
| 2     | 4        | 1.2%    |
| 5     | 1        | 0.3%    |

Unsupported Device Types
------------------------

Types of unsupported devices

![Unsupported Device Types](./images/pie_chart/device_unsupported_type.svg)


| Type                     | Desktops | Percent |
|--------------------------|----------|---------|
| Graphics card            | 17       | 36.17%  |
| Net/wireless             | 12       | 25.53%  |
| Unassigned class         | 5        | 10.64%  |
| Multimedia controller    | 3        | 6.38%   |
| Sound                    | 2        | 4.26%   |
| Communication controller | 2        | 4.26%   |
| Storage/raid             | 1        | 2.13%   |
| Modem                    | 1        | 2.13%   |
| Fingerprint reader       | 1        | 2.13%   |
| Chipcard                 | 1        | 2.13%   |
| Card reader              | 1        | 2.13%   |
| Bluetooth                | 1        | 2.13%   |

