Linux Community Hardware Test Coverage
--------------------------------------

A project to collect tested hardware configurations for upcoming and released versions of Linux distributions.

Anyone can contribute to this project by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Contents
--------

* [ Debian 11 ](/Dist/Debian_11)
* [ Debian 10 ](/Dist/Debian_10)
* [ Fedora 35 ](/Dist/Fedora_35)
* [ Fedora 34 ](/Dist/Fedora_34)
* [ Zorin 16 ](/Dist/Zorin_16)
* [ Zorin 15 ](/Dist/Zorin_15)
* [ Ubuntu 21.10 ](/Dist/Ubuntu_21.10)
* [ Ubuntu 21.04 ](/Dist/Ubuntu_21.04)
* [ CentOS 8 ](/Dist/CentOS_8)
* [ CentOS 7 ](/Dist/CentOS_7)
* [ Chrome OS ](/Dist/Chrome_OS)
* [ Elementary 6.1 ](/Dist/Elementary_6.1)
* [ Elementary 6 ](/Dist/Elementary_6)
* [ Gentoo 2.8 ](/Dist/Gentoo_2.8)
* [ Gentoo 2.7 ](/Dist/Gentoo_2.7)
* [ LinuxFX 11 ](/Dist/LinuxFX_11)
* [ LinuxFX 10 ](/Dist/LinuxFX_10)
* [ LMDE 4 ](/Dist/LMDE_4)
* [ Lubuntu 20.04 ](/Dist/Lubuntu_20.04)
* [ MX 21 ](/Dist/MX_21)
* [ MX 20 ](/Dist/MX_20)
* [ MX 19 ](/Dist/MX_19)
* [ openSUSE Leap-15.3 ](/Dist/openSUSE_Leap-15.3)
* [ Parrot 4.11 ](/Dist/Parrot_4.11)
* [ Peppermint 10 ](/Dist/Peppermint_10)
* [ RHEL 8 ](/Dist/RHEL_8)
* [ Slackware 14.2 ](/Dist/Slackware_14.2)
* [ Solus 4.1 ](/Dist/Solus_4.1)
* [ Ubuntu Budgie 20.04 ](/Dist/Ubuntu_Budgie_20.04)
* [ Ubuntu MATE 20.04 ](/Dist/Ubuntu_MATE_20.04)
* [ EndeavourOS ](/Dist/EndeavourOS)
* [ Garuda ](/Dist/Garuda)
